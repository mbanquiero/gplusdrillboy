
(MARCO)
// variables de la drillboy
PROGRAMA initVariables

// espesor del marco
//esp_marco:=10
// posicion de presando
pos_z0 := -maq_dz + esp_marco
pos_pren_z := (pos_z0+dist_prensor_z+drot)

// mdm = compensacion del marco con respecto a la puerta
// doble_marco = 0 ->maquina nueva, 1->maquina vieja


PLANO XY

G49 G40 G80 G50 G61 G90
G54
G92.1
G21 
S1000
G17

rot_0:=3
G01 Y{pos_y_inicial} F{vel_xy}
IF mano_pta==1
	// puerta izquierda
	XG01 X{dp-pos_pinza}
	// puerta izquierda , el cero esta en el tope izquierdo
	// el exe va transformar las posiciones  
	// G52 X{pos_x} ---->  G52 X{pos_abs_x0 - pos_x}
	// G01 X{pos_x} ---->  G01 X{-pos_x}
	pos_abs_x0 := dp - 2*offset
	(ESPEJAR_X = {pos_abs_x0})
ELSE	
	G01 X{-offset}
ENDIF


(Posicion inicial de la pinza)
M20
G4 P{pausa}
G01 Z{pos_desac_tope-p_offset_z} F{vel_Z}
G52 X{-pos_pinza} Y0 Z{pos_z0}
(POS_PINZA = {pos_pinza})

//  moverAYSegura
pos_y_segura := 60
G01 Y{pos_y_segura} F{vel_xy}
G52 X{-pos_pinza+desf_x0} Y0 Z{-maq_dz+esp_marco}

(DESF_Y={-fn_dfy})
(DESF_Z={-fn_dfz})


IF pernios
	// BISAGRA COMUN DE LA PUERTA DE PASO
	t:=0
	WHILE t<cant_pernios
		xp:=pp_pos_x[t] + pp_dm_pernio[t]
		(desf_tope_marco = {desf_tope_marco})
		IF largo_espiga>0
			// mdm  = esp_marco + luz => luz = mdm - esp_marco
			pos_x := xp+mdm-esp_marco  + largo_espiga + desf_tope_marco
		ELSE
			pos_x := xp+mdm+desf_tope_marco
		ENDIF
		IF mano_pta==1
			pos_x := pos_x + desf_izq_marco
		ENDIF
		largo_pernio:=if(tipo_pernio==0,dc,pb_da)
		(PERNIO : {t+1} de {cant_pernios})
		// busco la posicion X
		PROGRAMA moverPinzaPos CON pos_x + largo_pernio + 50

		// me posicion en X pos del pernio
		G01 X{pos_x} F{vel_pinza}

		// hago la bisagra pp dicha
		IF tipo_pernio==0
			//bisagras comunes
			PROGRAMA bisagraXYN CON pos_x,t
		ELSE
			// BISAGRA PARA PUERTA BLINDADA
			PROGRAMA pb_bisagraXYN CON pos_x,t
		ENDIF

		// restauro el offset de Y para que en el siguietne paso se mueva OK a la posicion Y segura
		G52 Y0
		
		t := t + 1 
	END WHILE
ENDIF


PROGRAMA moverAZSegura
			
// finalizacion
M189

//  moverAYSegura
G01 Y{pos_y_segura} F{vel_xy}

// FINALIZAR PIEZA
//  terminar el trabajo ahora:
// entonces retiro la pieza y finalizo todo
PROGRAMA retirarPieza

