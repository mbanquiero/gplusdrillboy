// CERRADURA

PLANO XY
G18
	
IF mano_pta==1
	// OJO: RECORDAR MULTIPLICAR POR K (-1) LOS RADIOS I DE LOS G02 Y G03
	K:=-1
	pos_x0 := alto_pta-(pos_picaporte-cdf)
ELSE
	K:=1
	pos_x0 := pos_picaporte-cdf
ENDIF


// si termino de hacer el ultimo pernio del canto superior, ya estoy en una posicion Z segura
// que deberia ser esta, por las dudas, la vuelvo a poner
G52 X{-dfx-pos_pinza+desf_x0} Y{dfy+suple_dy} Z{-maq_dz-dfz}
PROGRAMA moverAZSegura

// Ahora bajo hasta la posicion y segura para presentar
pos_y_segura := mar_ys_rot
G01 Y{pos_y_segura} F{vel_xy}

// voy a buscar la posicion X (estoy en el canto inferior)
// la posicion de la pinza conviene que este a la derecha del cajeado, de tal forma que deje unos 261 aprox
PROGRAMA moverPinzaPos CON pos_x0 + dist_pinza_prensor
(POS_PINZA = {pos_pinza})

// corrijo el offset de X para que el cero sea la posicion del picaporte
G52 X{pos_x0-pos_pinza + desf_x0}
G01 X0 F{vel_xy}

(Bajo a Y para entrar)
G52 Y{-dfresa_cajeado}
G01 Y0 F{vel_xy}

// y me posiciono en Z 
G52 Z{-maq_dz-dfz+pieza_dz}
G01 Z0 F{vel_z}

	
// ----------------------------------------------------------------------
// la cerradura esta desplazada del centro = esp_pta/2
(cdb={cdb} , esp_pta={esp_pta} , r_fresa_i={r_fresa_i}, dcentro={dcentro} )
aux_z:=(esp_pta-cdb)/2+r_fresa_i+dcentro
aux_k:=(cdb-2*r_fresa_i)
(aux_z={aux_z})
	
// me posiciono cerca del punto de entra. Por un prob. mecanico, conviene
// penetran en diaganol y no direcamente entrar en el pto exacto de entrada
// por eso al aux_z, se le resta 2 mm (1)
IF sentido
	G01 X{K*(cda-cdb/2-10)} Y-3 F{vel_xy}
ELSE
	G01 X{K*(cdb/2+10)} Y-3 F{vel_xy}
ENDIF

M03

IF cdb < 2*r_fresa_i + 4
	//no puede dejar los 2 mm porque no se va del ancho de la cerradura
	G01 Z{-aux_z} F{vel_z}
ELSE
	//Entra a 2 milimetros del borde para no romper la madera
	G01 Z{-aux_z-2} F{vel_z}
ENDIF
	
G01 Y-1

IF sentido
	'como el perimetro en el sentido horario
	// (1) Entra en el pto de entrada, interpolando Y tambien, para no entrar tan recto
	G01 X{K*(cda-cdb/2)} Y{cdl} F{vel_mec}
	ACTIVAR FRESA
	G01 Z{-aux_z} F{vel_z}

	IF mano_pta==0
		G03 X{cda-cdb/2} Z{-aux_z-aux_k} I0  K{-(aux_k/2)} F{vel_curvas}
	ELSE
		G02 X{-(cda-cdb/2)} Z{-aux_z-aux_k} I0  K{-(aux_k/2)} F{vel_curvas}
	ENDIF
	G01 X{K*cdb/2} F{vel_mec}
	IF mano_pta==0
		G03 X{(cdb/2)} Z{-aux_z} I0  K{aux_k/2} F{vel_curvas}
	ELSE
		G02 X{-(cdb/2)} Z{-aux_z} I0  K{aux_k/2} F{vel_curvas}
	ENDIF
	G01 X{K*(cda-cdb/2)} F{vel_mec}

	G01 Z{-aux_z} F{vel_z}

	IF doblep_c
		//se agrego una doble pasada por si la madera se expande luego de la primera pasada
		'Segunda Pasada al frente de la cerradura
		IF mano_pta==0
			G03 X{cda-cdb/2} Z{-aux_z-aux_k} I0  K{-(aux_k/2)} F{vel_curvas}
		ELSE
			G02 X{-(cda-cdb/2)} Z{-aux_z-aux_k} I0  K{-(aux_k/2)} F{vel_curvas}
		ENDIF
		G01 X{K*cdb/2} Y{cdl} F{vel_mec}
		IF mano_pta==0
			G03 X{(cdb/2)} Z{-aux_z} I0  K{aux_k/2} F{vel_curvas}
		ELSE
			G02 X{-(cdb/2)} Z{-aux_z} I0  K{aux_k/2} F{vel_curvas}
		ENDIF
		G01 X{K*(cda-cdb/2)} F{vel_mec}
	ENDIF

	G01 X{K*cdb/2} F{vel_mec}

ELSE

	'como el perimetro en el sentido antihorario {normal}

	G01 X{K*cdb/2} Y{cdl} F{vel_mec}
	ACTIVAR FRESA
	G01 Z{-aux_z} F{vel_z}
	IF mano_pta==0
		G02 X{K*cdb/2} Z{-aux_z-aux_k} I0  K{-(aux_k/2)} F{vel_curvas}
	ELSE
		G03 X{K*cdb/2} Z{-aux_z-aux_k} I0  K{-(aux_k/2)} F{vel_curvas}
	ENDIF
	G01 X{K*(cda-cdb/2)} F{vel_mec}
	IF mano_pta==0
		G02 X{K*(cda-cdb/2)} Z{-aux_z} I0  K{aux_k/2} F{vel_curvas}
	ELSE
		G03 X{K*(cda-cdb/2)} Z{-aux_z} I0  K{aux_k/2} F{vel_curvas}
	ENDIF
	G01 Z{-aux_z} F{vel_z}
	G01 X{K*cdb/2} F{vel_mec}

	IF doblep_c
		//se agrego una doble pasada por si la madera se expande luego de la primera pasada
		'Doble pasada al frente de la cerradura
		IF mano_pta==0
			G02 X{K*cdb/2} Z{-aux_z-aux_k} I0  K{-(aux_k/2)} F{vel_curvas}
		ELSE
			G03 X{K*cdb/2} Z{-aux_z-aux_k} I0  K{-(aux_k/2)} F{vel_curvas}
		ENDIF
		G01 X{K*(cda-cdb/2)} F{vel_mec}
		IF mano_pta==0
			G02 X{K*(cda-cdb/2)} Z{-aux_z} I0  K{aux_k/2} F{vel_curvas}
		ELSE
			G03 X{K*(cda-cdb/2)} Z{-aux_z} I0  K{aux_k/2} F{vel_curvas}
		ENDIF
	ENDIF

	G01 X{K*cdb/2} F{vel_mec}

ENDIF

PARAR FRESA
G01 Y-3 F{vel_xy}


' Cajon cerraduras
IF circular==1
	IF K==1
		G52 X{pos_picaporte-diam/2-pos_pinza + desf_x0} 
	ELSE
		G52 X{alto_pta-(pos_picaporte-diam/2)-pos_pinza + desf_x0} 
	ENDIF
ELSE
	IF K==1
		G52 X{pos_picaporte-cdf+cdh-pos_pinza + desf_x0}
	ELSE
		G52 X{alto_pta-(pos_picaporte-cdf+cdh)-pos_pinza + desf_x0}
	ENDIF
ENDIF
G01 Z{-esp_pta/2-dcentro} F{vel_z}

G01 X0 Y{cdl} F{vel_mec}
ACTIVAR FRESA
prof_y:=cdl
WHILE prof_y<cdd
	prof_y:=prof_y+cfeed
	IF prof_y>cdd
		prof_y:=cdd
	ENDIF

	IF circular==1
		G01 X{K*r_fresa_i} F{vel_xy}
		G01 Y{prof_y} F{vel_pen}
		G02 X{K*r_fresa_i} Z{-esp_pta/2-dcentro} I{K*(diam/2-r_fresa_i)} K0 F{vel_curvas}
	ELSE
		IF cde<=2*r_fresa_i
			IF zigzag
				G01 X{K*cdc} Y{prof_y} F{vel_cajeado}
			ELSE
				G01 Y{prof_y} F{vel_pen}
				G01 X{K*cdc} F{vel_cajeado}
			ENDIF
			IF prof_y<cdd
				prof_y:=prof_y+cfeed
				IF prof_y>cdd
					prof_y:=cdd
				ENDIF
				IF zigzag
					G01 X0 Y{prof_y} F{vel_cajeado}
				ELSE
					G01 Y{prof_y} F{vel_pen}
					G01 X0 F{vel_cajeado}
				ENDIF
			ENDIF
		ELSE
			' hace rectangulos
			G01 Z{-(esp_pta-cde)/2-r_fresa_i-dcentro} F{vel_z}
			IF zigzag
				G01 X{K*cdc} Y{prof_y} F{vel_cajeado}
			ELSE
				G01 Y{prof_y} F{vel_pen}
				G01 X{K*cdc} F{vel_cajeado}
			ENDIF
			G01 Z{-(esp_pta+cde)/2+r_fresa_i-dcentro} F{vel_z}
			IF zigzag
				prof_y:=prof_y+cfeed
				IF prof_y>cdd
					prof_y:=cdd
				ENDIF
				G01 X0 Y{prof_y} F{vel_cajeado}

			ELSE
				G01 X0 F{vel_cajeado}
			ENDIF
		ENDIF
	ENDIF

END WHILE

IF zigzag==1 .AND. circular!=1 .AND. cde>16
	' completo el rectangulo
	G01 Z{-(esp_pta+cde)/2+r_fresa_i-dcentro} F{vel_z}
	G01 X{K*cdc} F{vel_cajeado}
	G01 Z{-(esp_pta-cde)/2-r_fresa_i-dcentro} F{vel_z}
	G01 X0 F{vel_cajeado}
ENDIF

'Fin de la cerradura {voy a referenciar}

PARAR FRESA
G01 Y-3

G54
G52 Z0
G01 Z0 F{vel_z}
	
M05
	
	
// Aca ya trabajo con otro conjunto, y en otro plano
G17
	
IF pestillo
	' Ojo pestillo

	IF K==1
		G52 X{pos_picaporte-pos_pinza + desf_x0} Y{cdi-cdj/2} Z{-maq_abajo_dz+esp_pta}
	ELSE
		G52 X{alto_pta-pos_picaporte-pos_pinza + desf_x0} Y{cdi-cdj/2} Z{-maq_abajo_dz+esp_pta}
	ENDIF
	
	G01 X0 Y{r_fresa_p} F{vel_xy}
	
	ACTIVAR FRESA
	IF circular
		//si es  cerradura tubular
		'cerradura tubular
		prof_z:=0
		WHILE prof_z<esp_pta+2
			prof_z:=prof_z+feed
			IF prof_z>=esp_pta
				prof_z:=esp_pta+2
			ENDIF
			G01 Z{-prof_z}  F{vel_z}
			IF cdj>r_fresa_p*2
				G02 X0 Y{r_fresa_p} I0 J{(cdj-r_fresa_p*2)/2} F{vel_curvas}
			ENDIF
		END WHILE
	
	ELSE
		//si no es  cerradura tubular
		//come en cuatro pasadas, salteo el centro porque es hueco		
		aux_z:=0
		profundiza:=9
		WHILE -aux_z < esp_pta+2
			IF aux_z == -2 * profundiza
				aux_z:= -(esp_pta + 2 - 2*profundiza)
			ENDIF
			aux_z:= aux_z - profundiza				
			G01 Z{aux_z}  F{vel_pen}
				
			IF cdj>r_fresa_p*2
				G02 X0 Y{r_fresa_p} I0 J{(cdj-r_fresa_p*2)/2} F700
			ENDIF
			
		END WHILE
				
	ENDIF
ENDIF
	
PARAR FRESA

G54
G52 Z0
G01 Z0 F{vel_z}

	
	
// Agujero de la condena
IF cdk!=0 .AND. condena==1
	'agujero de la condena
	
	//me aseguro de que cdk no sea menor que mdk3
	IF cdk3>cdk
		aux:=cdk3
		cdk3:=cdk
		cdk:=aux
	ENDIF

	IF K==1
		G52 X{pos_picaporte+cdg-pos_pinza + desf_x0} Y{cdi-cdk/2} Z{-maq_abajo_dz+esp_pta}
	ELSE
		G52 X{alto_pta-(pos_picaporte+cdg)-pos_pinza + desf_x0} Y{cdi-cdk/2} Z{-maq_abajo_dz+esp_pta}
	ENDIF
		
	M03
		
	G01 X0 Y{r_fresa_p} F{vel_xy}
	G01 Z2 F{vel_z}
	
	// el agujero se hace en 2 etapas
	// hasta prof1 hace un agujero grande (diam cdk), luego saltea la caja de la cerradura
	// y luego hace hasta la salida el agujero chico, diam cdk3

	' agujero grande {DIAM cdk}	
	prof_z:=0
	prof1:=(esp_pta-cde)/2+2
	WHILE prof_z<prof1
		prof_z:=prof_z+feed
		IF prof_z>=prof1
			prof_z:=prof1
		ENDIF
		G01 Z{-prof_z}  F{vel_pen}
		IF cdk>2*r_fresa_p
			G02 X0 Y{r_fresa_p} I0 J{(cdk-2*r_fresa_p)/2} F{vel_curvas}
		ENDIF
		IF cdk2>r_fresa_p
			G01 Y{cdk/2} F{vel_mec}
			G01 X{K*(cdk2-r_fresa_p)}
			G01 X0
			G01 Y{r_fresa_p}
		ENDIF
	END WHILE
	
	' agujero chico {DIAM cdk3}, para escudo de la llave
	
	IF cdk3>0
		IF K==1
			G52 X{pos_picaporte+cdg-pos_pinza + desf_x0} Y{cdi-cdk3/2} Z{-maq_abajo_dz+esp_pta}
		ELSE
			G52 X{alto_pta-(pos_picaporte+cdg)-pos_pinza + desf_x0} Y{cdi-cdk3/2} Z{-maq_abajo_dz+esp_pta}
		ENDIF
		G01 X0 Y{r_fresa_p} F{vel_xy}
	
		' salteo la prof1 mas el vaciado de la cerradura
		prof_z:=prof1+cde
		WHILE prof_z<esp_pta+2
			prof_z:=prof_z+feed
			IF prof_z>=esp_pta
				prof_z:=esp_pta+2
			ENDIF
			G01 Z{-prof_z}  F{vel_pen}
			IF cdk3>2*r_fresa_p
				G02 X0 Y{r_fresa_p} I0 J{(cdk3-2*r_fresa_p)/2} F{vel_curvas}
			ENDIF
			IF cdk2>r_fresa_p
				G01 Y{cdk/2} F{vel_mec}
				G01 X{K*(cdk2-r_fresa_p)}
				G01 X0
				G01 Y{r_fresa_p}	
			ENDIF
		END WHILE
	ENDIF
	G54
	G52 Z0
	G01 Z0 F{vel_z}
ENDIF
	
	
// Agujero de la mirilla
IF radio_mirilla!=0
	'Agujero de la mirilla

	IF K==1
		G52 X{-pos_pinza + desf_x0} Y0 Z{-maq_abajo_dz+esp_pta}
	ELSE
		G52 X{alto_pta-pos_pinza + desf_x0} Y0 Z{-maq_abajo_dz+esp_pta}
	ENDIF
	
	IF radio_mirilla>r_fresa_p
		M03
		
		G01 X{K*(alto_pta-alto_mirilla)} Y{ancho_pta/2+radio_mirilla-r_fresa_p} F{vel_xy}
		G01 Z2 F{vel_z}
		
		ACTIVAR FRESA
		pen_z:=0
		'penetro en circulos
		WHILE pen_z<esp_pta+2
			pen_z:=pen_z+feed
			IF pen_z>esp_pta+2
				pen_z:= esp_pta+2
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
			G02 X{K*(alto_pta-alto_mirilla)} Y{ancho_pta/2+radio_mirilla-r_fresa_p} I0 J{r_fresa_p-radio_mirilla} F{vel_curvas}
		END WHILE
	ELSE
			
		' solo perfora		
		M03
		G01 X{K*(alto_pta-alto_mirilla)} Y{ancho_pta/2} F{vel_xy}
		
		ACTIVAR FRESA
		pen_z:=0
		WHILE pen_z<esp_pta+2
			pen_z:=pen_z+feed
			IF pen_z>esp_pta+2
				pen_z:= esp_pta+2
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
		END WHILE
	ENDIF
	G01 Z5

ENDIF
PARAR FRESA
	
// Agujero del pomo

IF radio_pomo!=0
	'agujero del pomo
	IF K==1
		G52 X{-pos_pinza + desf_x0} Y0 Z{-maq_abajo_dz+esp_pta}
	ELSE
		G52 X{alto_pta-pos_pinza + desf_x0} Y0 Z{-maq_abajo_dz+esp_pta}
	ENDIF

	IF radio_pomo>r_fresa_p
		'penetra en circulos
		M03
			
		G01 X{K*(alto_pta-alto_pomo)} Y{ancho_pta/2+radio_pomo-r_fresa_p} F{vel_xy}
			
		ACTIVAR FRESA
		pen_z:=0		
		WHILE pen_z<esp_pta+2
			pen_z:=pen_z+feed
			IF pen_z>esp_pta+2
				pen_z:= esp_pta+2
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
			G02 X{K*(alto_pta-alto_pomo)} Y{ancho_pta/2+radio_pomo-r_fresa_p} I0 J{r_fresa_p-radio_pomo} F{vel_curvas}
		END WHILE
	ELSE
		' solo perfora
		M03
			
		G01 X{K*(alto_pta-alto_pomo)} Y{ancho_pta/2} F{vel_xy}
			
		ACTIVAR FRESA
		pen_z:=0
		WHILE pen_z<esp_pta+2
			pen_z:=pen_z+feed
			IF pen_z>esp_pta+2
				pen_z:= esp_pta+2
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
		END WHILE
	ENDIF
	G01 Z5
ENDIF
	
'finalizo
PARAR FRESA
	

