PARAMETROS pos_x,npernio

(pos pernio = {pos_x} , pos_pinza ={pos_pinza})

//si es simetrico es una bisagra inclinada de las dos lados
db3:=0
IF db2==0 .OR. db2==db
	' bisagra normal	
	db2:=db
ELSE
	' bisagra inclinada	
	IF db2<2*r_fresa_s
		db2:=2*r_fresa_s
	ENDIF
	IF simetrica
		db3:=(db-db2)/2
		db2:=db2 + db3
	ENDIF
ENDIF



// bisagra para marco, maquina nueva
// mds = generalemente 1 o 2 mm 
IF de<2*r_fresa_p
	de:=2*r_fresa_p
ENDIF
df := calle
dy := esp_pta + mds


IF ri==0
	ri:=de/2
ENDIF
IF rd==0
	rd:=de/2
ENDIF


'Comienzo del programa bisagra en el marco
G52 X{-pos_pinza+desf_x0+pos_x} Y{dy + fn_dfy} Z{-maq_dz+esp_marco+fn_dfz+ l_fresa_p}

IF invisible
	PROGRAMA invisiblexy
ELSE
		
	G01 X{dc-da-r_fresa_p} Y{-dy} F{vel_xy}
	M502
	G04 p1
	//cuello de la bisagra
	IF db2 < db
		//entro por la izquierda del cuello para la bisagra oblicua
		G01 X{dc-da-db+db3+r_fresa_p} F{vel_xy}
		G01 Z0.2 F{vel_z}
		G01 Z{-prof} F{vel_pen}
		ACTIVAR FRESA
		
		'comienza el mecanizado
		'cuello
		G01 X{dc-da-db+r_fresa_p} Y{-df-de+r_fresa_p} F{vel_mec}
		G01 X{dc-da-db+db3+r_fresa_p} Y{-dy}
		G01 X{dc-da-(db-db2)-r_fresa_p} F{vel_mec}
		G01 X{dc-da-r_fresa_p} Y{-df-de+r_fresa_p}
		
	ELSE
		//entro por la derecha(normal)
		G01 Z0.2 F{vel_z}
		G01 Z{-prof} F{vel_pen}
		ACTIVAR FRESA
		G01 Y{-df-de+r_fresa_p} F{vel_mec}
		
		'comienza el mecanizado
		'cuello
		G01 Y{-dy}
		G01 X{dc-da-db+r_fresa_p}
		G01 Y{-df-de+r_fresa_p} F1000
	
		//si el cuello es mayor a 28 hace una pasada mas
		IF db>4*r_fresa_p
			'hace una pasada porque es mayor a 28
			G01 X{dc-da-3*r_fresa_p-1} F{vel_mec}	
			G01 Y{-dy}
			G01 Y{-df-de+r_fresa_p} F1000
		ENDIF
		
	ENDIF		

	'cuerpo
	IF de<=2*r_fresa_p
		// BISAGRA COMUN		
		G01 X{r_fresa_p} F{vel_mec}
		G01 X{dc-r_fresa_p}
	ELSE

		' HIBRIDA
		
		IF da>ri
			G01 X{dc-ri}
			G03 X{dc-r_fresa_p} Y{-df-de+ri} I0 J{ri-r_fresa_p} F{vel_curvas}
		ELSE
			G01 X{dc-r_fresa_p}
		ENDIF
		
		IF ri<=r_fresa_p
			G01 Y{-df-r_fresa_p} F{vel_mec}
		ELSE
			G01 Y{-df-ri} F{vel_mec}
			G03 X{dc-ri} Y{-df-r_fresa_p} I{r_fresa_p-ri} J0 F{vel_curvas}
		ENDIF
		IF rd <=r_fresa_p
			G01 X{r_fresa_p}	F{vel_mec}
			G01 Y{-df-de+r_fresa_p}
		ELSE
			G01 X{rd} F{vel_mec}
			G03 X{r_fresa_p} Y{-df-rd} I0 J{r_fresa_p-ri}	F{vel_curvas}
			G01 X{r_fresa_p} Y{-df-de+rd} F{vel_mec}
			G03 X{rd} Y{-df-de+r_fresa_p} I{rd-r_fresa_p} J0 F{vel_curvas}
		ENDIF
		G01 X{dc-da-r_fresa_p} F{vel_mec}

	ENDIF

	//fin del if invisible
ENDIF

M503
PARAR FRESA

(retiro Z)
PROGRAMA moverAZSegura

'fin de la bisagra en el marco



