//Programa de la bisagra Anuba
//todo protocolo de inicializacion y finalizacion debe ir en bisagra.pro



// Agujero
IF rd>0
	//dejo el offset en el centro del agujero. El X ya esta bien ubicado. Corrijo el Y
	
	G52 X{K*pos_x-da} Y{dd/2}
	
	'agujero
	
	G01 Y0
	G01 X0
	ACTIVAR FRESA	

	IF rd<=r_fresa_p
		G01 Z{-prof} F{vel_pen}
	ELSE
		
		//Cantidad de pasadas para hacer el agujero
		cant_pasadas:= 3
		j:=0
		WHILE j<cant_pasadas
			G01 Z{-(j+1)*prof/cant_pasadas} F{vel_pen}
			IF K == 1
				G02 X0 Y0 I{rd-r_fresa_p} J0 F{vel_curvas}
			ELSE
				G03 X0 Y0 I{rd-r_fresa_p} J0 F{vel_curvas}
			ENDIF
			
			j := j + 1
		END WHILE
	ENDIF
	
ENDIF
PARAR FRESA
'finalizo la bisagra en el marco
G01 Z{dsz} F{vel_mec}




