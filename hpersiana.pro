// herraje de la persiana
//desfasajes des_d desfasaje espiga derecha
//des_m desfasaje espigas centradas
des_d:=0
des_m:=0
sep3:=0

PLANO XY 
G49 G40 G80 G50 G61 G90
G54
G92.1
G21 
S1000

rot_0:=3
pi:=3.14159
alfa:=angulo_ranura*pi/180
dx:=largo_ranura*cos(alfa)
dy:=largo_ranura*sin(alfa)

// pos_x1 = pos de la primera ranura,
// pos_x2 usualmente es igual a pos_x1, pero luego, como el paso
// es fijo, se agranda pos_x2 para que quede al ras de la ultima ranura
//pos_x2:=pos_x1

// dr es la distancia entre ranuras
dr:=dx-solape_ranura

IF dr<10
	dr:=10
ENDIF

// ranuras
// calculo el largo util y la cantidad de ranuras
largo_u := largo_pieza - pos_x2 - pos_x1
cant:= INT((largo_u-dx)/dr ) + 1
IF cant<1
	cant:=1
ENDIF

dt:=0
cant_tra:=0
IF pos_x3>0
	IF pos_x3<30
		cant_tra:=pos_x3
		// travesanios centrados, cuando llega a un trav. deja 2 varillas libres
		dt:= INT((cant-2*cant_tra)/(cant_tra+1))
		// hago dt varillas, luego dejo 2, hago dt varillas, dejo 2 etc
	ENDIF
ENDIF

IF modelo==2
	G01 Z0 F{vel_z}
	G01 A90 F{vel_rot}
ENDIF

IF !trav_bisagra .OR. 1
	//si las bisagras van en una posicion variable	
	
	//referencio en y para anular el 0 arriba de la pieza

	PLANO XY
	G17			
	// plano XY
	G59P12
	
	//prendo la fresa para qque el palpador no pegue contra la pieza
	IF modelo ==2
		M03
	ENDIF	

	'Comienza el trabajo en plano XY
	PROGRAMA bisagrapersianaXY CON pos_h_x1

	IF pos_h_x2!=0
		PROGRAMA bisagrapersianaXY CON pos_h_x2
	ENDIF

	IF pos_h_x3!=0
		PROGRAMA bisagrapersianaXY CON pos_h_x3
	ENDIF


	'Comienza el trabajo en canto superior
	G18
	G54
	G92.1
	IF modelo == 2
		//lo llevo a una posicion segura para rotar
		G01 Z0 F{vel_z}
		//me desplazo 50mm debajo de la parte superior de la pieza para esquivar las pinzas
		G01 Y{ancho_per-50} F{vel_xy}
		G01 X{-dist_ax}
		M05
		G01 A0 F{vel_rot}
	ENDIF
	
	pri:=1
	IF pos_h_x3!=0
		PROGRAMA bisagrapersiana CON pos_h_x3+maq_dx,pri
		pri:=0
	ENDIF

	IF pos_h_x2!=0
		PROGRAMA bisagrapersiana CON pos_h_x2+maq_dx,pri
		pri:=0
	ENDIF


	PROGRAMA bisagrapersiana CON pos_h_x1+maq_dx,pri
		
ELSE
	//agrego las ranuras para la bisagra izquierda
		PROGRAMA bisagrapersiana CON pos_h_x1 + pos_x1 - sep1 - (largo_espiga + esp)/2		
	
	IF pos_x3<30			
		i:=pos_x3-1
		WHILE i>=0
			//agrego las ranuras para las bisagras centricas
		
			pos_tra:=pos_x1+((i+1)*dt + 2*i + 1) *dr + (solape_ranura - esp)/2
		
			PROGRAMA bisagrapersiana CON (des_m+pos_tra)
			i:=i-1
		END WHILE
	ELSE
		PROGRAMA bisagrapersiana CON pos_x3+des_m
	ENDIF
		//agrego las ranuras para la bisagra derecha

		PROGRAMA bisagrapersiana CON largo_pieza - ( pos_x2 - sep2 + sep3 + esp)/2 + des_d
ENDIF

// me retiro en Z
G54
G92.1
G01 Z0 F{vel_z}
G01 Y{-ancho_per-maq_dfresa} F{vel_xy}
G28.1 Y{-ancho_per-maq_dfresa} 
G01 X{-dist_ax} Y0 F{vel_xy}
IF modelo == 2
	G01 A{rot_0} F{vel_rot}
ENDIF
G28.1 X{-dist_ax} Y0 Z0

