
// Globales
ancho_pta := 750
zmaquina:=140
espesor_mat:=19
vel1:= 2000
rfresa:=7

// Globales
ancho_pta := 750
zmaquina:=140
espesor_mat:=19

G0 G49 G40 G80 G50 G61 G90
G54
92.1
G21 (mm)
S1000

bisagra:=1
circulo:=1
vel1:=3000

(MSG,Bisagra standard)
G18
G59P11
G52 X120 Y725 Z-124
G01   Y+10 F20000
G01 X41.6 F20000
G01  Z0 F{vel1/2}
M03
G01 Y-2.5 F500
G01 Z14 F{vel1/2}
G01 Z0  F{vel1}
G01 X32.4 F{vel1}
G01 Z14 F{vel1/2}
G01 X7 F{vel1}
G01 X92.5  F{vel1}
G01   Y+10 F{vel1}

M05
G54
G18
G52 Z0
G01 Z0 F2000
