#var op mano_pta := 0	{Derecha,Izquierda}		// Mano de la Puerta

#var mm pb_pos_c1:= 200		// Posicion 1er caja:
#var mm pb_d1:= 120		// Ancho Caja 1:
#var mm pb_pos_c2:= 900		// Posicion 2da caja:
#var mm pb_d2:= 200		// Ancho Caja 2:
#var mm pb_pos_c3:= 1700		// Posicion 3da caja:
#var mm ancho_pta:= 825			// Ancho Puerta :
#var mm alto_pta:= 2030		// Altura Puerta:
#var mm pb_ar:= 20		// Ancho Ranura:
#var mm pb_pr:= 10		// Prof de la Ranura:

#var mm esp_pta:= 40		// Espesor de la Puerta :
#var mm pb_esp_caja:= 16		// Espesor de la Caja:
#var mm pb_prof_caja1:= 80		// Profunidad Caja1:
#var mm pb_prof_caja2:= 100	// Profunidad Caja2:
#var mm pb_pos_lx:= 150	// Posicion de la llave
#var mm pb_pos_ly:= 50	// Posicion de la llave
#var mm pb_lr:= 20		// Radio llave

#var mm pb_alto_mirilla:= 1600 // Altura de la mirilla
#var mm pb_radio_mirilla:= 0	// Radio de la mirilla (Ojo de Buey)

#var mm maq_dz:= 122			// prof maquina:
#var mm feed:= 4			// Profundidad x pasada:

rebaje_canto := pb_rebaje_canto
retropalpador:=0



rot_0:=3
//dcentro:=0
dsz2:=2

G49 G40 G80 G50 G61 G90
G54
G92.1
G21 (mm)
S1000


PLANO XZ
G17

IF mano_pta==1
	K:=-1
ELSE
	K:=1
ENDIF

// maq_dx2 = desfasaje en el eje x en parte de abajo (usualmente se usa en la pta izquierda)
// en la pta derecha = 0, se aplica a las posiciondes de los cajeados 
// no se aplica a la mirilla etc. (Solo se aplica al canto inferior)

G52 Z0
IF retropalpador
	//version vieja que retropalpa, La otra version no se puede mover al z0 porque mantiene el offset 
	//del palpado de las bisagras, Y si se mueve a Z0 estaria tocando la superficie de la madera
	G01 Z0 F{vel_z}
ENDIF


//da la posicion del tope en x que se va a usar
pos_x0 := 0
//posicion del z0
IF retropalpador .OR. palpador_m == 0
	//vesion vieja fv1 Retropalpa Interesa el primer offset para poder apollarse en la pieza
	pos_z0 := -maq_dz + l_fresa_p -pb_rebaje_canto
ELSE
	//Modelo Nuevo Como ahora palpa hacia adelante no importa en que posion esta el offset 
	//total avanza hasta tocar la superficie de la placa
	pos_z0 := 0
ENDIF

IF K == 1
	pos_y0 := esp_pta + dcentro
ELSE
	pos_y0 := dcentro
ENDIF


G52 X{pos_x0} Y{pos_y0} Z{pos_z0}
	
IF retropalpador == 0
	//palpo solo para la fv1 con palpador directo
	(palpa)
	G01 X{alto_pta - 50} F{vel_xy}
		
	G01 Y{-K*(esp_pta/2)} F{vel_xy}
	
	G28.1 Z1
	G52 Z{dpalpador_z}
		
	G01 Z2 F{vel_z}
	
	//enciendo la fresa para esconder el palpador
	M03	
ENDIF

pos_ini_cajas := 0
IF ranura 
	// RANURA
	// ojo que profundiza con rectangulos
	// en cada pasada (ida y vuelta) entonces profundiza 5mm o menos

	hacer_extremo := 0
	//si pb_rd=pb_ri=0 la ranura ocupa toda la puerta
	IF pb_rd == 0	
		pos_ini_cajas := 0
		pb_rd := -10
	ELSE
		pos_ini_cajas := -pb_rd
		pb_rd := pb_rd + r_fresa_i
	ENDIF

	IF pb_ri == 0
		//pos_ini_cajas := 0
		pb_ri := 10
		hacer_extremo := 1
	ELSE
		//pos_ini_cajas := pb_ri
		pb_ri := pb_ri + r_fresa_i
	ENDIF

	'Ranura, profundiza haciendo rectangulos

	G01 X{(alto_pta-pb_rd)} F{vel_xy}
	G01 Y{-K*((esp_pta-pb_ar)/2 + r_fresa_i )} F{vel_y}


	G01 Z{dsz +pb_ rebaje_canto} F{vel_z}
	pen_z:=0

	M03
	G01 Z1 F{vel_z}
	ACTIVAR FRESA
	

	WHILE pen_z < pb_pr
		pen_z:=pen_z+feed
		IF pen_z>pb_pr
			pen_z:= pb_pr
		ENDIF
		
		G01 X{(alto_pta-pb_rd)} F{vel_xy}
		G01 Y{-K*((esp_pta-pb_ar)/2 + r_fresa_i )} F1000
		
		G01 Z{-pen_z} F{vel_pen}
		G01 X{pb_ri} F{vel_cajeado}
		G01 Y{-K*((esp_pta+pb_ar)/2 - r_fresa_i )} F1000
		G01 X{(alto_pta-pb_rd)} F{vel_cajeado}

	END WHILE
	
	PARAR FRESA
	IF hacer_extremo
		' extremo que quedo sin cajear
		G01 Z{dsz+pb_rebaje_canto} F{vel_z}
		G01 X{-4} F{vel_xy}
		pen_z:=0
		WHILE pen_z < pb_pr
			pen_z:=pen_z+feed
			IF pen_z > pb_pr
				pen_z:= pb_pr
			ENDIF

			G01 X{-4} F{vel_cajeado}
			G01 Y{-K*((esp_pta-pb_ar)/2 + r_fresa_i )} F1000

			G01 Z{-pen_z} F{vel_pen}
			ACTIVAR FRESA
			G01 X{12} F{vel_cajeado}
			G01 Y{-K*((esp_pta+pb_ar)/2 - r_fresa_i )} F1000
			G01 X{-4} F{vel_cajeado}

		END WHILE
	ENDIF
	
	PARAR FRESA
ENDIF

// calado de las cajas OJO QUE cajeado NO APAGA LA FRESA 
// Usualmente tiene 3 cajas o 5, la del medio es la caja grande, y el resto son chicas
//desfasaje por los topes de la maquina

desf_x:=pos_x0



IF ranura==0 .AND. cant_cajeados>0 
	// Si no esta la ranura,me tengo que posicionar desde un lugar seguro
	
	IF retropalpador
		//retropalpador
		G52 X0 Y{pos_y0} Z{pos_z0}
	ELSE
		//Palpador directo
		G52 X0 Y{pos_y0}
	ENDIF
	// me retiro a una pos. segura
	
	G01 Z{dsz+pb_rebaje_canto} F{vel_z}	
ENDIF

// Hay 3 vaciados o 5, el del medio es grande y el resto peque�os
t:=0
cg := if(cant_cajeados==3,1,2)
WHILE t < cant_cajeados
	pb_d:= if(t==cg,pb_d2,pb_d1)
	pb_prof_caja:= if(t==cg,pb_prof_caja2,pb_prof_caja1)
	'cajas
	//le sumo la posicion izquierda de la ranura (pb_ri) porque la cajas estan unidas al frente de la ranura
	PROGRAMA cajeadoPXY CON pos_ini_cajas+alto_pta-(pb_pos_c[t]+pb_d)+maq_dx2+desf_x,pb_d,pb_esp_caja,pb_prof_caja,K
	
	// pb_d3:= pb_d2 - 100
	pb_prof2_caja2 := pb_prof_caja3 + pb_prof_caja2
	IF t == cg .AND. pb_prof2_caja2 > pb_prof_caja2
		//segundo cajeado para la caja central
		diffx:= (pb_d - pb_d3)/2
		//hack activo temporalmente la ranura y le doy la profundidad de la primer caja para que cajeado.pro la saltee
		aux_ranura := ranura
		aux_pb_pr := pb_pr
		ranura := 1
		pb_pr := pb_prof_caja
		
		PROGRAMA cajeadoPXY CON pos_ini_cajas+alto_pta-(pb_pos_c[t]+pb_d)+diffx+maq_dx2+desf_x,pb_d3,pb_esp_caja,pb_prof2_caja2,K
		
		ranura:= aux_ranura
		pb_pr := aux_pb_pr
	ENDIF
	
	t := t + 1
END WHILE


'fin de los cajeados {voy a referenciar}

PARAR FRESA
G01 Z{dsz+pb_rebaje_canto} F{vel_z}
M05

'finalizo

G54
G92.1
IF retropalpador == 0
	G28.1 A0
ENDIF
G52 X0 Y0
G01 Y0 F{vel_y}
G01 X{-dist_ax} F{vel_xy}
IF retropalpador
	//version anterior de la fv1 con retropalpador
	G28.1 X{-dist_ax} Y0 Z0	
ELSE
	//version nueva de la fv1 que palpa directo
	G28.1 X{-dist_ax} Y0
	G28.1 Z{dsz+dpalpador_z}
ENDIF

