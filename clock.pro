// Clock
PARAMETROS pos_x,pos_y,ac,tipo
// DSZ := maq_dz-pieza_esp-2
clock_da:=0.5
// Hasta 40mm de diametro
clock_diam:=21
clock_prof:=14.4
clock_prof_c:=10

	IF tipo==0
		// Clock izquierdo
		G59P17
		G52 X{pos_x-clock_da} Y{pos_y} Z{pos_z0}
		K:=1
	ELSE
		// Clock derecho
		G59P17
		G52 X{pieza_dx-pos_x+clock_da} Y{pos_y} Z{pos_z0}
		K:=-1
	ENDIF
		
	G01 Z2 F1500
	G01 X{K*(clock_diam/2-r_fresa_p)} Y0 F{vel_xy}
	IF modelo <2
		M04
	ELSE
		M03
	ENDIF
	ACTIVAR FRESA
	pen_z:=0
	WHILE pen_z<clock_prof
		pen_z:=pen_z+5
		IF pen_z>clock_prof
			pen_z:=clock_prof
		ENDIF
		G01 Z{-pen_z} F1500
		G03 X{K*(clock_diam/2-r_fresa_p)} Y0 I{K*(clock_diam/2-r_fresa_p)} J0 F20000
	END WHILE

	G01 Z2 F2000
	PARAR FRESA

	IF ac!=0
		G01 X{K*(clock_diam/2+32)} Y0  F20000
		M103
		ACTIVAR FRESA
		pen_z:=0
		WHILE pen_z<clock_prof_c
			pen_z:=pen_z+5
			IF pen_z>clock_prof_c
				pen_z:=clock_prof_c
			ENDIF
			G01 Z{-pen_z} F1500
			
		END WHILE
		G01 Z2 F2000
		PARAR FRESA
	ENDIF
	G01 Z{dsz} F{vel_z}
	M05
	