PARAMETROS pos_xp

//pos_xp es la posicion de la ranura con el desfasaje integrado


G17

// 2 mm por arriba del cero = sup de la pieza
DSZ := 2			
// plano XY
G59P12

G52 X0 Y0 Z{-maq_abajo_dz+esp_per}
// ubico la fresa en el borde izquierdo inferior de la ranura
G01 X{pos_xp+r_fresa_p} Y{ancho_per-largo}  F{vel_xy}

// activo la fresa
ACTIVAR FRESA
M04

// ranura en el plano xy
G01 Z{-prof} F{vel_pen}

// contorno de la ranura
G01 Y{ancho_per} F{vel_mec}
G01 X{pos_xp+esp-r_fresa_p} 
G01 Y{ancho_per-largo} 
G01 X{pos_xp+r_fresa_p}

// como el interior (REVISAR)
//IF esp>=4*r_fresa_p
//	G01 X{pos_xp+3*r_fresa_p-1}
//	G01 Y{ancho_per-r_fresa_p}
//ENDIF
//IF esp>=6*r_fresa_p
//	G01 X{pos_xp+5*r_fresa_p-2}
//	G01 Y{ancho_per-largo+r_fresa_p}
//ENDIF

PARAR FRESA
G01 Z{DSZ} F{vel_z}
M09

// ahora hago la ranura en el canto superior
PLANO XZ
G18
G54
// el z esta en la parte de atras de la maquina
G52 X0 Y0 Z{-maq_dz}

G01 Y{ancho_per+maq_dfresa+30} F{vel_xy}
G01 X{pos_xp+esp/2} 
G01 Z5 F{vel_z}

// LO MANDO A PALPAR
G28.1 Y{ancho_per+maq_dfresa+30-1}

G01 Y0 F{vel_xy}
G01 Z{esp_per-largo2+r_fresa_s} F{vel_z}
G01 X{pos_xp+r_fresa_s} F{vel_xy}

//activo la fresa
ACTIVAR FRESA
M03

G01 Y{-dpalpador} F{vel_mec}
G01 Y{-prof-dpalpador} F{vel_pen}
// Contorno
G01 Z{esp_per} F{vel_z}
G01 X{pos_xp+esp-r_fresa_s} F{vel_mec}
G01 Z{esp_per-largo2+r_fresa_s} F{vel_z}
G01 X{pos_xp+r_fresa_s} F{vel_mec}

// interior (REVISAR)
//IF esp>=4*r_fresa_s
//	G01 X{pos_xp+3*r_fresa_s-1}
//	G01 Z{esp_per} F{vel_z}
//ENDIF
//IF esp>=6*r_fresa_s
//	G01 X{pos_xp+5*r_fresa_s-2} F{vel_mec}
//	G01 Z{esp_per-largo2+r_fresa_s} F{vel_z}
//ENDIF

PARAR FRESA
G01 Y10 F{vel_xy}
M05

