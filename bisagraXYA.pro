PARAMETROS pos_x,K,primera


G52 X{pos_x} Z{l_fresa_p} Y{36*K}
G01 Z40 F{vel_z}
G01 X0 Y0 F{vel_xy}
IF primera
	M04
ENDIF


IF K==1
	// Puerta Derecha
	G01 X{dc-da-r_fresa_p} F{vel_xy}
	G01  Y0 
	ACTIVAR FRESA
	G01 Z{-prof} F{vel_pen}
	G01 Y{K*(dd+r_fresa_p)} F{vel_mec}
	G01 Y0 
	G01 X{dc-da-db+r_fresa_p}
	G01 Y{K*(dd+r_fresa_p)} 
	G01 X{r_fresa_p} 
	G01 X{dc-r_fresa_p} 
ELSE
	// Puerta Izquierda
	G01 X{dc-da-db+r_fresa_p} F{vel_xy}
	G01  Y0 F{vel_xy}
	ACTIVAR FRESA
	G01 Z{-prof} F{vel_pen}
	G01 Y{K*(dd+r_fresa_p)} F{vel_mec}
	G01 Y0 
	G01 X{dc-da-r_fresa_p}
	G01 Y{K*(dd+r_fresa_p)}
	G01 X{r_fresa_p} 
	G01 X{dc-r_fresa_p} 
ENDIF

PARAR FRESA
G01 Z20 F{vel_z}
