
// Agujeros Simples

G0 G49 G40 G80 G50 G90
G54
G61
G92.1
G21 (mm)
S1000
G17
PLANO XY

maq_feed:=4

// Lista de Agujeros
DSZ := maq_dz-pieza_esp-2

IF modelo < 3
	pos_z0 := -maq_dz + pieza_esp
ELSE
	pos_z0 := -maq_dz + l_fresa_p
ENDIF

IF retropalpador == 0
	//modelo nuevo del la fv1
	G01 Z5 F{vel_z}
	G01 X0 F{vel_xy}
	G01 Y0 F{vel_y}
ENDIF
// Calculo auxiliar
cant_fresa_1:=0
i:=0
WHILE i<cant_agujeros
	
	IF agujero[i].fresa==0
		cant_fresa_1:=cant_fresa_1+1
	ENDIF
	i:=i+1
END WHILE

// xxx.fresa = 0 -> Fresa #1
// xxx.fresa = 1 -> Fresa #2

// Hacemos primero los de la fresa peque�a (fresa 2, ojo xxx.fresa==1)
IF cant_agujeros-cant_fresa_1!=0
	G59P18
	G52 X0 Y0 Z{pos_z0}
	i:=0
	G01 Z1 F{vel_z}
	primero:=1	

	WHILE i<cant_agujeros
		// Agujero
		IF agujero[i].fresa==1
			G01 X{agujero[i].x} F{vel_xy}
			G01 Y{agujero[i].y} F{vel_y}
			IF primero==1
				IF modelo < 2
					M08
				ELSE
					M03
				ENDIF
				primero:=0
			ENDIF

			pen_z :=  agujero[i].penetracion
			//IF pen_z==0
				// Agujero pasante
			//	pen_z := pieza_esp + 5
			//ENDIF
			ACTIVAR FRESA
			G01 Z{-pen_z} F{vel_pen}
			G01 Z2 F{vel_z}
			PARAR FRESA

		ENDIF
		i:=i+1
	END WHILE
	
	G01 Z{dsz} F{vel_z}
	IF modelo < 2
		M09
	ELSE
		M05
	ENDIF

ENDIF


// Lista de Calados FRESA chica
i:=0
G59P18
G52 X0 Y0 Z{pos_z0}
WHILE i<cant_calados
	IF calado[i].fresa==1
		// Calado
		IF modelo == 3
			G01 X{calado[i].x0} F{vel_xy}
			G01 Y{calado[i].y0} F{vel_y}			
		ELSE
			G01 X{calado[i].x0} Y{calado[i].y0} F{vel_xy}
		ENDIF
		pen_z:=calado[i].penetracion
		IF pen_z>4
			pen_z=4
		ENDIF
		IF modelo < 2
			M08	
		ELSE
			M03
		ENDIF
		ACTIVAR FRESA
		G01 Z{-pen_z} F{vel_pen}
		G01 X{calado[i].x1} Y{calado[i].y1} F{vel_mec}

		G01 Z{dsz} F{vel_z}
		//G01 Z2 F{vel_z}
		PARAR FRESA
		IF modelo < 2
			M09
		ELSE
			M05
		ENDIF
	ENDIF
	i:=i+1
END WHILE
G54
G92.1


// Hago los agujeros de la fresa #1 (la grande)
IF cant_fresa_1!=0
	G59P17
	G52 X0 Y0 Z{pos_z0}
	i:=0
	G01 Z1 F{vel_z}
	primero:=1	

	WHILE i<cant_agujeros
		// Agujero
		IF agujero[i].fresa==0
			IF modelo == 3
				G01 X{agujero[i].x} F{vel_xy}
				G01 Y{agujero[i].y} F{vel_y}
			ELSE
				G01 X{agujero[i].x} Y{agujero[i].y} F{vel_xy}
			ENDIF
			IF primero==1
				IF modelo < 2
					M04
				ELSE
					M03
				ENDIF				
				primero:=0
			ENDIF

			pen_z :=  agujero[i].penetracion
			diam :=  agujero[i].diametro
			IF pen_z==0 
				// Agujero pasante
				pen_z := pieza_esp + 2
				IF  diam<=10
					ACTIVAR FRESA
					G01 Z{-pen_z} F{vel_pen}
					
					PARAR FRESA
				ELSE
					IF modelo == 3
						G01 X{agujero[i].x-diam/2+r_fresa_p} F{vel_xy}
						G01 Y{agujero[i].y} F{vel_y}
					ELSE
						G01 X{agujero[i].x-diam/2+r_fresa_p} Y{agujero[i].y} F{vel_xy}
					ENDIF
					z:=0
					WHILE z<pen_z
						z:=z+maq_feed
						IF z>pen_z
							z:=pen_z
						ENDIF
						G01 Z{-z}
						G02 X{agujero[i].x-diam/2+r_fresa_p} Y{agujero[i].y} I{diam/2-r_fresa_p} J0 F{vel_curvas}
					END WHILE
				ENDIF
			ELSE
				// Agujero ciego
				IF  diam<=10
					ACTIVAR FRESA
					G01 Z{-pen_z} F{vel_pen}
					
					PARAR FRESA
				ELSE
					z:=0
					WHILE z<pen_z
						z:=z+maq_feed
						IF z>pen_z
							z:=pen_z
						ENDIF
						radio:=diam/2-r_fresa_p+2*r_fresa_p
						WHILE radio>r_fresa_p
							radio:=radio-2*r_fresa_p
							IF radio<r_fresa_p
								radio:=r_fresa_p
							ENDIF
							IF modelo == 3
								G01 X{agujero[i].x-radio} F{vel_xy}
								G01 Y{agujero[i].y} F{vel_y}
							ELSE
								G01 X{agujero[i].x-radio} Y{agujero[i].y} F{vel_xy}
							ENDIF
							G01 Z{-z} F{vel_pen}
							G02 X{agujero[i].x-radio} Y{agujero[i].y} I{radio} J0 F{vel_curvas}
						END WHILE			
					// ----			
					END WHILE
					
				ENDIF
			ENDIF
			// Levanto la Fresa
			G01 Z2 F{vel_z}
		ENDIF
		i:=i+1
	END WHILE
	G01 Z{dsz} F{vel_z}
	M05

ENDIF

// Lista de Calados FRESA GRANDE
i:=0
G59P17
G52 X0 Y0 Z{pos_z0}
primera:=1

WHILE i<cant_calados
	IF calado[i].fresa==0
		// Calado
		IF modelo == 3
			G01 X{calado[i].x0} F{vel_xy}
			G01 Y{calado[i].y0} F{vel_y}
		ELSE
			G01 X{calado[i].x0} Y{calado[i].y0} F{vel_xy}
		ENDIF
		
		pen_z:=calado[i].penetracion
		IF pen_z==0 
			// pasante
			pen_z := pieza_esp + 2
		ENDIF
		z:=0
		WHILE z<pen_z
			z:=z+feed
			IF z>pen_z
				z:=pen_z		
			ENDIF

			IF primera==1
				IF modelo < 2
					M04
				ELSE
					M03
				ENDIF
				primera:=0
			ENDIF

			ACTIVAR FRESA
			G01 Z{-z} F{vel_pen}
			G01 X{calado[i].x1} Y{calado[i].y1} F{vel_mec}
			
			IF z<pen_z

				z:=z+feed
				IF z>pen_z
					z:=pen_z		
				ENDIF
			
				G01 Z{-z} F{vel_pen}
				G01 X{calado[i].x0} Y{calado[i].y0} F{vel_mec}
			ENDIF
		END WHILE			

		G01 Z2 F{vel_z}
		PARAR FRESA
	ENDIF
	i:=i+1
END WHILE
G01 Z{dsz} F{vel_z}
M05
G54
G92.1


// Lista de Clocks
i:=0
G54
G92.1

WHILE i<cant_clocks

	IF clock[i].tipo==0 .OR. clock[i].tipo==2
		// Clock Izquierdo
		PROGRAMA clock CON clock[i].x,clock[i].y,clock[i].achico,0
	ENDIF

	IF clock[i].tipo==1 .OR. clock[i].tipo==2
		// Clock derecho
		PROGRAMA clock CON clock[i].x,clock[i].y,clock[i].achico,1
	ENDIF
	i:=i+1
END WHILE



// Lista de pockets
i:=0
G54
G92.1

WHILE i<cant_pockets

	PROGRAMA pocket CON pocket[i].x,pocket[i].y,pocket[i].dx,pocket[i].dy,pocket[i].penetracion
	i:=i+1
END WHILE


G54
G92.1
G52 X0 Y0
IF modelo != 3 || retropalpador
	//FV1 con retropalpador o modelos anteriores de las maquinas
	G28.1 Z0
ENDIF
IF modelo == 3
	M05
ENDIF
G92.1
G01 Y0 F{vel_y}
G01 X{-dist_ax} F{vel_xy}
G28.1 X{-dist_ax} Y0
IF modelo == 3 && retropalpador == 0
	//FV1 palpador directo (modelo nuevo)
	G28.1 Z5
ENDIF

