PARAMETROS pos_x,K,npernio



//si es simetrico es una bisagra inclinada de las dos lados
db3:=0
IF db2==0 .OR. db2==db
	' bisagra normal	
	db2:=db
ELSE
	' bisagra inclinada	
	IF db2<2*r_fresa_s
		db2:=2*r_fresa_s
	ENDIF
	IF simetrica
		db3:=(db-db2)/2
		db2:=db2 + db3
	ENDIF
ENDIF

IF contra_puerta
	//invierto la inclinacion para simular el pernio del marco
	aux := db3
	db3 := db - db2
	db2 := db - aux
ENDIF

// bisagra para marco, maquina nueva
// mds = generalemente 1 o 2 mm 
IF de<2*r_fresa_p
	de:=2*r_fresa_p
ENDIF
df := calle
dy := esp_pta + mds
	

IF ri==0
	ri:=de/2
ENDIF
IF rd==0
	rd:=de/2
ENDIF

dpal_marco:=5

//en la fv1 vertical, la puerta de izquierda se mecaniza igual que la derecha pero espejando el Y
//por ese motivo en la puerta izquierda hago un offset al espesor de la puerta y cambio el signo de lo movimientos en Y
IF K == 1
	//Puerta derecha, el Y va normal
	pos_y0 := 0
ELSE
	//Puerta izquierda, el Y va espejado
	pos_y0:= esp_pta
ENDIF

//en la FV1 el z0 definido en la profundidad de la maquina
IF retropalpador .OR. palpador_m == 0
	//vesion vieja fv1 Retropalpa Interesa el primer offset para poder apollarse en la pieza
	pos_z0 := -maq_dz + l_fresa_p - rebaje_canto
ELSE
	//Modelo Nuevo Como ahora palpa hacia adelante no importa en que posion esta el offset 
	//total avanza hasta tocar la superficie de la placa
	pos_z0 := 0
ENDIF


'Comienzo del programa bisagra en el marco
IF npernio == 0
	G52 X{pos_x} Y{pos_y0} Z{pos_z0}
ELSE
	G52 X{pos_x} Y{pos_y0}
ENDIF

IF palpador_m==1
	// LO MANDO A PALPAR
	pp_x:=0
	PROGRAMA palpam CON pp_x,0
ENDIF

IF invisible
	PROGRAMA invisiblePXY
ELSE
	
	// Puerta Izquierda Y derecha
	IF npernio == 0 .AND. palpador_m==0
		M03
	ENDIF
	
	//cuello
	IF K == 1
		//bisagra derecha
		G01 X{dc-da-(db-db2)-r_fresa_p} F{vel_xy}
		G01 Y0 F{vel_y}
		G01 Z0.2 F{vel_z}
		G01 Z{-prof} F{vel_pen}
		ACTIVAR FRESA
		
		'comienza el mecanizado
		G01 X{dc-da-r_fresa_p} Y{K*(dd+r_fresa_p)} F{vel_mec}
		G01 X{dc-da-(db-db2)-r_fresa_p} Y0
		G01 X{dc-da-db+db3+r_fresa_p}
		G01 X{dc-da-db+r_fresa_p} Y{K*(dd+r_fresa_p)} F{vel_mec}
	ELSE
		//bisagra izquierda entra por el extremo opuesto del cuello
		G01 X{dc-da-db+db3+r_fresa_p} F{vel_xy}
		G01 Y0 F{vel_y}
		G01 Z0.2 F{vel_z}
		G01 Z{-prof} F{vel_pen}
		ACTIVAR FRESA	
		
		'comienza el mecanizado
		G01 X{dc-da-db+r_fresa_p} Y{K*(dd+r_fresa_p)} F{vel_mec}
		G01 X{dc-da-db+db3+r_fresa_p} Y0
		G01 X{dc-da-(db-db2)-r_fresa_p} F{vel_mec}
		G01 X{dc-da-r_fresa_p} Y{K*(dd+r_fresa_p)}
	ENDIF	
	//si el cuello es mayor a 28 hace una pasada mas
	IF db>4*r_fresa_p .AND. db2 == db .AND. db3==0
		'hace una pasada porque es mayor a 28
		G01 X{dc-da-3*r_fresa_p+1} F{vel_mec}	
		G01 Y0
		G01 Y{K*(dd+r_fresa_p)} F1000
	ENDIF
	

	//cuerpo de la bisagra
	IF de<=2*r_fresa_p
		// BISAGRA COMUN				
		G01 X{r_fresa_p} F{vel_mec}
		G01 X{dc-r_fresa_p}
	ELSE

		' HIBRIDA
		
		IF da>ri
			G01 X{dc-ri}
			IF K == 1
				G03 X{dc-r_fresa_p} Y{dd+ri} I0 J{ri-r_fresa_p} F{vel_curvas}
			ELSE
				G02 X{dc-r_fresa_p} Y{K*(dd+ri)} I0 J{K*(ri-r_fresa_p)} F{vel_curvas}
			ENDIF
		ELSE
			G01 X{dc-r_fresa_p}
		ENDIF
		
		IF ri<=r_fresa_p
			G01 Y{K*(dd+de-r_fresa_p)} F{vel_mec}
		ELSE
			G01 Y{K*(dd+de-ri)} F{vel_mec}
			IF K == 1
				G03 X{dc-ri} Y{dd+de-r_fresa_p} I{r_fresa_p-ri} J0 F{vel_curvas}
			ELSE
				G02 X{dc-ri} Y{K*(dd+de-r_fresa_p)} I{r_fresa_p-ri} J0 F{vel_curvas}
			ENDIF
		ENDIF
		IF rd <=r_fresa_p
			G01 X{r_fresa_p}	F{vel_mec}
			G01 Y{K*(dd+r_fresa_p)}
		ELSE
			G01 X{rd} F{vel_mec}
			IF K == 1
				G03 X{r_fresa_p} Y{de+dd-rd} I0 J{r_fresa_p-ri}	F{vel_curvas}
			ELSE
				G02 X{r_fresa_p} Y{K*(de+dd-rd)} I0 J{K*(r_fresa_p-ri)}	F{vel_curvas}
			ENDIF
			G01 X{r_fresa_p} Y{K*(dd+rd)} F{vel_mec}
			IF K == 1
				G03 X{rd} Y{dd+r_fresa_p} I{rd-r_fresa_p} J0 F{vel_curvas}
			ELSE
				G02 X{rd} Y{K*(dd+r_fresa_p)} I{rd-r_fresa_p} J0 F{vel_curvas}
			ENDIF
		ENDIF
		G01 X{dc-da-r_fresa_p} F{vel_mec}
			
	ENDIF
	
//fin del if invisible
ENDIF

PARAR FRESA
G01 Z{dsz+rebaje_canto} F{vel_mec}

IF contra_puerta
	//restauro el valor de db2 para futuros calculos
	aux := db3
	db3 := db - db2
	db2 := db - aux
ENDIF
	
	
IF palpador_m==1 .AND. npernio != cant_pernios-1
	//  apaga la fresa para poder palpar y que se oculte el palpador
	M05
ENDIF

'fin de la bisagra en el marco



