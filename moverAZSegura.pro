// FUNCION moverAZSegura
	(mover a z segura)
	// pos_z_segura es una pos. absoluta (le voy a restar el offset de z)
	pos_z_segura := dsz + pos_pren_z
	IF pos_z_segura > 0
		pos_z_segura := 0
	ENDIF
	G01 Z{pos_z_segura - p_offset_z} F{vel_Z}
