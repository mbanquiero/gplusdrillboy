(CHAPITA)


// variables de la drillboy
PROGRAMA initVariables

// espesor del marco
//esp_marco:=10
// posicion de presando
pos_z0 := -maq_dz + esp_marco
pos_pren_z := (pos_z0+dist_prensor_z+drot)

PLANO XY
G49 G40 G80 G50 G61 G90
G54
G92.1
G21 
S1000
G17


G01 Y{pos_y_inicial} F{vel_xy} 
IF mano_pta==1
	// puerta izquierda
	XG01 X{dp-pos_pinza}
	// puerta izquierda , el cero esta en el tope izquierdo
	// el exe va transformar las posiciones  
	// G52 X{pos_x} ---->  G52 X{pos_abs_x0 - pos_x}
	// G01 X{pos_x} ---->  G01 X{-pos_x}
	pos_abs_x0 := dp - 2*offset
	(ESPEJAR_X = {pos_abs_x0})
ELSE	
	G01 X{-offset}
ENDIF

(Posicion inicial de la pinza)
M20
G4 P{pausa}
G01 Z{pos_desac_tope-p_offset_z} F{vel_Z}
G52 X{-pos_pinza} Y0 Z{pos_z0}
(POS_PINZA = {pos_pinza})

//  moverAYSegura
pos_y_segura := 60
G01 Y{pos_y_segura} F{vel_xy}
G52 X{-pos_pinza+desf_x0} Y0 Z{-maq_dz+esp_marco}

(DESF_Y={-fn_dfy})
(DESF_Z={-fn_dfz})

// mdl = calle
// mrc = curvatura para la chapita sin cuello (hibrida)
// mrc = Inclinacion para la chapita con cuello

inc_cuello := 0
IF mdk!=0 .AND. mdc!=mda
	//si tiene cuello la inclinacion es igual al mrc
	inc_cuello := mrc
ENDIF

dy:=esp_pta + mds
//mdl:=dy-mdd-mdb
//
//centro:=(esp_pta+mde)/2+mdg - luz - dcentro

IF calle_chapita == 0
	mdl:=(esp_pta+mds)-mdd-mdb
ELSE
	mdl:=calle_chapita
ENDIF
//si mdb es igual a cero la chapita no lleva cuello


// me posiciono en la pos. X de la chapita	
G52 X{-pos_pinza+desf_x0} 
IF largo_espiga>0
	pos_x := pos_picaporte+mdp+ fn_dfx + mdm -esp_marco  + largo_espiga + desf_tope_marco
ELSE
	pos_x := pos_picaporte+mdp+mdm + fn_dfx + desf_tope_marco
ENDIF

IF mano_pta==1
	pos_x := pos_x + desf_izq_chapita
ENDIF
PROGRAMA moverAZSeguraChapita
G01 Y{pos_y_segura} F{vel_xy}
PROGRAMA moverPinzaPos CON pos_x + mda/2 + mdc-mda + 60

'Comienzo del programa chapita en el marco
G52 X{-pos_pinza+desf_x0+pos_x} Y{dy+fn_dfy} Z{-maq_dz+esp_marco+ fn_dfz+ l_fresa_p}

// prendo el motor
M502
G04 p1
// issue GPL-20
G01 Z{pos_desac_tope-p_offset_z} F{vel_Z}
IF mdb!=0
	G01 X{(-mda/2+r_fresa_p)} Y{-dy} F{vel_xy}
ELSE
	G01 X{(-mda/2+r_fresa_p)} Y{-(mdl+mdd-r_fresa_p)} F{vel_xy}
ENDIF
'comienza el mecanizado
G01 Z0.2 F{vel_z}
G01 Z{-mdi} F{vel_pen}
ACTIVAR FRESA

IF mdk!=0 .AND. mdc!=mda
	//chapita con cuello
	// el usuario ingreso mdk = dist al comienzo del cuello
	// pero yo quiero la distancia al centro del cuello => hago las correciones
	IF mdk==0
		// simetrica
		mdk:=mdc/2
	ELSE
	// transformo para que mdk = distancia al eje del cuello
		mdk:=mdk+mda/2
	ENDIF
	
	IF mdb!=0
		'perimetro del cuello
		G01 X{-(mda/2-r_fresa_p)} Y{-(mdl+mdd-r_fresa_p)} F{vel_mec}
		G01 X{(mda/2-r_fresa_p)}
		G01 X{(mda/2-r_fresa_p - inc_cuello)} Y{-dy}
		G01 X{-(mda/2-r_fresa_p - inc_cuello)}
		'interior del cuello
		G01 Y{-dy+(2*r_fresa_p-1)}
		G01 X{(mda/2-r_fresa_p)}
	ENDIF
	  
	//me posiciono para hacer el cuerpo
	G01 Y{-(mdl+mdd-r_fresa_p)}
	  
	'perimetro del cuerpo de la chapita
	G01 X{-(mdk-mdd/2)}
	G02 X{-(mdk-mdd/2)} Y{-(mdl+r_fresa_p)} I0 J{(mdd-2*r_fresa_p)/2} F{vel_curvas}
	G01 X{(mdc-mdk-mdd/2)} F{vel_mec}
	G02 X{(mdc-mdk-mdd/2)} Y{-(mdl+mdd-r_fresa_p)} I0 J{-(mdd-2*r_fresa_p)/2} F{vel_curvas}
	IF mdb!=0
		G01 X0 F{vel_mec}
	ELSE
		G01 X{-(mdk-mdd/2 - r_fresa_p)}
	ENDIF
	  
	IF mdd>4*r_fresa_p
		' como la isla central
		G01 Y{-(mdl+mdd/2)} F{vel_mec}
		G01 X{-(mdk-r_fresa_p)}
		G01 X{(mdc-mdk-r_fresa_p)}
		G01 X0
	ENDIF
	PARAR FRESA

	' 1er vaciado
	IF mde<=2*r_fresa_p
		' vaciado simple
		G01 X{(-mdf/2+r_fresa_p)} Y{-(mdl+mdg+r_fresa_p)} F{vel_xy}
		ACTIVAR FRESA
		pen_z:=mdi
		WHILE pen_z<mdh
			pen_z:=pen_z+feed
			IF pen_z>mdh
				pen_z:= mdh
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
			G01 X{(mdf/2-r_fresa_p)} F{vel_mec}
			IF pen_z<mdh
				pen_z:=pen_z+feed
				IF pen_z>mdh
					pen_z:= mdh
				ENDIF
				G01 Z{-pen_z} F{vel_pen}
				G01 X{(-mdf/2+r_fresa_p)} F{vel_mec}
			ENDIF
		END WHILE
	ELSE
		'Cajeado rectangular {maximo 28mm}
		G01 X{(-mdf/2+r_fresa_p)} Y{-(mdl+mdg+r_fresa_p)} F{vel_xy}
		ACTIVAR FRESA
		pen_z:=mdi
		WHILE pen_z<mdh
			pen_z:=pen_z+feed
			IF pen_z>mdh
				pen_z:= mdh
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
			G01 X{(mdf/2-r_fresa_p)} F{vel_mec}
			G01 Y{-(mdl+mdg+mde-r_fresa_p)}
			G01 X{(-mdf/2+r_fresa_p)}
			G01 Y{-(mdl+mdg+r_fresa_p)}
		END WHILE
	ENDIF

	PARAR FRESA
	G01 Z{dsz} F{vel_z}

	IF mdj!=0
		' Segundo vaciado
		IF mde2<=2*r_fresa_p
			' vaciado simple
			G01 X{(mdf/2+mdj+r_fresa_p)} Y{-(mdl+mdg2+r_fresa_p)} F{vel_xy}
			ACTIVAR FRESA
			G01 Z0.2 F{vel_z}
			pen_z:=mdi
			WHILE pen_z<mdh2
				pen_z:=pen_z+feed
				IF pen_z>mdh2
					pen_z:= mdh2
				ENDIF
				G01 Z{-pen_z} F{vel_pen}
				G01 X{(mdf/2+mdj+mdf2-r_fresa_p)} F{vel_mec}
				IF pen_z<mdh2
					pen_z:=pen_z+feed
					IF pen_z>mdh2
						pen_z:= mdh2
					ENDIF
					G01 Z{-pen_z} F{vel_pen}
					G01 X{(mdf/2+mdj+r_fresa_p)} F{vel_mec}
				ENDIF
			END WHILE
		ELSE
			' Cajeado rectangular {maximo 28mm}
			G01 X{(mdf/2+mdj+r_fresa_p)} Y{-(mdl+mdg2+r_fresa_p)} F{vel_xy}
			ACTIVAR FRESA			  
			G01 Z0.2 F{vel_z}
			pen_z:=mdi
			WHILE pen_z<mdh2
				pen_z:=pen_z+feed
				IF pen_z>mdh2
					pen_z:= mdh2
				ENDIF
				G01 Z{-pen_z} F{vel_pen}
				G01 X{(mdf/2+mdj+mdf2-r_fresa_p)} F{vel_mec}
				G01 Y{-(mdl+mdg2+mde2-r_fresa_p)} 
				G01 X{(mdf/2+mdj+r_fresa_p)}
				G01 Y{-(mdl+mdg2+r_fresa_p)} 
			END WHILE
		ENDIF

		'Finaliza el programa chapita
		PARAR FRESA
		G01 Z{dsz} F{vel_z}
	ENDIF
ELSE
	'chapita sin cuello
	IF mrc<r_fresa_p
		mrc:=r_fresa_p
	ENDIF
	mdl:=dy-mdd
	
	IF msentido=0
		'sentido horario
		'como el perimetro
		G01 Y{-(mdl+mrc)} F{vel_mec}
		G02 X{-(mda/2-mrc)} Y{-(mdl+r_fresa_p)} I{-(-mrc+r_fresa_p)} J0 F{vel_curvas}
		G01 X{(mda/2-mrc)} F{vel_mec}
		G02 X{(mda/2-r_fresa_p)} Y{-(mdl+mrc)} I0 J{-mrc+r_fresa_p} F{vel_curvas}
		G01 Y{-dy} F{vel_mec}
		G01 X{-(mda/2-r_fresa_p)}
	ELSE
		'sentido normal antihorario
		'como el perimetro de la chapita sin cuello
		G01 X{(mda/2-r_fresa_p)} F{vel_mec}
		G01 Y{-(mdl+mrc)}
		G03 X{(mda/2-mrc)} Y{-(mdl+r_fresa_p)} I{-(mrc-r_fresa_p)} J0	F{vel_curvas}
		G01 X{-(mda/2-mrc)} F{vel_mec}
		G03 X{-(mda/2-r_fresa_p)} Y{-(mdl+mrc)} I0 J{-mrc+r_fresa_p} F{vel_curvas}
		G01 Y{-dy} F{vel_mec}
	ENDIF
	
	IF mdd>3*r_fresa_p-1
	  IF mdd>5*r_fresa_p-2
			G01 Y{-dy+r_fresa_p*2-1}
			G01 X{(mda/2-r_fresa_p*2+1)}
		ELSE
			G01 Y{-(mdl+r_fresa_p*3-1)}
			G01 X{(mda/2-r_fresa_p*2+1)}
		ENDIF
	ENDIF
	
	IF mdd>5*r_fresa_p-2
		IF mdd>7*r_fresa_p-3
			G01 Y{-dy+r_fresa_p*4-2}
			G01 X{-(mda/2-r_fresa_p*2+1)}
		ELSE
			G01 Y{-(mdl+r_fresa_p*3-1)}
			G01 X{-(mda/2-r_fresa_p*2+1)}
		ENDIF
	ENDIF
	
	IF mdd>7*r_fresa_p-3
		IF mdd>9*r_fresa_p-4
			G01 Y{-dy+r_fresa_p*6-3}
			G01 X{(mda/2-r_fresa_p*2+1)}
		ELSE
			G01 Y{-(mdl+r_fresa_p*3-1)}
			G01 X{(mda/2-r_fresa_p*2+1)}
		ENDIF
	ENDIF

	PARAR FRESA
	
	' 1er vaciado 
	IF mde<=2*r_fresa_p
		' vaciado simple
		G01 X{(-mdf/2+r_fresa_p)} Y{-(mdl+mdg+r_fresa_p)} F{vel_xy}
		ACTIVAR FRESA
		G01 Z0.2 F{vel_z}
		pen_z:=mdi
		WHILE pen_z<mdh
			pen_z:=pen_z+feed
			IF pen_z>mdh
				pen_z:= mdh
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
			G01 X{(mdf/2-r_fresa_p)} F{vel_mec}
			IF pen_z<mdh
				pen_z:=pen_z+feed
				IF pen_z>mdh
					pen_z:= mdh
				ENDIF
				G01 Z{-pen_z} F{vel_pen}
				G01 X{(-mdf/2+r_fresa_p)} F{vel_mec}
			ENDIF
		END WHILE
	ELSE
		' Cajeado rectangular {maximo 28mm}
		G01 X{(-mdf/2+r_fresa_p)} Y{-(mdl+mdg+r_fresa_p)} F{vel_xy}
		ACTIVAR FRESA
		G01 Z0.2 F{vel_z}
		pen_z:=mdi
		WHILE pen_z<mdh
			pen_z:=pen_z+feed
			IF pen_z>mdh
				pen_z:= mdh
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
			G01 X{(mdf/2-r_fresa_p)} F{vel_mec}
			G01 Y{-(mdl+mdg+mde-r_fresa_p)} 
			G01 X{(-mdf/2+r_fresa_p)}
			G01 Y{-(mdl+mdg+r_fresa_p)}
		END WHILE
	ENDIF

	'Finaliza el programa chapita
	PARAR FRESA
	G01 Z{dsz} F{vel_z}
ENDIF

// apago el motor
M503
PARAR FRESA
	
PROGRAMA moverAZSeguraChapita
			
// finalizacion
M189

//  moverAYSegura
G01 Y{pos_y_segura} F{vel_xy}

// FINALIZAR PIEZA
//  terminar el trabajo ahora:
// entonces retiro la pieza y finalizo todo
PROGRAMA retirarPieza

	
