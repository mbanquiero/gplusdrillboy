//pb_mdm -> Desfasaje del marco en relacion a la puerta
// Pero pb_marco le pasa la medida ya desfasada (pos_x ya tiene el desfasaje)	

(VACIADO DE MARCO)
// variables de la drillboy
PROGRAMA initVariables

// espesor del marco
//esp_marco:=10
// posicion de presando
pos_z0 := -maq_dz + esp_marco
pos_pren_z := (pos_z0+dist_prensor_z+drot)

// mdm = compensacion del marco con respecto a la puerta

PLANO XY
G49 G40 G80 G50 G61 G90
G54
G92.1
G21 
S1000
G17

rot_0:=3
G01 Y{pos_y_inicial} F{vel_xy}
IF mano_pta==1
	// puerta izquierda
	XG01 X{dp-pos_pinza}
	// puerta izquierda , el cero esta en el tope izquierdo
	// el exe va transformar las posiciones  
	// G52 X{pos_x} ---->  G52 X{pos_abs_x0 - pos_x}
	// G01 X{pos_x} ---->  G01 X{-pos_x}
	pos_abs_x0 := dp - 2*offset
	(ESPEJAR_X = {pos_abs_x0})
ELSE	
	G01 X{-offset}
ENDIF

(Posicion inicial de la pinza)
M20
G4 P{pausa}
G01 Z{pos_desac_tope-p_offset_z} F{vel_Z}
G52 X{-pos_pinza} Y0 Z{pos_z0}
(POS_PINZA = {pos_pinza})

//  moverAYSegura
pos_y_segura := 60
G01 Y{pos_y_segura} F{vel_xy}
G52 X{-pos_pinza+desf_x0} Y0 Z{-maq_dz+esp_marco}

(DESF_Y={-fn_dfy})
(DESF_Z={-fn_dfz})


// Cerradura del marco
IF vaciados!=0
	// Hay 3 vaciados o 5, el del medio es grande y el resto peque�os
	t:=0	
	vg := if(cant_vaciados==3,1,2)
	WHILE t<cant_vaciados
		dv:=if(t==vg,pb_dvg,pb_dvc)
		fdv:=if(t==vg,pb_fdvg,pb_fdvc)
		IF largo_espiga>0
			// mdm  = esp_marco + luz => luz = mdm - esp_marco
			pos_x := pb_pos_v[t]+pb_mdm-esp_marco  + largo_espiga + desf_tope_marco
		ELSE
			pos_x := pb_pos_v[t]+pb_mdm + desf_tope_marco
		ENDIF

		IF pb_frente			
			// busco la posicion X
			PROGRAMA moverPinzaPos CON pos_x -(fdv-dv)/2-desf_x0
			// me posicion en X pos del pernio
			G01 X{pos_x} F{vel_pinza}
			PROGRAMA ranura_frente CON pos_x-(fdv-dv)/2-desf_x0,K,fdv,pb_fesp,pb_fprof
		ENDIF
		
		(VACIADO : {t+1} de {cant_vaciados})
		// busco la posicion X
		PROGRAMA moverPinzaPos CON pos_x + dv + 50
		// me posicion en X pos del pernio
		G01 X{pos_x} F{vel_pinza}
		
		PROGRAMA cajeadoXY CON pos_x,dv,pb_esp_vaciado,pb_prof_vaciado		
		
		IF t==vg .AND. pb_dxvp>0
			// vaciado del pestillo
			PROGRAMA cajeadoXY CON pos_x-pb_sep_vp-pb_dxvp,pb_dxvp,pb_esp_vaciado,pb_prof_vaciado		
		ENDIF
		
		// restauro el offset de Y para que en el siguietne paso se mueva OK a la posicion Y segura
		G52 Y0
		
		t:=t+1
	END WHILE

ENDIF


PROGRAMA moverAZSegura
			
// finalizacion
M189

//  moverAYSegura
G01 Y{pos_y_segura} F{vel_xy}

// FINALIZAR PIEZA
//  terminar el trabajo ahora:
// entonces retiro la pieza y finalizo todo
PROGRAMA retirarPieza
