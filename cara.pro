//distacia de seguridad en z

pos_z0:=-maq_dz+pieza_dz
pos_rot:=pos_z0+l_fresa+dsz
//f_rot:=25000
//rot_0:=3
// distancia entre la base de la fresa y el eje de rotacion
//drot:=57.5
// desfasaje entre el fresador alta frecuencia (=auxiliar) que se usa para calibrar el cero
// y el fresador rotador
//dfx:=70
//dfy:=130
//dfz:=37

offset_x:=0
IF campo==1
	offset_x := maq_topeB - pieza_dx
ENDIF

dfx:=fresador[fresa].dfx
dfy:=fresador[fresa].dfy
dfz:=fresador[fresa].dfz
//dz_canto_d:=1
//dy_canto_d:=2


// Inicializacion de mecanizado de Caras
	// operacion
	// 0-> si esta inicializando
	// 1-> si esta retirando (finalizando)
	// 2-> cambio de herramientas en la misma cara


IF cara==0
	// pos normal
	M110
	
	IF operacion==0
		/* inicializacion Plano XY */
		
		// roto el cabezal y prendo el fresador
		IF fresa==0
			G01 A90	F{vel_rot}
			G52 X{-dfx} Y{dfy} Z{pos_z0+l_fresa+drot-dfz}
		ENDIF
		IF fresa==1
			G01 A270 F{vel_rot}
			G52 X{-dfx+dcx} Y{dfy+dcy} Z{pos_z0+l_fresa+drot-dfz}
		ENDIF
		IF fresa==2
			G28.1 A{rot_0}
			G01 A0 F{vel_rot}
			FRESA_ON(2)
			G52 X0 Y0 Z{pos_z0+l_fresa}
		ENDIF
		IF fresa==3
			G28.1 A{rot_0}
			G01 A0 F{vel_rot}
			G52 X{-dfx} Y{dfy} Z{pos_z0+l_fresa-dfz}
		ENDIF

	ENDIF

	IF operacion==2
		// cambio de herramienta (ya esta pos. en el plano XY)
		IF fresa==1
			G01 A270 F{vel_rot}
			G52 X{-dfx} Y{dfy} Z{pos_z0+l_fresa+drot-dfz}
		ENDIF
		IF fresa==2
			// Apago el fresador anterior
			FRESA_OFF(ant_fresa)
			G28.1 A{rot_0}
			G01 A0 F{vel_rot}
			// prendo el nuevo
			FRESA_ON(fresa)
			G52 X0 Y0 Z{pos_z0+l_fresa}
		ENDIF
		IF fresa==3
			// Apago el fresador anterior
			FRESA_OFF(ant_fresa+1-1)
			G28.1 A{rot_0}
			G01 A0 F{vel_rot}
			// prendo el nuevo
			FRESA_ON(fresa)
			G52 X{-dfx} Y{dfy} Z{pos_z0+l_fresa-dfz}
		ENDIF
		
	ENDIF

	
	IF operacion==1
		// Retiro (si fresa<>-1, es retiro de fresa, que no hago nada, el GCAD siempre
		// deja la fresa en pos segura luego de cada objeto)
		IF fresa==-1
			/* retiro del plano XY */
			// apago la ultima fresa activa
			IF ant_fresa==-1
				FRESA_OFF(0)
			ELSE
				FRESA_OFF(ant_fresa)
			ENDIF
			G54
			G92.1
			G28.1 A{rot_0}
			G01 A0 F{vel_rot}
			G01 Z0 F{vel_z}
		ENDIF
	ENDIF	
ENDIF

IF cara==2
	// Canto izquierdo
	M111
	
	IF operacion==0
		/* entrada canto izquierdo */
		IF fresa==0
			//fresa uno no necesitar rotar tiene que estar en su posicion normal
			//pero como el referenciado no lo deja en A0 exacto
			G01 A0 F{vel_rot}
			G52 X{-l_fresa-drot-dfx} Y{dfy} Z{pos_z0-dfz}
		ELSE
			//roto 180 para usar esta fresa
			G01 A180 F{vel_rot}
			G52 X{-l_fresa-drot-dfx} Y{dfy+dccy} Z{pos_z0-dfz+dccz}
		ENDIF
		// roto el cabezal grafico
		G01 X{offset_x-dsx} F{vel_xy}
		G01 Z{-pieza_dz/2} F{vel_z}
		// la fresa la enciende el primer objeto

	ENDIF

	IF operacion==2
		/* Cambio de herramientas en canto izquierdo */
		// solo hay que rotar, en un lugar seguro de X y de Y
		df := abs(LF1-LF2)
		G01 X{offset_x-dsx-df} Y190 F{vel_xy}
		// como solo hay 2 fresas, si cambia de fresa es la fresa 2
		G01 A180 F{vel_rot}

		G52 X{-l_fresa-drot-dfx} Y{dfy} Z{pos_z0-dfz}
		G01 X{offset_x-dsx} F{vel_xy}
		G01 Z{-pieza_dz/2} F{vel_z}
	ENDIF
		
	IF operacion==1 .AND. fresa==-1
		/* retiro canto izquierdo */
		FRESA_OFF(0)
		G54
		G92.1
		// me retiro en Z
		G01 Z0 F{vel_z}
		// dejo en pos normal
		G28.1 A{rot_0}
		G01 A0 F{vel_rot}
		G01 Z0 F{vel_z}
	ENDIF	
ENDIF

IF cara==3
	// Canto derecho
	M112
	IF operacion==0
		/* entrada canto derecho */
		IF fresa==0
			G1 A180 F{vel_rot}
			G52 X{l_fresa+drot-dfx} Y{dfy+dy_canto_d} Z{pos_z0-dfz+dz_canto_d}
		ELSE
			// pos. normal, corrijo error de refenciado
			G1 A0 F{vel_rot}
			G52 X{l_fresa+drot-dfx} Y{dfy+dy_canto_d-dccy} Z{pos_z0-dfz+dz_canto_d-dccz}
		ENDIF
		
		G01 X{offset_x+pieza_dx+dsx} F{vel_xy}
		G01 Z{-pieza_dz/2} F{vel_z}
	ENDIF

	IF operacion==2
		/* Cambio de herramientas en canto derecho */
		// solo hay que rotar, en un lugar seguro de X 
		df := abs(LF1-LF2)
		G01 X{offset_x+pieza_dx+dsx+df} Y190 F{vel_xy}
		// como solo hay 2 fresas, si cambia de fresa es la fresa 2
		// y tiene que ir en pos normal (A0)
		G28.1 A{rot_0}
		G01 A0 F{vel_rot}

		G52 X{l_fresa+drot-dfx} Y{dfy+dy_canto_d} Z{pos_z0-dfz+dz_canto_d}
		G01 X{offset_x+pieza_dx+dsx} F{vel_xy}
		G01 Z{-pieza_dz/2} F{vel_z}
	ENDIF
		
	IF operacion==1 .AND. fresa==-1
		/* retiro canto derecho */
		FRESA_OFF(0)
		G54
		G92.1
		// me retiro en Z
		G01 Z0 F{vel_z}
		// dejo en pos normal
		G28.1 A{rot_0}
		G01 A0 F{vel_rot}
		G01 Z0 F{vel_z}
	ENDIF	
ENDIF
