
// Cajeado de la espiga (pos_y = centro de la espiga)
PARAMETROS pos_x,pos_y,ancho,esp,prof

//por el momento prefieren que salga una linea en la mitad
//IF esp<2*r_fresa_ps
	esp:=2*r_fresa_ps
//ENDIF

G52 X{pos_x} Y{pos_y}

IF esp <= 2*r_fresa_ps
	// Cajeado simple (va y viene)

	G01 X{r_fresa_ps} Y0 F{vel_xy}

	pen_z:=0
	lado:=0
	G01 Z1 F{vel_z}

	ACTIVAR FRESA
	WHILE pen_z < prof
		pen_z:=pen_z+feed
		IF pen_z>prof
			pen_z:= prof
		ENDIF			
		G01 Z{-pen_z} F{vel_pen}
		IF lado ==0
			G01 X{ancho-r_fresa_ps} F{vel_mec}
			lado:=1
		ELSE
			G01 X{r_fresa_ps} F{vel_mec}
			lado:=0
		ENDIF
	END WHILE
ELSE
	// Cajeado mayor al diametro de la fresa
	r_espiga:=esp/2

	G01 X{r_espiga+r_fresa_ps} Y{-esp/2+r_fresa_ps} F{vel_xy}

	pen_z:=0
	flag:=0
	G01 Z1 F{vel_z}
	ACTIVAR FRESA

	WHILE pen_z < prof
		
		pen_z:=pen_z+feed
		IF pen_z>prof
			pen_z:= prof
		ENDIF

		G01 Z{-pen_z} F{vel_pen}
		G01 X{r_espiga} F{vel_mec}
		G02 X{r_espiga} Y{esp/2-r_fresa_ps} I0 J{r_espiga-r_fresa_ps} F{vel_curvas}
		G01 X{ancho-r_espiga} F{vel_mec}
		G02 X{ancho-r_espiga} Y{-esp/2+r_fresa_ps} I0 J{-(r_espiga-r_fresa_ps)} F{vel_curvas}
		G01 X{r_espiga} F{vel_mec}
			
	END WHILE
	
ENDIF
PARAR FRESA

G01 Z5


