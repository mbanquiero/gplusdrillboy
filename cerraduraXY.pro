#var op mano_pta := 0	{Derecha,Izquierda}		// Mano de la Puerta
#var mm pos_picaporte:= 1015		// Pos del Picaporte:

#var mm cda:= 165.5		// Largo del Frente:
#var mm cdb:= 21.5		// Ancho del Frente:
#var mm cdc:= 98			// Largo de la Caja:
#var mm cdd:= 79.2		// Profundidad de la Caja:
#var mm cde:= 16			// Ancho de la Caja:
#var mm cdf:= 14			// Dist. borde al centro del picaporte
#var mm cdg:= 70			// Dist. entre Centro condena:
#var mm cdh:= 35			// Dist. del borde a la Caja:
#var mm cdi:= 51			// Altura del Picaporte:
#var mm cdj:= 22			// Diametro del Picaporte:
#var mm cdk:= 18			// Diametro de la condena:
#var mm cdl:= 3.7			// Espesor de la Cerradura:

#var mm maq_dz:= 140			// prof maquina:
#var mm esp_pta:= 33		// Espesor de la Puerta :
#var mm feed:= 4		// Profundidad x pasada:

#var sn circular:= 1		// Cajeado Circular
#var mm diam:= 25		// Diametro del Cajeado


//Las primeras versiones de la fv1 no palpan directo, sino que retropalpan.
//La version Nueva palpa directo esto es a pedido de marcelo el 16/4/09. 
//La variable retropalpador sirve para diferenciar las 2 versiones de la maquina (si vale 1 retro_palpa, sino es la version nueva)

//desf_vir es el desfasaje que se le hace a la parte inferior de los cajeado por la acumulacion de viruta
//en la parte inferior del cajeado queda como si fuera una rampa porque la diferencia de altura entre el frente(profundidad=0) del cajeado
//y en la profundidad total es de desf_vir
desf_vir:=2

// maq_dx2 = desfasaje en el eje x en parte de abajo (usualmente se usa en la pta izquierda)
// en la pta derecha = 0, se lo aplico a la pos. del picaporte
pos_picaporte:=pos_picaporte+maq_dx2
pb_rebaje_canto:=rebaje_canto

G49 G40 G80 G50 G61 G90
G54
G92.1
G21 (mm)
S1000

PLANO XZ
G17

IF mano_pta==1
	K:=-1	
ELSE
	K:=1
ENDIF

//da la posicion del tope en x que se va a usar
pos_x0 := 0
IF retropalpador
	//vesion vieja fv1 Retropalpa Interesa el primer offset para poder apollarse en la pieza
	pos_z0 := -maq_dz+l_fresa_p - rebaje_canto
ELSE
	//Modelo Nuevo Como ahora palpa hacia adelante no importa en que posion esta el offset 
	//total avanza hasta tocar la superficie de la placa
	pos_z0 := 0
ENDIF
IF K == 1
	pos_y0 := esp_pta
ELSE
	pos_y0 := 0
ENDIF

dpalpador_z := dpalpador_z 

G52 Z0
IF retropalpador
	//retro palpador
	G01 Z0 F{vel_z}
ENDIF

//invierte la posicion de la cerradura, para que tome la posicion desde "arriba"porque el cero ahora //esta abajo
pos_picaporte := alto_pta - pos_picaporte
// Cerradura

G52 X{pos_x0+pos_picaporte-cda+cdf} Y{pos_y0} Z{pos_z0}
	

// la cerradura esta desplazada del centro = esp_pta/2
aux_y:=(esp_pta-cdb)/2+r_fresa_p+dcentro
aux_j:=(cdb-2*r_fresa_p)
aux_arab:=0

// me posiciono cerca del punto de entra. Por un prob. mecanico, conviene
// penetran en diaganol y no direcamente entrar en el pto exacto de entrada
// por eso al aux_z, se le resta 2 mm (1)
IF sentido
	G01 X{(cda-cdb/2-10)} F{vel_xy}
		
	G01 Y{-K*(aux_y+2)} F{vel_xy}
	
	IF retropalpador
		//Va hasta 1mm antes de tocar la placa con la fresa y el palpador queda apollado sobre la superficie de la placa
		//Cuando se haga la instruccion G28.1 Z1 la fresa se va a alejar de la placa buscando el Z0 y cuando el palpador 
		//Empieza a perder contanto con la placa se activa el switch que le miente al sistema diciendole que toco el 0 absoluto de la maquina
		G01 Z1 F{vel_z}
	ENDIF
	G28.1 Z1
	G52 Z{dpalpador_z}
		
	G01 Z2 F{vel_z}
	
	//enciendo la fresa para esconder el palpador
	M03	
ELSE
	G01 X{(cdb/2+10)} F{vel_xy}
	
	G01 Y{-K*(aux_y+2)} F{vel_xy}
	
	IF retropalpador
		//Va hasta 1mm antes de tocar la placa con la fresa y el palpador queda apollado sobre la superficie de la placa
		//Cuando se haga la instruccion G28.1 Z1 la fresa se va a alejar de la placa buscando el Z0 y cuando el palpador 
		//Empieza a perder contanto con la placa se activa el switch que le miente al sistema diciendole que toco el 0 absoluto de la maquina
		G01 Z1 F{vel_z}
	ENDIF
	G28.1 Z1
	G52 Z{dpalpador_z}
		
	G01 Z2 F{vel_z}
	//enciendo la fresa para esconder el palpador
	M03
	
ENDIF
G01 Y{-K*(aux_y+2)} F{vel_y}

//G01 Z1 F{vel_z}

IF tipo_cerradura <= 1
	//frente de cerradura con extremos en media ca�a
	IF sentido
		'como el perimetro en el sentido horario
		// (1) Entra en el pto de entrada, interpolando y tambien, para no entrar tan recto
		G01 X{(cda-cdb/2)} Z{-cdl} F{vel_pen}
		ACTIVAR FRESA
		G01 Y{-K*aux_y} F{vel_mec}
	
		IF mano_pta==0
			G02 X{cda-cdb/2} Y{-K*(aux_y+aux_j)} I0  J{-K*(aux_j/2)} F{vel_curvas}
		ELSE
			G03 X{(cda-cdb/2)} Y{(aux_y+aux_j)} I0  J{(aux_j/2)} F{vel_curvas}
		ENDIF
		G01 X{cdb/2} F{vel_mec}
		IF mano_pta==0
			G02 X{(cdb/2)} Y{-K*aux_y} I0  J{K*aux_j/2} F{vel_curvas}
		ELSE
			G03 X{(cdb/2)} Y{aux_y} I0  J{K*aux_j/2} F{vel_curvas}
		ENDIF
		G01 X{(cda-cdb/2)} F{vel_mec}
	
		G01 Y{K*(-aux_y+aux_arab)} F{vel_mec}
	
		IF doblep_c
			//se agrego una doble pasada por si la madera se expande luego de la primera pasada
			'Segunda Pasada al frente de la cerradura
			IF mano_pta==0
				G02 X{cda-cdb/2} Y{-K*(aux_y+aux_j)} I0  J{-K*(aux_j/2)} F{vel_curvas}
			ELSE
				G03 X{(cda-cdb/2)} Y{-K*(aux_y+aux_j)} I0  J{(aux_j/2)} F{vel_curvas}
			ENDIF
			G01 X{cdb/2} Z{-cdl} F{vel_pen}
			IF mano_pta==0
				G02 X{(cdb/2)} Y{-K*aux_y} I0  J{K*aux_j/2} F{vel_curvas}
			ELSE
				G03 X{(cdb/2)} Y{-K*aux_y} I0  J{K*aux_j/2} F{vel_curvas}
			ENDIF
			G01 X{(cda-cdb/2)} F{vel_mec}
		ENDIF
	
		G01 X{cdb/2} F{vel_mec}
	
	ELSE
	
		'como el perimetro en el sentido antihorario {normal}
	
		G01 X{cdb/2} Z{-cdl} F{vel_pen}
		ACTIVAR FRESA
		G01 Y{-K*aux_y} F{vel_y}
		IF mano_pta==0
			G03 X{cdb/2} Y{-K*(aux_y+aux_j)} I0  J{-K*(aux_j/2)} F{vel_curvas}
		ELSE
			G02 X{cdb/2} Y{-K*(aux_y+aux_j)} I0  J{-K*(aux_j/2)} F{vel_curvas}
		ENDIF
		G01 X{(cda-cdb/2)} F{vel_mec}
		IF mano_pta==0
			G03 X{(cda-cdb/2)} Y{-K*aux_y} I0  J{K*aux_j/2} F{vel_curvas}
		ELSE
			G02 X{(cda-cdb/2)} Y{-K*aux_y} I0  J{K*aux_j/2} F{vel_curvas}
		ENDIF
		G01 Y{K*(-aux_y+aux_arab)} F{vel_mec}
		G01 X{cdb/2} F{vel_mec}
	
		IF doblep_c
			//se agrego una doble pasada por si la madera se expande luego de la primera pasada
			'Doble pasada al frente de la cerradura
			IF mano_pta==0
				G03 X{cdb/2} Y{-K*(aux_y+aux_j)} I0 J{-K*(aux_j/2)} F{vel_curvas}
			ELSE
				G02 X{cdb/2} Y{-K*(aux_y+aux_j)} I0 J{-K*(aux_j/2)} F{vel_curvas}
			ENDIF
			G01 X{(cda-cdb/2)} F{vel_mec}
			IF mano_pta==0
				G03 X{(cda-cdb/2)} Y{-K*aux_y} I0 J{K*aux_j/2} F{vel_curvas}
			ELSE
				G02 X{(cda-cdb/2)} Y{-K*aux_y} I0 J{K*aux_j/2} F{vel_curvas}
			ENDIF
		ENDIF
	
		G01 X{cdb/2} F{vel_mec}
	
	ENDIF

ELSE
	//frente de cerradura con extremos a recto
	
	IF sentido
		'como el perimetro en el sentido horario
		// (1) Entra en el pto de entrada, interpolando y tambien, para no entrar tan recto
		G01 X{(cda-cdb/2)} Z{-cdl} F{vel_pen}
		ACTIVAR FRESA
		G01 Y{-K*aux_y} F{vel_mec}
		
		G01 X{cda} F{vel_mec}
		G01 Y{-K*(aux_y+aux_j)}
		G01 X{0}
		G01 Y{-K*(aux_y)}
		G01 X{cda}
			
	
		IF doblep_c
			//se agrego una doble pasada por si la madera se expande luego de la primera pasada
			'Segunda Pasada al frente de la cerradura
			G01 X{cda} F{vel_mec}
			G01 Y{-K*(aux_y+aux_j)}
			G01 X{0}
			G01 Y{-K*(aux_y)}
			G01 X{cda}
		ENDIF	
			
	ELSE
	
		'como el perimetro en el sentido antihorario {normal}
	
		G01 X{cdb/2} Z{-cdl} F{vel_pen}
		ACTIVAR FRESA
		G01 Y{-K*aux_y} F{vel_y}
		
		G01 X{0} F{vel_mec}
		G01 Y{-K*(aux_y+aux_j)}		
		G01 X{cda}
		G01 Y{-K*(aux_y)}		
		G01 X{0}
	
		IF doblep_c
			//se agrego una doble pasada por si la madera se expande luego de la primera pasada
			'Doble pasada al frente de la cerradura
			G01 X{0} F{vel_mec}
			G01 Y{-K*(aux_y+aux_j)}		
			G01 X{cda}
			G01 Y{-K*(aux_y)}		
			G01 X{0}
		ENDIF	
	
	ENDIF
	
ENDIF

PARAR FRESA
G01 Z2 F{vel_z}


' Cajon cerraduras
IF circular==1
	
	G52 X{pos_x0+pos_picaporte-diam/2} Y{pos_y0} Z{dpalpador_z}
	
ELSE	
	G52 X{pos_x0+(pos_picaporte+cdf-cda+cdh)} Y{pos_y0} Z{dpalpador_z}	
ENDIF
G01 Y{-K*(esp_pta/2+dcentro)} F{vel_y}


IF cdd-cdl >0
	//desfasaje por la viruta	
	desf_vir_x := desf_vir/INT((cdd-cdl)/cfeed + 0.95)
ELSE
	desf_vir_x := 0
ENDIF
IF zigzag
	desf_vir_x := 2*desf_vir_x
ENDIF

G01 X0 Z{-cdl} F{vel_mec}
ACTIVAR FRESA
prof_z:=cdl
aux_x := 0
WHILE prof_z<cdd
	prof_z:=prof_z+cfeed
	IF prof_z>cdd
		prof_z:=cdd
	ENDIF
	aux_x := aux_x + desf_vir_x
	
	IF circular==1
		G01 X{r_fresa_p} F{vel_xy}
		G01 Z{-prof_z} F{vel_pen}
		G03 X{r_fresa_p} Y{-K*(esp_pta/2+dcentro)} I{(diam/2-r_fresa_p)} J0 F{vel_curvas}
	ELSE
		IF cde<=2*r_fresa_p
			IF zigzag
				G01 X{cdc} Z{-prof_z} F{vel_cajeado}
			ELSE
				G01 Z{-prof_z} F{vel_pen}
				G01 X{cdc} F{vel_cajeado}
			ENDIF
			IF prof_z<cdd
				prof_z:=prof_z+cfeed
				IF prof_z>cdd
					prof_z:=cdd
				ENDIF
				IF zigzag
					G01 X{aux_x} Z{-prof_z} F{vel_cajeado}
				ELSE
					G01 Z{-prof_z} F{vel_pen}
					G01 X{aux_x} F{vel_cajeado}
				ENDIF
			ENDIF
		ELSE
			' hace rectangulos
			G01 Y{-K*((esp_pta-cde)/2+r_fresa_p+dcentro)} F{vel_y}
			IF zigzag
				G01 X{cdc} Z{-prof_z} F{vel_cajeado}
			ELSE
				G01 Z{-prof_z} F{vel_pen}
				G01 X{cdc} F{vel_cajeado}
			ENDIF
			G01 Y{-K*((esp_pta+cde)/2-r_fresa_p+dcentro)} F{vel_mec}
			IF zigzag
				prof_z:=prof_z+cfeed
				IF prof_z>cdd
					prof_z:=cdd
				ENDIF
				G01 X{aux_x} Z{-prof_z} F{vel_cajeado}

			ELSE
				G01 X{aux_x} F{vel_cajeado}
			ENDIF
		ENDIF
	ENDIF

END WHILE

IF zigzag==1 .AND. circular!=1 .AND. cde>16
	' completo el rectangulo
	G01 Y{-K*((esp_pta+cde)/2-r_fresa_p+dcentro)} F{vel_mec}
	G01 X{cdc} F{vel_cajeado}
	G01 Y{-K*((esp_pta-cde)/2+r_fresa_p-dcentro)} F{vel_mec}
	G01 X{desf_vir} F{vel_cajeado}
ENDIF

PARAR FRESA

//IF tipo_cerradura == 2
IF cdp > 0 .AND. cdn > 0 .AND. cdo > 0
	//Cerradura electronica
	//Hago el cajeado mas ancho que esta centrado en el alto del cajeado anterior
	G52 X{pos_x0+(pos_picaporte+cdf-(cda + cdn)/2)} Y{(esp_pta-cdo)/2 + r_fresa_p + dcentro} Z{dpalpador_z}
	
	G01 Z2 F{vel_z}
	
	//lado izquiero
	G01 X0 F{vel_mec}
	G01 Y0
	
	ACTIVAR FRESA
	
	prof_z:=0
	lado := 0
	WHILE prof_z < cdp
		prof_z:=prof_z+cfeed
		IF prof_z > cdp
			prof_z := cdp
		ENDIF
		
		G01 Z{-prof_z} F{vel_pen}
		
		IF lado == 0
			G01 X{cdn} F{vel_mec}
		ELSE
			G01 X0 F{vel_mec}
		ENDIF
		
		lado := 1 - lado
		
	END WHILE
	
	PARAR FRESA
	
	//Lado derecho	
	G01 Z2 F{vel_z}
	
	//lado izquiero
	G01 X0 F{vel_mec}
	G01 Y{cdo - 2*r_fresa_p}
	
	ACTIVAR FRESA
	
	prof_z:=0
	lado := 0
	WHILE prof_z < cdp
		prof_z:=prof_z+cfeed
		IF prof_z > cdp
			prof_z := cdp
		ENDIF
		
		G01 Z{-prof_z} F{vel_pen}
		
		IF lado == 0
			G01 X{cdn} F{vel_mec}
		ELSE
			G01 X0 F{vel_mec}
		ENDIF
		
		lado := 1 - lado
		
	END WHILE
	
	
	//lado izquierdo
ENDIF

'Fin de la cerradura {voy a referenciar}


PARAR FRESA
G01 Z{dsz+rebaje_canto} F{vel_z}
// AGREGO MARIANO: paro el motor
M5


IF retropalpador == 0 .AND. (pestillo .OR. (condena .AND. cdk!=0))
	//Solo la maquina nueva de la fv1 con palpador directo puede hacer agujeros para el pestillo y la codena
	//Pongo en Pausa para que puedan rotar la Puerta y poder hacer los agujeros
	M1
	PLANO XZ
	
	// Aca ya trabajo con otro conjunto
	G59P12
	G54
	G92.1
	
	G52 X{pos_x0+pos_picaporte} Y{cdi-cdj/2}
	
	//palpo el la posicion del Picaporte
	(Palpa)
	G01 X0 F{vel_xy}
	G01 Y0 F{vel_y}
        G52 Z0          
	G28.1 Z1
	G52 Z{dpalpador_z}
		
	G01 Z2 F{vel_z}
	//enciendo la fresa para esconder el palpador
	M3	
	
	IF pestillo
		' Ojo pestillo
	
		G01 X0 Y{r_fresa_p} F{vel_xy}
		
			
		ACTIVAR FRESA
		IF circular
			//si es  cerradura tubular
			'cerradura tubular
			prof_z:=0
			WHILE prof_z<esp_pta+2
				prof_z:=prof_z+feed
				IF prof_z>=esp_pta
					prof_z:=esp_pta+2
				ENDIF
				G01 Z{-prof_z}  F{vel_z}
				IF cdj>r_fresa_p*2
					G02 X0 Y{r_fresa_p} I0 J{(cdj-r_fresa_p*2)/2} F{vel_curvas}
				ENDIF
			END WHILE
		ELSE
			//si no es  cerradura tubular
	
			//come en cuatro pasadas, salteo el centro porque es hueco		
				aux_z:=0
				profundiza:=9
				WHILE -aux_z < esp_pta+2
					IF aux_z == -2 * profundiza
						aux_z:= -(esp_pta + 2 - 2*profundiza)
					ENDIF
					aux_z:= aux_z - profundiza				
					G01 Z{aux_z}  F{vel_pen}
						
					IF cdj>r_fresa_p*2
						G02 X0 Y{r_fresa_p} I0 J{(cdj-r_fresa_p*2)/2} F700
					ENDIF
					
				END WHILE
				
		ENDIF
	ENDIF
	
	PARAR FRESA
	
	G01 Z{dsz} F{vel_z}	
	
	// Agujero de la condena
	IF cdk!=0 .AND. condena==1
		'agujero de la condena
		
		//me aseguro de que cdk no sea menor que mdk3
		IF cdk3>cdk
			aux:=cdk3
			cdk3:=cdk
			cdk:=aux
		ENDIF
		
		G52 X{pos_x0+pos_picaporte+cdg-cda+cdf} Y{cdi-cdk/2}
		
		M03		
		
		G01 X0 F{vel_xy}
		G01 Y{r_fresa_p} F{vel_y}
		G01 Z2 F{vel_z}
	
		// el agujero se hace en 2 etapas
		// hasta prof1 hace un agujero grande (diam cdk), luego saltea la caja de la cerradura
		// y luego hace hasta la salida el agujero chico, diam cdk3
		ACTIVAR FRESA
		' agujero grande {DIAM cdk}	
		prof_z:=0
		prof1:=(esp_pta-cde)/2+2
		WHILE prof_z<prof1
			prof_z:=prof_z+feed
			IF prof_z>=prof1
				prof_z:=prof1
			ENDIF
			G01 Z{-prof_z}  F{vel_pen}
			IF cdk>2*r_fresa_p
				G02 X0 Y{r_fresa_p} I0 J{(cdk-2*r_fresa_p)/2} F{vel_curvas}
			ENDIF
			IF cdk2>r_fresa_p
				G01 Y{cdk/2} F{vel_mec}
				G01 X{-(cdk2-r_fresa_p)}
				G01 X0
				G01 Y{r_fresa_p}
			ENDIF
		END WHILE
	
		' agujero chico {DIAM cdk3}, para escudo de la llave
	
		IF cdk3>0
			
			G52 X{pos_x0+pos_picaporte+cdg-cda+cdf} Y{cdi-cdk3/2}
			
			G01 X0 Y{r_fresa_p} F{vel_xy}
		
			' salteo la prof1 mas el vaciado de la cerradura
			prof_z:=prof1+cde
			WHILE prof_z<esp_pta+2
				prof_z:=prof_z+feed
				IF prof_z>=esp_pta
					prof_z:=esp_pta+2
				ENDIF
				G01 Z{-prof_z}  F{vel_pen}
				IF cdk3>2*r_fresa_p
					G02 X0 Y{r_fresa_p} I0 J{(cdk3-2*r_fresa_p)/2} F{vel_curvas}
				ENDIF
				IF cdk2>r_fresa_p
					G01 Y{cdk/2} F{vel_mec}
					G01 X{-(cdk2-r_fresa_p)}
					G01 X0
					G01 Y{r_fresa_p}	
				ENDIF
			END WHILE
		ENDIF
		
		PARAR FRESA
		G01 Z{dsz} F{vel_z}
	ENDIF
	
	//IF tipo_cerradura == 2 .AND. diam > 0
	IF diam > 0 .AND. cdr != 0 .AND. cdm != 0
		//Cerradura electronica
		// lleva 2 agujeros mas aparte del picaporte y la condena
		
		IF diam > 2*r_fresa_p
			//el diametro es mayor al de la fresa
			//come haciendo circulos
			
			//primer agujero
			G52 X{pos_x0+pos_picaporte + cdm - cda + cdf} Y{cdi-diam/2}
							
			G01 X0 Y{r_fresa_p} F{vel_xy}
			
			ACTIVAR FRESA
						
			prof_z:=0
			WHILE prof_z<esp_pta+2
				prof_z:=prof_z+feed
				IF prof_z>=esp_pta
					prof_z:=esp_pta+2
				ENDIF
				G01 Z{-prof_z}  F{vel_pen}
				G02 X0 Y{r_fresa_p} I0 J{diam/2-r_fresa_p} F{vel_curvas}
							
			END WHILE
			PARAR FRESA
			G01 Z{dsz} F{vel_z}
			
			//Segundo agujero
			G52 X{pos_x0+pos_picaporte - cdr + cdf} Y{cdi-diam/2}
							
			G01 X0 Y{r_fresa_p} F{vel_xy}
			
			ACTIVAR FRESA
			
			prof_z:=0
			WHILE prof_z<esp_pta+2
				prof_z:=prof_z+feed
				IF prof_z>=esp_pta
					prof_z:=esp_pta+2
				ENDIF
				G01 Z{-prof_z}  F{vel_pen}
				G02 X0 Y{r_fresa_p} I0 J{diam/2-r_fresa_p} F{vel_curvas}
							
			END WHILE
			PARAR FRESA
			G01 Z{dsz} F{vel_z}
			
		ELSE
			//con solo penetrar alcanza para hacer ambos agujeros
			//primer agujero
			G52 X{pos_x0+pos_picaporte + cdm - cda + cdf} Y{cdi}
							
			G01 X0 Y{r_fresa_p} F{vel_xy}
			ACTIVAR FRESA			
			G01 Z{-(esp_pta+2)} F{vel_pen}			
			PARAR FRESA			
			G01 Z{dsz} F{vel_z}
			
			//Segundo agujero
			G52 X{pos_x0+pos_picaporte - cdr + cdf} Y{cdi}
							
			G01 X0 Y{r_fresa_p} F{vel_xy}
			ACTIVAR FRESA
			G01 Z{-(esp_pta+2)} F{vel_pen}
			PARAR FRESA
			
			G01 Z{dsz} F{vel_z}
		
		ENDIF
		
	ENDIF
	
	'finalizo
	PARAR FRESA
	
//fin de retropalpador == 0
ENDIF

	

IF retropalpador
	//Solo las maquinas con retropalpador pueden referenciar, Las que palpan directo solo pueden ser referenciadas
	//en la posicion Xreferenciado e Yreferenciado. En ningun otro lugar puede encontrar el 0 de la maquina
	G28.1 Z{dsz}
ENDIF
M05

'finalizo

G54
G92.1
IF retropalpador == 0
	G28.1 A0
ENDIF
G52 X0 Y0
G01 Y0 F{vel_y}
G01 X{-dist_ax} F{vel_xy}
IF retropalpador
	//retro palpador (modelo viejo)
	G28.1 X{-dist_ax} Y0 Z0
ELSE
	//palpador directo (modelo nuevo)
	G28.1 X{-dist_ax} Y0
	G28.1 Z{dsz+dpalpador_z}
ENDIF


IF cerradura_antes .AND. pernios
	//pongo en pausa para rotar la pieza
	M1
ENDIF


