// Pasadores para puertas dobles

// largo_sup,largo_inf, prof_pasador,ancho_pasador


IF modelo == 3
	//si es la maquina FV1 solo se puede trabajar en el PL XY
	PROGRAMA pasadorXY
	
ELSE	
	//si no es la FV1 se trabaja en canto sup
	PLANO XZ
	G49 G40 G80 G50 G61 G90
	G54
	G92.1
	G21 
	S1000
	
	
	aux_vel_curvas:=vel_curvas
	//si interpola cambio la velocidad del las curvas por la velocidad de interpolacion
	IF modelo==2
		vel_curvas:=vel_interpolacion
	ENDIF
	
	aux_maq_dz:=maq_dz
	maq_dz := maq_dz + lambor_dz
	
	rot_0:=3
	
	IF mano_pta==1
		K:=-1
	ELSE
		K:=1
	ENDIF
	
	G01 X0 Y0 F{vel_xy}
		
	//es el desfasaje de la posicion del tope en x que se va a usar
	dpos_x0:=0
	IF modelo==2 .AND. 0
		//si es mayor a 2040 se usan otros topes
		IF alto_pta>2040
			IF K==1
				dpos_x0:=-pta_tope1
			ELSE
				//dpos_x0:=alto_pta-pta_tope3
			ENDIF
		ELSE
			IF K!=1
				//dpos_x0:=alto_pta-pta_tope2
			ENDIF
		ENDIF
	ENDIF
		
	IF modelo==2
		G52 Z0
		G01 Z0 F{vel_z}	
		G01 A{-ang_lambor} F{vel_rot}
	ENDIF
	
	// le aplico el lambor a la prof. del pasador
	prof_pasador:=prof_pasador + dang
	
	//posicion desde la punta de la fresa hasta la sup de la madera
	pos_y0:=-dpalpador+l_fresa_s
	
	// no puedo hacer un solo pasador, o hace los 2 o ninguno
	IF largo_inf!=0 .AND. largo_sup==0
		largo_sup:=largo_inf
	ELSE
		IF largo_sup!=0 .AND. largo_inf==0
			largo_inf:=largo_sup
		ENDIF
	ENDIF
	
	// pasador de la izquierda
	IF mano_pta==1
		largo_pasador:=largo_inf
	ELSE
		largo_pasador:=largo_sup
	ENDIF
	
	G52 X{dpos_x0} Y0 Z{-maq_dz}
	
	IF largo_pasador
	
		// Pasador de la izquierda
		// Lo mando arriba para palpar
		G01 Y{ancho_pta+maq_dfresa+dsys} X{largo_pasador-r_fresa_s}
		G01 Z{esp_pta/2} F{vel_z}
		G28.1 Y{ancho_pta+maq_dfresa+dsys-1}
	
		G01 Y0 F{vel_xy}
	
		ACTIVAR FRESA
		M03	
		G01 Y{pos_y0} F{vel_mec}
			
		// activo el lambor
		LAMBOR = -ang_lambor
	
	
		IF ancho_pasador<=2*r_fresa_s
			pen_y:=0
			WHILE pen_y<prof_pasador
				pen_y:=pen_y+feed
				IF pen_y>prof_pasador
					pen_y:= prof_pasador
				ENDIF
				G01 Y{pos_y0-pen_y} F{vel_pen}
				G01 X0 F{vel_cajeado}
				
				IF pen_y<prof_pasador
					pen_y:=pen_y+feed
					IF pen_y>prof_pasador
						pen_y:= prof_pasador
					ENDIF
					G01 Y{pos_y0-pen_y} F{vel_pen}
					G01 X{largo_pasador-r_fresa_s} F{vel_cajeado}
				ENDIF
			END WHILE 
		ELSE
			pen_y:=0
			G01 X0 F{vel_cajeado}
			WHILE pen_y<prof_pasador
				pen_y:=pen_y+feed
				IF pen_y>prof_pasador
					pen_y:= prof_pasador
				ENDIF
				G01 Z{(esp_pta+ancho_pasador)/2 - r_fresa_s} F{vel_z}
				G01 Y{pos_y0-pen_y} F{vel_pen}
				G01 X{largo_pasador-r_fresa_s} F{vel_cajeado}
				G01 Z{(esp_pta-ancho_pasador)/2 + r_fresa_s} F{vel_z}
				G01 X0 F{vel_cajeado}
				
			END WHILE 
	
		ENDIF
	
		// pasador de la izquierda
		IF mano_pta==1
			largo_pasador:=largo_sup
		ELSE
			largo_pasador:=largo_inf
		ENDIF
		
		//finalizo el lambor
		SIN LAMBOR
	
		PARAR FRESA
		
		G01 Y{dsys} F{vel_xy}
		
		//apago para palpar
		M05	
		
		G01 X{alto_pta-largo_pasador+r_fresa_s}
		G28.1 Y5
		G01 Y0 
	
		ACTIVAR FRESA
		M03
		G01 Y{pos_y0} F{vel_mec}
		
		// activo el lambor
		LAMBOR = -pend_lambor
	
		IF ancho_pasador<=2*r_fresa_s
			pen_y:=0
			WHILE pen_y<prof_pasador
				pen_y:=pen_y+feed
				IF pen_y>prof_pasador
					pen_y:= prof_pasador
				ENDIF
				G01 Y{pos_y0-pen_y} F{vel_pen}
				G01 X{alto_pta} F{vel_cajeado}
			
				IF pen_y<prof_pasador
					pen_y:=pen_y+feed
					IF pen_y>prof_pasador
						pen_y:= prof_pasador
					ENDIF
					G01 Y{pos_y0-pen_y} F{vel_pen}
					G01 X{alto_pta-largo_pasador+r_fresa_s} F{vel_cajeado}
				ENDIF
			END WHILE 
	
		ELSE
			pen_y:=0
			G01 X{alto_pta} F{vel_cajeado}
			WHILE pen_y<prof_pasador
				pen_y:=pen_y+feed
				IF pen_y>prof_pasador
					pen_y:= prof_pasador
				ENDIF
				G01 Z{(esp_pta+ancho_pasador)/2 - r_fresa_s} F{vel_z}
				G01 Y{pos_y0-pen_y} F{vel_pen}
				G01 X{alto_pta-largo_pasador+r_fresa_s} F{vel_cajeado}
				G01 Z{(esp_pta-ancho_pasador)/2 + r_fresa_s}
				G01 X{alto_pta} F{vel_cajeado}
				
			END WHILE 
	
		ENDIF
		
		// finalizo el lambor
		SIN LAMBOR
	
		PARAR FRESA
		G01 Y{dsys} F{vel_xy}
		
		M05
		
	ENDIF
	
	
	// Chapita del marco para la puerta
	IF pos_picaporte
	
		// le aplico el lambor al espesor de la chapita
		aux_mdi := mdi
		mdi:=mdi+dang
	
		// la cerradura esta desplazada del centro = esp_pta/2, segun dcentro
		IF mano_pta==0
			G52 X{pos_picaporte+mdp+dpos_x0} Y0 Z{-maq_dz+dcentro}
		ELSE
			G52 X{alto_pta-pos_picaporte-mdp} Y0 Z{-maq_dz+dcentro}
		ENDIF
	
		IF largo_pasador==0
			// Lo mando arriba para palpar
			G01 Y{ancho_pta+maq_dfresa+dsys} 
			G01 X0
			G01 Z{esp_pta/2} F{vel_z}
			G28.1 Y{ancho_pta+maq_dfresa+dsys-1}
		ELSE
			// ya esta arriba, palpo sobre la posicion de la chapita
			G01 Y{dsys} F{vel_xy}
			G01 X0
			G01 Z{esp_pta/2} F{vel_z}
			G28.1 Y5
		ENDIF
	
		IF mdk!=0 .AND. mdc!=mda
			//chapita con cuello 
	
			G01 Y0 F{vel_xy}
			G01 X{K*(-mda/2+r_fresa_s)} 
			G01 Z{esp_pta} F{vel_z}
			ACTIVAR FRESA
			M03
			G01 Y{pos_y0+1} F{vel_xy}
			
			// activo el lambor
			LAMBOR = -pend_lambor
			
			G01 Y{pos_y0-mdi} F{vel_pen}
			G01 Z{esp_pta-(mdb+r_fresa_s)} F{vel_z}
			G01 Z{esp_pta} 
	
			IF mdk==0
				mdk:=mdc/2
			ELSE
				mdk:=mdk+mda/2
			ENDIF
	
			G18
			G01 X{K*(mda/2-r_fresa_s)} F{vel_mec}
			G01 Z{esp_pta-(2*r_fresa_s-1)} F{vel_z}
			G01 X{K*(-mda/2+r_fresa_s)} F{vel_mec}
			G01 Z{esp_pta-(mdb+r_fresa_s)} F{vel_z}
			G01 X{-K*(mdk-mdd/2)} F{vel_mec}
			IF K==1
				G02 X{-K*(mdk-mdd/2)} Z{esp_pta-(mdb+mdd-r_fresa_s)} I0 K{-(mdd-2*r_fresa_s)/2} F{vel_curvas}
			ELSE
				G03 X{-K*(mdk-mdd/2)} Z{esp_pta-(mdb+mdd-r_fresa_s)} I0 K{-(mdd-2*r_fresa_s)/2} F{vel_curvas}
			ENDIF
			G01 X{K*(mdc-mdk-mdd/2)} F{vel_mec}
			IF K==1
				G02 X{K*(mdc-mdk-mdd/2)} Z{esp_pta-(mdb+r_fresa_s)} I0 K{+(mdd-2*r_fresa_s)/2} F{vel_curvas}
			ELSE	
				G03 X{K*(mdc-mdk-mdd/2)} Z{esp_pta-(mdb+r_fresa_s)} I0 K{+(mdd-2*r_fresa_s)/2} F{vel_curvas}
			ENDIF
			G01 X0 F{vel_mec} 		
	
	
			// 1er vaciado 
			IF mde<=2*r_fresa_s
				// vaciado simple
				PARAR FRESA
				G01 X{-K*mdf/2} F{vel_xy}
				G01 Z{esp_pta-(mdb+mdd-mdg-r_fresa_s)} F{vel_z}
				ACTIVAR FRESA
				pen_y:=mdi
				WHILE pen_y<mdh
					pen_y:=pen_y+feed
					IF pen_y>mdh
						pen_y:= mdh
					ENDIF
	
					G01 Y{pos_y0-pen_y} F{vel_pen}
					G01 X{K*mdf/2} F{vel_mec/2}
	
					IF pen_y<mdh
	
						pen_y:=pen_y+feed
						IF pen_y>mdh
							pen_y:= mdh
						ENDIF
						G01 Y{pos_y0-pen_y} F{vel_pen}
						G01 X{-K*mdf/2} F{vel_mec/2}
					ENDIF
				END WHILE
	
			ELSE
				// Cajeado rectangular (maximo 28mm)
				PARAR FRESA
				G01 X{-K*mdf/2} F{vel_xy}
				G01 Z{esp_pta-(mdb+mdd-mdg-r_fresa_s)} F{vel_z}
				ACTIVAR FRESA
				pen_y:=mdi
				WHILE pen_y<mdh
					pen_y:=pen_y+feed
					IF pen_y>mdh
						pen_y:= mdh
					ENDIF
	
					G01 Y{pos_y0-pen_y} F{vel_pen}
					G01 X{K*mdf/2} F{vel_mec/2}
					G01 Z{esp_pta-(mdb+mdd-mdg-mde+r_fresa_s)} F{vel_z}
					G01 X{-K*mdf/2} F{vel_mec/2}
					G01 Z{esp_pta-(mdb+mdd-mdg-r_fresa_s)} F{vel_z}
	
				END WHILE
	
			ENDIF
	
			PARAR FRESA
			G01 Y5 F{vel_xy}
	
			IF mdj!=0
				// Segundo vaciado
				IF mde2<=2*r_fresa_s
					// vaciado simple
					G01 X{K*(mdf/2+mdj+r_fresa_s)} F{vel_xy}
					G01 Z{esp_pta-(mdb+mdd-mdg2-r_fresa_s)} F{vel_z}
					ACTIVAR FRESA
					pen_y:=mdi
					WHILE pen_y<mdh2
						pen_y:=pen_y+feed
						IF pen_y>mdh2
							pen_y:= mdh2
						ENDIF
	
						G01 Y{pos_y0-pen_y} F{vel_pen}
						G01 X{K*(mdf/2+mdj+mdf2-r_fresa_s)} F{vel_mec/2}
	
						IF pen_y<mdh2
	
							pen_y:=pen_y+feed
							IF pen_y>mdh2
								pen_y:= mdh2
							ENDIF
							G01 Y{pos_y0-pen_y} F{vel_pen}
							G01 X{K*(mdf/2+mdj+r_fresa_s)} F{vel_mec/2}
						ENDIF
					END WHILE
	
				ELSE
					// Cajeado rectangular (maximo 28mm)
					G01 X{K*(mdf/2+mdj+r_fresa_s)} F{vel_xy}
					G01 Z{esp_pta-(mdb+mdd-mdg2-r_fresa_s)} F{vel_z}
					ACTIVAR FRESA
					pen_y:=mdi
					WHILE pen_y<mdh2
						pen_y:=pen_y+feed
						IF pen_y>mdh2
							pen_y:= mdh2
						ENDIF
	
						G01 Y{pos_y0-pen_y} F{vel_pen}
						G01 X{K*(mdf/2+mdj+mdf2-r_fresa_s)} F{vel_mec/2}
						G01 Z{esp_pta-(mdb+mdd-mdg2-mde2+r_fresa_s)} F{vel_z}
						G01 X{K*(mdf/2+mdj+r_fresa_s)} F{vel_mec/2}
						G01 Z{esp_pta-(mdb+mdd-mdg2-r_fresa_s)} F{vel_z}
	
					END WHILE
	
				ENDIF
				
			ENDIF
			PARAR FRESA
		ELSE
		//CHAPITA SIN CUELLO
		//chapita sin cuello
		//Cuando la chapita va sin cuello el ancho total de la chapita en vez de ser mdc es mda
		//y el mdc queda inutil
		mdb:=0
	
		//el mrc lo toma desde la interface
		//mrc:=r_fresa_s
			IF mrc<r_fresa_s		
				mrc:=r_fresa_s
			ENDIF
	
			G01 Y0 F{vel_xy}
			G01 X{K*(-mda/2+r_fresa_s)} 
			G01 Z{esp_pta} F{vel_z}
			
			G18
			
			ACTIVAR FRESA
			M03
			G01 Y{pos_y0+1} F{vel_xy}
			
			// activo el lambor
			LAMBOR = -pend_lambor	
			
			G01 Y{pos_y0-mdi} F{vel_pen}
			
			mdl:=esp_pta-mdd
			
			
			//Empiezo a comer el contorno de la chapita
			//NOTA:las X estan al eje o sea se come la mitad del da para la izquierda negativa y la otra mitad para la derecha positiva
			G01 X{-K*(mda/2-r_fresa_s)} F{vel_mec}
			//estoy abajo me muevo hacia arriba 
			G01 Z{(mdl+mrc)} F{vel_Z}
			
			IF mrc<=r_fresa_s
				//si la curvatura es menor al radio de la fresa hago un G01 en vez de un G02/03
				G01 X{K*(mda/2-mrc)} F{vel_mec}
			ELSE
				IF K==1
					G02 X{-(mda/2-mrc)} Z{(mdl+r_fresa_s)} I{mrc-r_fresa_s} K0	F{vel_curvas}
					//voy hacia la derecha
					G01 X{K*(mda/2-mrc)} F{vel_mec}
					G02 X{mda/2-r_fresa_s} Z{(mdl+mrc)} I0 K{-(-mrc+r_fresa_s)} F{vel_curvas}
				ELSE
					G03 X{(mda/2-mrc)} Z{(mdl+r_fresa_s)} I{-mrc+r_fresa_s} K0	F{vel_curvas}
					//voy hacia la derecha
					G01 X{K*(mda/2-mrc)} F{vel_mec}
					G03 X{-(mda/2-r_fresa_s)} Z{(mdl+mrc)} I0 K{-(-mrc+r_fresa_s)} F{vel_curvas}
				ENDIF			
			ENDIF
			
			//Bajo y como la parte inferior
			G01 Z{esp_pta} F{vel_Z}		
			G01 X{-K*(mda/2-r_fresa_s)} F{vel_mec}
			
			//como el interior de la chapita
			//si es mas grande de 21
			IF mdd>3*r_fresa_s-1		  
				G01 Z{(mdl+r_fresa_s*3-1)} F{vel_Z}				
				G01 X{K*(mda/2-r_fresa_s*2+1)} F{vel_mec}		
			ENDIF
			//si es mas grande de 35
			IF mdd>5*r_fresa_s-2			
				G01 Z{(mdl+r_fresa_s*5-2)} F{vel_Z}	
				G01 X{-K*(mda/2-r_fresa_s*2+1)}	F{vel_mec}	
			ENDIF
			//si es mas grande de 49
			IF mdd>7*r_fresa_s-3			
				G01 Z{(mdl+r_fresa_s*7-3)} F{vel_Z}				
				G01 X{K*(mda/2-r_fresa_s*2+1)} F{vel_mec}	
			ENDIF
			
			// 1er vaciado 
			IF mde<=2*r_fresa_s
				// vaciado simple
				PARAR FRESA
				G01 X{-K*mdf/2} F{vel_xy}
				G01 Z{esp_pta-(mdb+mdd-mdg-r_fresa_s)} F{vel_z}
				ACTIVAR FRESA
				pen_y:=mdi
				WHILE pen_y<mdh
					pen_y:=pen_y+feed
					IF pen_y>mdh
						pen_y:= mdh
					ENDIF
	
					G01 Y{pos_y0-pen_y} F{vel_pen}
					G01 X{K*mdf/2} F{vel_mec/2}
	
					IF pen_y<mdh
	
						pen_y:=pen_y+feed
						IF pen_y>mdh
							pen_y:= mdh
						ENDIF
						G01 Y{pos_y0-pen_y} F{vel_pen}
						G01 X{-K*mdf/2} F{vel_mec/2}
					ENDIF
				END WHILE
	
			ELSE
				// Cajeado rectangular (maximo 28mm)
				PARAR FRESA
				G01 X{-K*mdf/2} F{vel_xy}
				G01 Z{esp_pta-(mdb+mdd-mdg-r_fresa_s)} F{vel_z}
				ACTIVAR FRESA
				pen_y:=mdi
				WHILE pen_y<mdh
					pen_y:=pen_y+feed
					IF pen_y>mdh
						pen_y:= mdh
					ENDIF
	
					G01 Y{pos_y0-pen_y} F{vel_pen}
					G01 X{K*mdf/2} F{vel_mec/2}
					G01 Z{esp_pta-(mdb+mdd-mdg-mde+r_fresa_s)} F{vel_z}
					G01 X{-K*mdf/2} F{vel_mec/2}
					G01 Z{esp_pta-(mdb+mdd-mdg-r_fresa_s)} F{vel_z}
	
				END WHILE
	
			ENDIF		
			
			PARAR FRESA		
		ENDIF	
		
		mdi := aux_mdi
	ENDIF
	
	PARAR FRESA
	G01 Y{dsys} F{vel_xy}
	M05
	
	
	// finalizo el lambor
	SIN LAMBOR
	
	//restauro el valor de vel_curvas por si fue cambiado por el de la interpolacion
	vel_curvas:=aux_vel_curvas
	
	maq_dz:=aux_maq_dz
	
	G54
	G92.1
	G52 Z0
	G01 Z0 F{vel_z}
	IF modelo==2	
		G01 A{rot_0} F{vel_rot}
	ENDIF
	G52 X0 Y0
	IF moldura==1
		// moldura
		G01 X{-dist_ax}  F{vel_xy}
		G01 Y{-ancho_pta-maq_dfresa} 
	ELSE
		G01 X{-dist_ax} Y{-ancho_pta-maq_dfresa} F{vel_xy}
	ENDIF
	G28.1 Y{-ancho_pta-maq_dfresa}
	G28.1 X{-dist_ax} Y0 Z0
	
	//fin modelo == 3
ENDIF		
	
  