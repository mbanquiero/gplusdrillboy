PARAMETROS pp_x1
//ojo la fresa tiene que estar apagada antes de llamar a esto

'Palpa

IF modelo == 3
	G01 Y-15 F{vel_y}
	G01 X{pp_x1+60} F{vel_xy}
	G01 Z1 F{vel_z}
	G28.1 Z1
ELSE
	G01 Y-15 F{vel_xy}
	G01 X{pp_x1}
	G01 Z1 F{vel_z}
	G28.1 Z1
ENDIF
G52 Z{dpalpador_z}
G01 Z2 F{vel_z}

// prende y apaga la fresa para poder palpar y que se oculte el palpador
// la prendo aca para que se oculte el palpador antes de empezar a hacer el dibujo
M03


