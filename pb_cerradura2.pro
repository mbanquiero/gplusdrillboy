(CERRADURA)
PLANO XY
G18

(DESF_Y={dfresa_cajeado})

pos_ini_cajas := 0

G52 X{-pos_pinza+desf_x0} Y0 Z{-maq_abajo_dz}
(dsz = {dsz} , pos_pren_z = {pos_pren_z} , p_offset_z = {p_offset_z})
(*****************************************************************************)

IF ranura

	// empieza unos 10 mm afuera del borde 
	pos_x0:=alto_pta + 10
	
	WHILE pos_x0 - dist_pinza_prensor > 100

		PROGRAMA moverAZSegura
		// Ahora bajo hasta la posicion y segura para penetrar
		pos_y_segura := mar_ys_rot
		G52 Y0
		G01 Y{pos_y_segura} F{vel_xy}

		// voy a buscar la posicion X (estoy en el canto inferior)
		// la posicion de la pinza conviene que este a la derecha del cajeado, de tal forma que deje unos 261 aprox
		PROGRAMA moverPinzaPos CON pos_x0 - dist_pinza_prensor - ancho_pinza +2
		(POS_PINZA = {pos_pinza})
		
		
		//corrijo el offset Z
		G52 Z{-maq_abajo_dz+pieza_dz}

		// me posiciono en x0
		G01 X{pos_x0} F{vel_xy}
		(Bajo a Y para entrar)
		G52 Y{-dfresa_cajeado}
		G01 Y-3 F{vel_xy}

		pen_y:=0
		M03

		WHILE pen_y < pb_pr
			pen_y:=pen_y+feed
			IF pen_y>pb_pr
				pen_y:= pb_pr
			ENDIF
			G01 X{pos_x0} F{vel_cajeado}
			G01 Z{-((esp_pta-pb_ar)/2 + r_fresa_i )} F{vel_z}
			ACTIVAR FRESA
			G01 Y{pen_y} F{vel_pen}
			G01 X{pos_x0-desf_x1+5} F{vel_cajeado}
			G01 Z{-((esp_pta+pb_ar)/2 - r_fresa_i )} F{vel_z}
			G01 X{pos_x0} F{vel_cajeado}
			
		END WHILE
		PARAR FRESA
		// bajo Y 
		G01 Y-3 F{vel_xy}
		// retiro Z
		PROGRAMA moverAZSegura
		// apago la fresa
		M05
		// marco hasta donde llegue
		ult_pos_x0:=pos_x0-desf_x1+5

		// paso al siguiente ciclo
		pos_x0:=pos_x0 - desf_x1+10
	END WHILE

	// ahora falta la primer parte del ranurado, desde -10 hasta ult_pos_x0

	pos_x0 := -10
	WHILE pos_x0 < ult_pos_x0+10

		// cajeo desde pos_x0 hasta pos_x1
		pos_x1 := pos_x0+desf_x1-5
		IF pos_x1> ult_pos_x0+10
			pos_x1 := ult_pos_x0+10	
		ENDIF
		
		PROGRAMA moverAZSegura
		G52 Y0
		G01 Y{pos_y_segura} F{vel_xy}
		PROGRAMA moverPinzaPos CON pos_x0 + dist_pinza_prensor
		G52 Z{-maq_abajo_dz+pieza_dz}
		// me posiciono en x0
		G01 X{pos_x0} F{vel_xy}
		// Bajo a Y para entrar
		G52 Y{-dfresa_cajeado}
		G01 Y0 F{vel_xy}
		pen_y:=0
		M03
		WHILE pen_y < pb_pr
			pen_y:=pen_y+feed
			IF pen_y>pb_pr
				pen_y:= pb_pr
			ENDIF
			G01 X{pos_x0} F{vel_cajeado}
			G01 Z{-((esp_pta-pb_ar)/2 + r_fresa_i )} F{vel_z}
			ACTIVAR FRESA
			G01 Y{pen_y} F{vel_pen}
			G01 X{pos_x1} F{vel_cajeado}
			G01 Z{-((esp_pta+pb_ar)/2 - r_fresa_i )} F{vel_z}
			G01 X{pos_x0} F{vel_cajeado}
				
		END WHILE
		PARAR FRESA
		// bajo Y 
		G01 Y-3 F{vel_xy}
		// retiro Z
		PROGRAMA moverAZSegura
		// apago la fresa
		M05

		// paso al siguiente ciclo
		pos_x0:=pos_x0 + desf_x1-10
	END WHILE
ELSE
	// Hago las cajas directamente
	// me retiro en Z
	PROGRAMA moverAZSegura
	// Ahora bajo hasta la posicion y segura para poder mover la pieza hasta el otro lado
	pos_y_segura := mar_ys_rot
	G52 Y0
	G01 Y{pos_y_segura} F{vel_xy}
	// el cabezal queda retirado de Z y en Y aprox. 100 como para empezar a mover la pieza hasta
	// la posicion de la primer caja
	
ENDIF

// Hay 3 vaciados o 5, el del medio es grande y el resto pequeños
t:=0
cg := if(cant_cajeados==3,1,2)
WHILE t < cant_cajeados
	pb_d:= if(t==cg,pb_d2,pb_d1)
	pb_prof_caja:= if(t==cg,pb_prof_caja2,pb_prof_caja1)
	'cajas
	//le sumo la posicion izquierda de la ranura (pb_ri) porque la cajas estan unidas al frente de la ranura
	pos_x0 := pos_ini_cajas+pb_pos_c[t]
	IF pos_x0 + dist_pinza_prensor < pieza_dx - ancho_pinza
		// pongo la pinza a la derecha de la caja
		PROGRAMA moverPinzaPos CON pos_x0 + dist_pinza_prensor	
	ELSE
		// tengo que poner la pinza la izquierda del cajon
		PROGRAMA moverPinzaPos CON pos_x0 + pb_d - dist_pinza_prensor - ancho_pinza
	ENDIF
		
	PROGRAMA cajeado CON pos_x0,pb_d,pb_esp_caja,pb_prof_caja
	IF t==cg && pb_prof_caja3>0
		//segundo cajeado para la caja central
		diffx:= (pb_d - pb_d3)/2
		pos_x0 := pos_ini_cajas+pb_pos_c[t]+diffx
		PROGRAMA cajeado CON pos_x0,pb_d3,pb_esp_caja,pb_prof_caja3 + pb_prof_caja2
	ENDIF
	
	// retiro Z
	PROGRAMA moverAZSegura
	
	t := t + 1
END WHILE
'fin de los cajeados
M5


// Aca ya trabajo con otro conjunto, y en otro plano
G17

(DESF_Y={-fn_dfy})
(DESF_Z={-fn_dfz})

(PESTILLO pos = {pb_pos_lx})	
IF pestillo

	PROGRAMA moverPinzaPos CON pb_pos_lx + fn_dfx + 200	
	
	G52 X{pb_pos_lx -pos_pinza + desf_x0+ fn_dfx} Y{fn_dfy} Z{-maq_abajo_dz+esp_pta + fn_dfz+ l_fresa_p}

	'pestillo agujero grande
	G01 X{-pb_lr+r_fresa_p} Y{pb_pos_ly} F{vel_xy}
	// prendo el motor de alta frecuencia
	M502
	G04 p1
			
	pen_z:=0
	prof1:=14
	WHILE pen_z<prof1
		pen_z:=pen_z+feed
		IF pen_z>prof1
			pen_z:= prof1
		ENDIF
		ACTIVAR FRESA
		G01 Z{-pen_z} F{vel_pen}
		IF pb_lr>7
			G02 X{-pb_lr+r_fresa_p} Y{pb_pos_ly} I{pb_lr-r_fresa_p} J0 F{vel_curvas}
		ENDIF
	END WHILE
	
	' Agujero chico
	pen_z:=prof1
	G01 X{-pb_lr2+r_fresa_p} Y{pb_pos_ly} F{vel_xy}
	WHILE pen_z<esp_pta+2
		pen_z:=pen_z+feed
		IF pen_z>esp_pta+2
			pen_z:= esp_pta+2
		ENDIF
		G01 Z{-pen_z} F{vel_pen}
		IF pb_lr2>r_fresa_p
			G02 X{-pb_lr2+r_fresa_p} Y{pb_pos_ly} I{pb_lr2-r_fresa_p} J0 F{vel_curvas}
		ENDIF
	END WHILE
	PARAR FRESA
	
	// retiro Z
	PROGRAMA moverAZSegura
	M503
ENDIF


// Agujero de la mirilla
IF pb_radio_mirilla!=0
	'Mirilla
	PROGRAMA moverPinzaPos CON alto_pta-pb_alto_mirilla + fn_dfx + pb_radio_mirilla + 50	
	G52 X{-pos_pinza+desf_x0+ fn_dfx} Y{fn_dfy} Z{-maq_abajo_dz+esp_pta + fn_dfz+ l_fresa_p}
	
	IF pb_radio_mirilla>r_fresa_p
		'penetra en circulos
		G01 X{alto_pta-pb_alto_mirilla} Y{ancho_pta/2+pb_radio_mirilla-r_fresa_p} F{vel_xy}
		M502
		G04 p1
		ACTIVAR FRESA
		pen_z:=0
		WHILE pen_z<esp_pta+2
			pen_z:=pen_z+feed
			IF pen_z>esp_pta+2
				pen_z:= esp_pta+2
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
			G02 X{alto_pta-pb_alto_mirilla} Y{ancho_pta/2+pb_radio_mirilla-r_fresa_p} I0 J{r_fresa_p-pb_radio_mirilla} F{vel_curvas}
		END WHILE
	ELSE
		' solo perfora
		G01 X{alto_pta-pb_alto_mirilla} Y{ancho_pta/2+pb_radio_mirilla-r_fresa_p} F{vel_xy}
		M502
		G04 p1
		G01 X{alto_pta-pb_alto_mirilla} Y{ancho_pta/2} F{vel_xy}
		ACTIVAR FRESA
		
		pen_z:=0
		WHILE pen_z<esp_pta+2
			pen_z:=pen_z+feed
			IF pen_z>esp_pta+2
				pen_z:= esp_pta+2
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
		END WHILE
	ENDIF
	PARAR FRESA
	
	// retiro Z
	PROGRAMA moverAZSegura
	M503
ENDIF

// Agujero del pomo
IF pb_radio_pomo!=0

	PROGRAMA moverPinzaPos CON alto_pta-pb_alto_pomo + fn_dfx + pb_radio_pomo + 50	
	G52 X{-pos_pinza + desf_x0+ fn_dfx} Y{fn_dfy} Z{-maq_abajo_dz+esp_pta + fn_dfz+ l_fresa_p}

	'Agujero del pomo
	IF pb_radio_pomo>r_fresa_p
		'penetra en circulos
		G01 X{alto_pta-pb_alto_pomo} Y{ancho_pta/2+pb_radio_pomo-r_fresa_p} F{vel_xy}
		M502
		G04 p1
		ACTIVAR FRESA
		pen_z:=-feed
		WHILE pen_z<esp_pta+2
			pen_z:=pen_z+feed
			IF pen_z>esp_pta+2
				pen_z:= esp_pta+2
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
			G02 X{alto_pta-pb_alto_pomo} Y{ancho_pta/2+pb_radio_pomo-r_fresa_p} I0 J{r_fresa_p-pb_radio_pomo} F{vel_curvas}
		END WHILE
	ELSE
		' solo perfora
		G01 X{alto_pta-pb_alto_pomo} Y{ancho_pta/2} F{vel_xy}
		M502
		G04 p1
		ACTIVAR FRESA
		pen_z:=0
		WHILE pen_z<esp_pta+2
			pen_z:=pen_z+feed
			IF pen_z>esp_pta+2
				pen_z:= esp_pta+2
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
		END WHILE
	ENDIF
	PARAR FRESA
	// retiro Z
	PROGRAMA moverAZSegura
	M503
ENDIF

G54
G52 Z0
G01 Z0 F{vel_z}
