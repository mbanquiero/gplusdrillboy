#var op mano_pta := 0	{Derecha,Izquierda}		// Mano de la Puerta
#var mm ancho_pta:= 825			// Ancho Puerta :
#var mm alto_pta:= 2020			// Alto Puerta :
#var mm esp_pta:= 35		// Esp de la puerta:

#var mm pb_pos_x1:= 120			// Pos 1era Bisagra :
#var mm pb_pos_x2:= 955			// Pos 2era Bisagra :
#var mm pb_pos_x3:= 1795			// Pos 3era Bisagra :
#var mm pb_pos_x4:= 0				// Pos 4era Bisagra :

#var mm pos_picaporte:= 1015		// Pos del Picaporte:
#var sn condena:= 1					// Lleva Condena

#var mm pb_da:= 150				// Largo total:
#var mm pb_db:= 32				// Ancho :
#var mm pb_prof:= 3				// Prof:
#var mm pb_rc:= 15				// Radio:
#var mm pb_ax:= 115				// Pos. Agujero X:
#var mm pb_az:= 16				// Pos. Agujero Z:
#var mm pb_ra:= 11				// Radio Agujero:

#var mm cda:= 165.5			// Largo del Frente:
#var mm cdb:= 21.5			// Ancho del Frente:
#var mm cdc:= 98			// Largo de la Caja:
#var mm cdd:= 79.2			// Profundidad de la Caja:
#var mm cdf:= 14			// Dist. borde al centro del picaporte
#var mm cdg:= 70			// Dist. entre Centro condena:
#var mm cdh:= 35			// Dist. del borde a la Caja:
#var mm cdi:= 51			// Altura del Picaporte:
#var mm cdj:= 22			// Diametro del Picaporte:
#var mm cdk:= 18			// Diametro de la condena:
#var mm cdl:= 3.7			// Espesor de la Cerradura:
#var sn circular:= 1		// Cajeado Circular
#var mm cdk2:= 18			// Diametro de la condena 2:
#var mm diam:= 25		// Diametro del Cajeado

#var mm mda:= 45		// DA = (Cuello ):
#var mm mdb:= 8		// DB = (Salida):
#var mm mdc:= 72		// DC = (Largo Total):
#var mm mdd:= 20		// DD = (Ancho):
#var mm mde:= 16		// DE = Espesor
#var mm mdf:= 30		// DF = (Base rectangulo):
#var mm mdg:= 3.5		// DG = (Sep. Rectangulo):
#var mm mdh:=10 		// DG = (Sep. Rectangulo):
#var mm mdi:=1.5 	// Espesor de la chapa
#var mm mdm:=20 		// DM= (Compensacion marco):
#var mm mdp:= 47				// Dist desde el ojo picaporte al pestillo:
#var mm m_prof_pestillo:=10		// Profunidad del pestillo
#var mm m_prof_bulon:=10		// Profunidad del bulon

// variables de ambito
#var sn pernio1:= 1		// Mecanizar pernio 1
#var sn pernio2:= 1		// Mecanizar pernio 1
#var sn pernio3:= 1		// Mecanizar pernio 1
#var sn pernio4:= 1		// Mecanizar pernio 1
#var sn pernio5:= 1		// Mecanizar pernio 1
#var sn cerradura:= 1		// Mecanizar Cerradura
#var sn doblep:= 0		// Doble pasada para pernios


// ambito para productos genericos
//G_TODO				0
//G_PUERTA				0
//G_CANTO_SUP			1
//G_CANTO_INF			2
//G_MARCO				3

IF mano_pta==1
	K:=-1
ELSE
	K:=1
ENDIF


IF ambito==0
	// PUERTA
	PROGRAMA pm_canto_sup
	PROGRAMA pm_cerradura
ENDIF

IF ambito==1
	// Solo el canto sup
	cerradura := 0
	PROGRAMA pm_canto_sup
ENDIF

IF ambito==2
	// Solo el canto inferior
	G49 G40 G80 G50 G61 G90
	G54
	G92.1
	G21 
	S1000
	G01 X{K*(pos_picaporte-cdf-cdh)} Y0 F24000
	G28.1 Y0
	PROGRAMA pm_cerradura
ENDIF

IF ambito==3
	// Marco
	PROGRAMA pm_marco
ENDIF
