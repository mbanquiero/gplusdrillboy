PLANO XZ

// Puerta Blindada
G49 G40 G80 G50 G61 G90
G54
G92.1
G21 
S1000

IF mano_pta==1
	G59P15
	K:=-1
ELSE
	K:=1
ENDIF
// -------------------------------------
// Le tengo que decir si es la primer bisagra, debido a que el ref. en y lo hace solo la primera ve
IF pb_pos_x1!=0 .AND. pernio1!=0
	IF doblep
		PROGRAMA pb_bisagra CON pb_pos_x1,K,1,pb_prof/2
	ENDIF
	PROGRAMA pb_bisagra CON pb_pos_x1,K,1,pb_prof
ENDIF

IF pb_pos_x2!=0 .AND. pernio2!=0
	IF doblep
		PROGRAMA pb_bisagra CON pb_pos_x2,K,0,pb_prof/2
	ENDIF
	PROGRAMA pb_bisagra CON pb_pos_x2,K,0,pb_prof
ENDIF

IF pb_pos_x3!=0 .AND. pernio3!=0
	IF doblep
		PROGRAMA pb_bisagra CON pb_pos_x3,K,0,pb_prof/2
	ENDIF
	PROGRAMA pb_bisagra CON pb_pos_x3,K,0,pb_prof
ENDIF

IF pb_pos_x4!=0 .AND. pernio4!=0
	IF doblep
		PROGRAMA pb_bisagra CON pb_pos_x4,K,0,pb_prof/2
	ENDIF
	PROGRAMA pb_bisagra CON pb_pos_x4,K,0,pb_prof
ENDIF


// cuando termina la ultima bisagra me retiro de la mesa en el Z
G54 
G52 X0 Z0
G01 Z0 F1500

IF palpador==1
	
	IF K==1
		// en la puerta derecha, la ultima bisagra queda sobre la dercha
		G01 X{alto_pta+10} Y{-ancho_pta-maq_dfresa} F24000
	ELSE
		// en la puerta derecha, la ultima bisagra queda sobre la izquierda
		G01 X0 Y{-ancho_pta-maq_dfresa} F24000
	ENDIF
	G28.1 Y{-ancho_pta-maq_dfresa}
ENDIF



IF cerradura==0
	// Si no hace la cerradura, tengo que terminar el programa
	G54
	G52 Z0
	G01 Z0 F2000
	G52 X0 Y0
	G0 X0 Y0  F20000
	G28.1 X0 Y0 Z0

ENDIF


  