#var op mano_pta := 0	{Derecha,Izquierda}		// Mano de la Puerta

#var mm pb_pos_x1:= 120				// Pos 1era Bisagra :
#var mm pb_pos_x2:= 955				// Pos 2era Bisagra :
#var mm pb_pos_x3:= 1795			// Pos 3era Bisagra :
#var mm pb_pos_x4:= 0				// Pos 4era Bisagra :

#var mm pb_da:= 150				// Largo total:
#var mm pb_db:= 32				// Ancho :
#var mm pb_prof:= 3				// Prof:
#var mm pb_rc:= 15				// Radio:
#var mm pb_ax:= 115				// Pos. Agujero X:
#var mm pb_az:= 16				// Pos. Agujero Z:
#var mm pb_ra:= 11				// Radio Agujero:

#var mm ancho_pta:= 825			// Ancho Puerta :

#var mm maq_dz:= 122			// prof maquina:
#var mm maq_dfresa:= 325		// Dist. Fresa:

rot_0:=3
aux_maq_dz:=maq_dz
maq_dz := maq_dz - lambor_dz

rebaje_canto := pb_rebaje_canto
dpalpador_z := dpalpador_z 


PLANO XY

// Puerta Blindada
G49 G40 G80 G50 G61 G90
G54
G92.1
G21 
S1000

IF mano_pta==1
	K:=-1
ELSE
	K:=1
ENDIF

IF !retropalpador
	//retira la punta del palpador que est� tocando el topecito met�lico, y se puede mover sin enganchar nada.
	G01 Z2 F{vel_z}
ENDIF

G01 X0 F{vel_xy}
G01 Y0 F{vel_y}

// maq_dpx<n> = idem para el pernio especifico <n> 

// al ancho del pernio (pb_db) le resto la salida del pernio
pb_db:=pb_db-pb_salida


//es el desfasaje de la posicion del tope en x que se va a usar
dpos_x0:=0


// -------------------------------------
// Le tengo que decir si es la primer bisagra, debido a que el ref. en y lo hace solo la primera ve
// a la prof del pernio le sumo dang, x el angulo del canto de la puerta
// Desfasajes: el desf. Z se lo aplico al cuello del pernio pb_db, y el x la posicion x1
aux_db := pb_db
aux_da := pb_da
aux_prof := pb_prof

IF pernios
	t:=0			
	WHILE t < cant_pernios
		//invierto el orden de los pernios para que comience por el mas cercano
		desp:= cant_pernios-t-1
		xp:=pb_pos_x[desp]+maq_dx+pb_da
		xp := alto_pta-xp
		x  := xp
		
		IF ancho_pta<=600
			pb_db:=aux_db+desf2_sup_z(x)
			pb_da:=aux_da+desf2_sup_l(x)
			aux_x:=xp+desf2_sup_x(x)
		ELSE
			pb_db:=aux_db+desf_sup_z(x)
			pb_da:=aux_da+desf_sup_l(x)
			aux_x:=xp+desf_sup_x(x)
		ENDIF		
		
		
		pb_prof:= aux_prof + pb_dang
		
		//modifico las variables del marco para que se adapten a la puerta
		pb_calle:=esp_pta - pb_db
		pb_mds:=0
		
		PROGRAMA pb_bisagraPXY CON aux_x+dpos_x0,K,t
		t := t + 1
	END WHILE
ENDIF

pb_da:=aux_da
pb_db:=aux_db	
pb_prof:=aux_prof

'finalizo

IF !cerradura
	//si no hace la cerradura finalizo
	G01 Z{dsz+pb_rebaje_canto} F{vel_z}
	G54
	M05
	G52 X0 Y0
	IF retropalpador
		//version con retropalpador
		G28.1 Z{dsz}
	ENDIF
	G92.1
	IF retropalpador == 0
		G28.1 A0
	ENDIF
	G01 Y0 F{vel_y}
	G01 X{-dist_ax} F{vel_xy}
	G28.1 X{-dist_ax} Y0
	IF retropalpador == 0
		//version con palpador directo
		G28.1 Z{dsz+dpalpador_z}
	ENDIF
ENDIF

IF cerradura .AND. pernios
	//pongo en pausa para rotar la pieza
	//elimino el offset del palpado
	
	G01 Z{dsz+pb_rebaje_canto} F{vel_z}
	G54
	IF retropalpador
		//version anterior con retropalpador, La version con palpador directo no puede referenciar aca
		//asique mantiene el offset hasta volver a palpar en la cerradura
		G28.1 Z{dsz}
	ENDIF
	G92.1
	M05
	
	//Pongo en pausa
	M1
ENDIF


  