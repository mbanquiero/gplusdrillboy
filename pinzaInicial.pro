//FUNCION pinzaInicial

	//	pos_x := - l_fresa - drot - dx_canto_i - pos_pinza + desf_x0 - dsx
	
	// Falta que se pueda configurar esto: 
	//dfx:=fresador[0].dfx
	//dx_canto_i:=fresador[0].dx_canto_d
	//l_fresa := que_largo(0)

	dfx:=f0_dfx
	dx_canto_i:=f0_dx_canto_d
	l_fresa := f0_largo
	
	// Cargar pieza
	// Cerrar pinza
	M20
	G4 P{pausa}
	// desactivarTope
	G01 Z{pos_desac_tope-p_offset_z} F{vel_Z}

	//dejo el offset en la posicion de la pinza
	G52 X{-pos_pinza+desf_x0} Y{suple_dy} Z{pos_z0}

