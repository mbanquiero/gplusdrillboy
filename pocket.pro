// POCKETS

PARAMETROS pos_x,pos_y,dx,dy,prof

 	// se supone que Z0 es el nivel de la pieza
	G59P17
	G52 X{pos_x} Y{pos_y} Z{pos_z0}
	G01 Z2 F{vel_z}

	G01 X{r_fresa_p} Y{r_fresa_p} F{vel_xy}
	IF modelo < 2
		M04
	ELSE
		M03
	ENDIF
	ACTIVAR FRESA

	pen_z:=0
	WHILE pen_z<prof
		pen_z:=pen_z+feed
		IF pen_z>prof
			pen_z:=prof
		ENDIF

		// Rectangulo en el borde
		G01 X{r_fresa_p} Y{r_fresa_p}
		G01 Z{-pen_z} F{vel_pen}
		G01 X{dx-r_fresa_p} F{vel_mec}
		G01 Y{(dy-r_fresa_p)} 
		G01 X{r_fresa_p}
		G01 Y{r_fresa_p}

		IF dx>4*r_fresa_p
		
			// Voy y vengo para comer el interior
			ty:=r_fresa_p
			WHILE ty<dy-3*r_fresa_p
				ty:=ty+2*r_fresa_p
				IF ty>dy-3*r_fresa_p
					ty:=dy-3*r_fresa_p
				ENDIF
	
				G01 Y{ty} 
				G01 X{dx-r_fresa_p}
	
				IF ty<dy-3*r_fresa_p
					ty:=ty+2*r_fresa_p
					IF ty>dy-3*r_fresa_p
						ty:=dy-3*r_fresa_p
					ENDIF
					G01 Y{ty}
					G01 X{r_fresa_p}
	
				ENDIF
			END WHILE
	
		ENDIF
	END WHILE

	
	G01 Z{dsz} F{vel_z}
	M05
	PARAR FRESA



