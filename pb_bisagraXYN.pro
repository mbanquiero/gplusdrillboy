// Bisagra puerta blindada, para el marco
PARAMETROS pos_x,npernio

'Comienzo del programa bisagra en el marco
// pb_mds = generalemente 1 o 2 mm
pb_dl:=pb_calle
dy:=esp_pta + pb_mds
solape:=0.5

IF pb_pa==0
	pb_pa:=12	
ENDIF

//pb_mdm -> Desfasaje del marco en relacion a la puerta
// Pero pb_marco le pasa la medida ya desfasada (pos_x ya tiene el desfasaje)
aux_pos_x:=pos_x
G52 X{-pos_pinza+desf_x0+pos_x} Y{dy + fn_dfy} Z{-maq_dz+esp_marco+ fn_dfz+ l_fresa_p}

//esta es una variable auxiliar hasta averiguar si es correcto que a bisagra vaya desde la calle hasta
//el ras del marco y no tomando el pb_db.
aux_db:=dy-pb_dl

G01 X{pb_da-r_fresa_p} Y{-dy} F{vel_xy}
M502
G04 p1
G01 Z0.2 F{vel_z}
G01 Z{-pb_prof} F{vel_pen}
ACTIVAR FRESA

'comienza el mecanizado
G01 X{r_fresa_p} F{vel_mec}
IF pb_rc > r_fresa_p
	//mecanizo con curvas
	G01 Y{-(pb_dl+pb_rc)}
	G02 X{pb_rc} Y{-(pb_dl+r_fresa_p)} I{pb_rc-r_fresa_p} J0 F{vel_curvas}
	G01 X{pb_da-pb_rc} F{vel_mec}
	G02 X{pb_da-r_fresa_p} Y{-(pb_dl+pb_rc)} I0 J{r_fresa_p-pb_rc} F{vel_curvas}
ELSE
	//mecanizo con lineas rectas
	G01 Y{-(pb_dl+r_fresa_p)}
	G01 X{pb_da-r_fresa_p}
ENDIF
G01 Y{-dy-1} F{vel_mec}

lado:=1

pos_y := dy
WHILE pos_y > pb_dl + 3*r_fresa_p - solape

	pos_y := pos_y - 2*r_fresa_p + solape
	IF pos_y < pb_dl + 3*r_fresa_p - solape
		pos_y := pb_dl + 3*r_fresa_p - solape
	ENDIF
	(pos_y : {pos_y}   hasta {pb_dl + 3*r_fresa_p - solape})
	
	// correccion para que al comer el interior no rompa la curvatura rc
	// aproximo la curvatura por una linea		
	dx:=0
	IF pos_y < pb_dl + pb_rc
		
		dx:=pb_dl+pb_rc-pos_y		
		IF lado==1
			G01 X{pb_da-2*(r_fresa_p-solape)-dx} F{vel_mec}			
		ELSE
			G01 X{2*r_fresa_p+dx} F{vel_mec}			
		ENDIF
		
	ENDIF

	G01 Y{-pos_y} F{vel_xy}		
	IF lado==1
		//derecha			
		G01 X{2*r_fresa_p+dx} F{vel_mec}
		lado:=0
	ELSE
		//izquierda
		G01 X{pb_da-2*(r_fresa_p-solape)-dx} F{vel_mec}					
		lado:=1
	ENDIF

END WHILE

PARAR FRESA

// Agujero
IF pb_ra>0		
	'agujero
	//https://lepton.atlassian.net/browse/GPL-15
	pb_ax_aux := pb_ax
	IF mano_pta==0
		pb_ax_aux := pb_da-pb_ax
	ENDIF
	pos_a := pb_ax_aux - pb_ra+r_fresa_p
	
	//Nuevo --> resto el pb_dl(luz_pta - marco) igual que como hace la bisagra
	pos_y_ag:= -(pb_dl + pb_db - pb_az)
	G01 Y{pos_y_ag}
		
	G01 X{pos_a}
	ACTIVAR FRESA	

	IF pb_ra<=r_fresa_p
		G01 Z{-pb_prof-pb_pa} F{vel_pen}
	ELSE
		cant_pasadas:=2
		j:=0
		WHILE j < cant_pasadas		
			G01 Z{-pb_prof-(j+1)*pb_pa/cant_pasadas} F{vel_pen}
			//Nuevo --> resto el pb_dl(luz_pta - marco) igual que como hace la bisagra
			G02 X{pos_a} Y{pos_y_ag} I{pb_ra-r_fresa_p} J0 F{vel_curvas}
			j := j + 1
		END WHILE
	ENDIF
	
ENDIF
M503
PARAR FRESA

(retiro Z)
G01 Z{dsz + pos_pren_z - p_offset_z} F{vel_Z}
'fin de la bisagra en el marco


