//pb_mdm -> Desfasaje del marco en relacion a la puerta
// Pero pb_marco le pasa la medida ya desfasada (pos_x ya tiene el desfasaje)	


(MARCO)
// variables de la drillboy
PROGRAMA initVariables

// espesor del marco
//esp_marco:=10
// posicion de presando
pos_z0 := -maq_dz + esp_marco
pos_pren_z := (pos_z0+dist_prensor_z+drot)

// mdm = compensacion del marco con respecto a la puerta

PLANO XY
G49 G40 G80 G50 G61 G90
G54
G92.1
G21 
S1000
G17

rot_0:=3
G01 Y{pos_y_inicial} F{vel_xy}
IF mano_pta==1
	// puerta izquierda
	XG01 X{dp-pos_pinza}
	// puerta izquierda , el cero esta en el tope izquierdo
	// el exe va transformar las posiciones  
	// G52 X{pos_x} ---->  G52 X{pos_abs_x0 - pos_x}
	// G01 X{pos_x} ---->  G01 X{-pos_x}
	pos_abs_x0 := dp - 2*offset
	(ESPEJAR_X = {pos_abs_x0})
ELSE	
	G01 X{-offset}
ENDIF

(Posicion inicial de la pinza)
M20
G4 P{pausa}
G01 Z{pos_desac_tope-p_offset_z} F{vel_Z}
G52 X{-pos_pinza} Y0 Z{pos_z0}
(POS_PINZA = {pos_pinza})

//  moverAYSegura
pos_y_segura := 60
G01 Y{pos_y_segura} F{vel_xy}
G52 X{-pos_pinza+desf_x0} Y0 Z{-maq_dz+esp_marco}

(DESF_Y={-fn_dfy})
(DESF_Z={-fn_dfz})

IF pernios
	t:=0
	WHILE t<cant_pernios
		xp := pb_pos_x[t]
		
		IF largo_espiga>0
			// mdm  = esp_marco + luz => luz = mdm - esp_marco
			pos_x := xp+pb_mdm-esp_marco  + largo_espiga +desf_tope_marco
		ELSE
			pos_x := xp+pb_mdm +desf_tope_marco
		ENDIF
		largo_pernio:=pb_da

		largo_pernio:=pb_da
		(PERNIO : {t+1} de {cant_pernios})
		// busco la posicion X
		PROGRAMA moverPinzaPos CON pos_x + largo_pernio + 50

		// me posicion en X pos del pernio
		G01 X{pos_x} F{vel_pinza}

		// BISAGRA PARA PUERTA BLINDADA
		PROGRAMA pb_bisagraXYN CON pos_x,t

		// restauro el offset de Y para que en el siguietne paso se mueva OK a la posicion Y segura
		G52 Y0
		
		t:=t+1
	END WHILE
ENDIF

PROGRAMA moverAZSegura
			
// finalizacion
M189

//  moverAYSegura
G01 Y{pos_y_segura} F{vel_xy}

// FINALIZAR PIEZA
//  terminar el trabajo ahora:
// entonces retiro la pieza y finalizo todo
PROGRAMA retirarPieza


/*

// Cerradura del marco
IF vaciados!=0
	IF doble_marco==0
		// LO MANDO AL CERO ANTES DE EMPEZAR EL PROGRAMA DE MARCOS
		IF pb_pos_x1!=0 .AND. pernios!=0
			IF modelo>=2 .AND. palpador_m==1
				G01 Z{dsz} F{vel_z}
				G54
				G92.1				
				G01 X{-dist_ax} F{vel_xy}
				G01 Y0				
				IF modelo == 3 .AND. retropalpador == 0
					G28.1 Z{dsz+dpalpador_z}
					G01 Z5 F{vel_z}
				ELSE
					G28.1 Z{dsz}
				ENDIF
			ELSE
				G54
				G92.1
				G01 Z0 F{vel_z}
				G01 X{-dist_ax} Y0 F{vel_xy}
			ENDIF			
			// LO PONGO EN PAUSA 
			M01
		ENDIF		
		
		
		desf_x0 := 0
		IF mano_pta==0
			IF !tope_mesa
				G59P11
			ELSE
				M5911
			ENDIF
			IF modelo == 3
				//fv1, cuando las bisagras en el marco usan el G59P11 la chapita usa el G59P10
				//la fv1 tiene el g59p11 en X = 0 y las posiciones en x tienen que estar compensadas a mano
				//por ese motivo sumo a todas las x de las bisagras (no de la chapita porque usa el G59P10) desf_x0
				 desf_x0:= alto_pta + pb_mdm + pb_mste
			ENDIF
		ELSE
			IF !tope_mesa
				G59P10
			ELSE
				M5910
			ENDIF
		ENDIF
	ENDIF

	// Hay 3 vaciados o 5, el del medio es grande y el resto peque�os
	t:=0	
	vg := if(cant_vaciados==3,1,2)
	WHILE t<cant_vaciados
		dv:=if(t==vg,pb_dvg,pb_dvc)
		fdv:=if(t==vg,pb_fdvg,pb_fdvc)
		IF pb_frente			
			PROGRAMA ranura_frente CON pb_pos_v[t]+pb_mdm-(fdv-dv)/2-desf_x0,K,fdv,pb_fesp,pb_fprof
		ENDIF
		PROGRAMA cajeadoXY CON pb_pos_v[t]+pb_mdm-desf_x0,K,dv,pb_esp_vaciado,pb_prof_vaciado		
		t:=t+1
	END WHILE

ENDIF
M05
*/
