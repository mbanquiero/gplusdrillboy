
//Invierto la K poque el marco va en espejo con respecto al canto superior

'programa bisagra invisible en el marco

IF de<2*r_fresa_p
	de:=2*r_fresa_p
ENDIF
//si el da_m es igual a 0 es porque el cajeado va centrado en el ancho de la bisagra
IF da==0
	da:=(dc-db)/2
ENDIF
IF ri==0
	ri:=de/2
ENDIF
IF rd==0
	rd:=de/2
ENDIF

G01 X{dc/2} F{vel_mec}
G01 Y{K*(r_fresa_p+dd)} F{vel_y}
ACTIVAR FRESA

IF npernio==0 .AND. palpador_m==0
		M03
ENDIF


G01 Z0.2 F{vel_z}
aux_z:=0

IF de<=2*r_fresa_p
	//si con el diametro de la fresa alcanza para comer toda la bisagra
	'con una pasada la fresa come todo el ancho
	aux_z:= -feed
	
	//primer pasada
	G01 Z{aux_z} F{vel_pen}
	//como el lado izquierdo
	G01 X{r_fresa_p} F{vel_mec}
	//luego el derecho
	G01 X{(dc-r_fresa_p)} F{vel_mec}
	lado:=0
	
	//si no alcanzo con 1 pasada para alcanzar la profundidad deseada
	WHILE aux_z > -prof
		IF aux_z+prof>feed
			aux_z:=aux_z-feed
		ELSE
			aux_z:=-prof
		ENDIF
		G01 Z{aux_z} F{vel_pen}	
		IF lado == 0
			//como el lado izquierdo
			G01 X{r_fresa_p} F{vel_mec}
			lado:=1
		ELSE
			//luego el derecho
			G01 X{(dc-r_fresa_p)} F{vel_mec}
			lado:=0
		ENDIF
	END WHILE

	'hago el cajeado
	G01 X{(r_fresa_p+da)} F{vel_mec}
	lado:=0
	aux_z:=-prof
	WHILE aux_z>-prof2
		IF aux_z+prof2>feed
			aux_z:=aux_z-feed
		ELSE
			aux_z:=-prof2
		ENDIF
		G01 Z{aux_z} F{vel_pen}
		//para hacer el cajeado va de lado a lado y baja entre ida y vuelta
		IF lado
			G01 X{(r_fresa_p+da)} F{vel_mec}
			lado:=0
		ELSE
			G01 X{(da+db-r_fresa_p)} F{vel_mec}
			lado:=1
		ENDIF
	END WHILE

ELSE

	WHILE aux_z > -prof
		IF aux_z+prof>feed
			aux_z:=aux_z-feed
		ELSE
			aux_z:=-prof
		ENDIF
		
		G01 Z{aux_z} F{vel_pen}

		//si el radio izquierdo es menor o igual al radio de la fresa en ves de un movimiento circular hago uno recto
		'perimetro de la bisagra
		IF ri<=r_fresa_p
			G01 X{r_fresa_p} F{vel_mec}
			G01 Y{K*(de+dd-r_fresa_p)}
		ELSE
			G01 X{ri} F{vel_mec}
			IF K==1
				G02 X{r_fresa_p} Y{ri+dd} I0 J{ri-r_fresa_p} F{vel_curvas}
				G01 Y{de+dd-ri} F{vel_mec}
				G02 X{ri} Y{dd+de-r_fresa_p} I{ri-r_fresa_p} J0 F{vel_curvas}
			ELSE
				G03 X{r_fresa_p} Y{K*(ri+dd)} I0 J{K*(ri-r_fresa_p)} F{vel_curvas}
				G01 Y{K*(de+dd-ri)} F{vel_mec}
				G03 X{ri} Y{K*(dd+de-r_fresa_p)} I{(ri-r_fresa_p)} J0 F{vel_curvas}
			ENDIF
		ENDIF

		//si el radio derecho es menor o igual al radio de la fresa en ves de un movimiento circular hago uno recto
		IF rd<=r_fresa_p
			G01 X{(dc-r_fresa_p)} F{vel_mec}
			G01 Y{K*(dd+r_fresa_p)}
		ELSE
			G01 X{(dc-rd)} F{vel_mec}
			IF K==1
				G02 X{dc-r_fresa_p} Y{dd+de-rd} I0 J{r_fresa_p-rd} F{vel_curvas}
				G01 Y{dd+rd} F{vel_mec}
				G02 X{dc-rd} Y{dd+r_fresa_p} I{r_fresa_p-rd} J0 F{vel_curvas}
			ELSE
				G03 X{(dc-r_fresa_p)} Y{K*(dd+de-rd)} I0 J{K*(r_fresa_p-rd)} F{vel_curvas}
				G01 Y{K*(dd+rd)} F{vel_mec}
				G03 X{(dc-rd)} Y{K*(dd+r_fresa_p)} I{(r_fresa_p-rd)} J0 F{vel_curvas}
			ENDIF
		ENDIF
		//voy rapido porque ya comio
		G01 X{(dc/2)} F{vel_mec}
			
	END WHILE

	'Hago el cajeado
	//si el radio izquierdo es menor o igual al radio de la fresa en ves de un movimiento circular hago uno recto
	aux_z:=-prof
	WHILE aux_z>-prof2
		IF aux_z+prof2>feed
			aux_z:=aux_z-feed
		ELSE
			aux_z:=-prof2
		ENDIF

		G01 X{(da+db/2)} F{vel_mec}
		G01 Z{aux_z} F{vel_pen}
		IF ri<=r_fresa_p
			G01 X{(da+r_fresa_p)} F{vel_mec}
			G01 Y{K*(de+dd-r_fresa_p)}
		ELSE
			G01 X{(da+ri)} F{vel_mec}
			IF K==1
				G02 X{da+r_fresa_p} Y{ri+dd} I0 J{ri-r_fresa_p} F{vel_curvas}
				G01 Y{de+dd-ri} F{vel_mec}
				G02 X{da+ri} Y{dd+de-r_fresa_p} I{ri-r_fresa_p} J0 F{vel_curvas}
			ELSE
				G03 X{(da+r_fresa_p)} Y{K*(ri+dd)} I0 J{K*(ri-r_fresa_p)} F{vel_curvas}
				G01 Y{K*(de+dd-ri)} F{vel_mec}
				G03 X{(da+ri)} Y{K*(dd+de-r_fresa_p)} I{(ri-r_fresa_p)} J0 F{vel_curvas}
			ENDIF
		ENDIF

		//si el radio derecho es menor o igual al radio de la fresa en ves de un movimiento circular hago uno recto
		IF rd<=r_fresa_p
			G01 X{(da+db-r_fresa_p)} F{vel_mec}
			G01 Y{K*(dd+r_fresa_p)}
		ELSE
			G01 X{(da+db-rd)} F{vel_mec}
			IF K==1
				G02 X{da+db-r_fresa_p} Y{dd+de-rd} I0 J{r_fresa_p-rd} F{vel_curvas}
				G01 Y{dd+rd} F{vel_mec}
				G02 X{da+db-rd} Y{dd+r_fresa_p} I{r_fresa_p-rd} J0 F{vel_curvas}
			ELSE
				G03 X{(da+db-r_fresa_p)} Y{K*(dd+de-rd)} I0 J{K*(r_fresa_p-rd)} F{vel_curvas}
				G01 Y{K*(dd+rd)} F{vel_mec}
				G03 X{(da+db-rd)} Y{K*(dd+r_fresa_p)} I{(r_fresa_p-rd)} J0 F{vel_curvas}
			ENDIF
		ENDIF
		G01 X{(da+db/2)} F{vel_mec}
	END WHILE
ENDIF

