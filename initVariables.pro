	// initVariables
	pieza_dx := alto_pta
	pieza_dy := ancho_pta
	pieza_dz := esp_pta
	pos_pinza := offset
	ancho_pinza:=pinza_dx
	pausa := 0.5
	pausa_prensor := 0.5
	// dist. seguridad X
	dsx := 10
	pos_desac_tope := 0

	// ----------------
	dfx:=0
	dfy:=0
	dfz:=0
	dx_canto_i:=0
	l_fresa := l_fresa_p
	pos_z0 := -maq_dz + pieza_dz
	pos_pren_z := (pos_z0+dist_prensor_z+drot)
	presion_pren := 12
	pos_desac_tope := 0
	pos_z_cerca := 3
	rot_0:=0
	desf_cero := (desf_x1 - dist_mesas )/2
	ant_pos_pinza := 0
	dx_palpa := if(mano_pta==0,dx_palpa,-dx_palpa)

	// distancia de seguridad Y para prensar la pieza
	mar_ys_rot := if( pieza_dy>150,if(moldura,60,100),pieza_dy/2)
	
	// https://lepton.atlassian.net/browse/GPL-34
	// posicion Y inicial
	pos_y_inicial := 120

	//https://lepton.atlassian.net/browse/GPL-35
	// desfsajes pernios en el marco dp_pernio[t]
	pp_dm_pernio_0 := 150
	pp_dm_pernio_1 := 0
	pp_dm_pernio_2 := 0
	pp_dm_pernio_3 := 0
	pp_dm_pernio_4 := 0
	
	