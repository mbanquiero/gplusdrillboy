// posicion, ancho, espesor, prof
// pos_y= -36 o 36 generalmente
// Le pasa el vertice x0 de la izquierda, OJO: La ubicacion en Y, la hace centrada 
// Con respecto al espesor de la puerta. Por eso solo le paso 36 o -36 
// Si pos_y es negativo -> todas las Y son negativas y viceversa
PARAMETROS pos_x,pos_y,ancho,esp,prof

G52 X{pos_x} Y{pos_y} Z{l_fresa_p}

aux_K:=1
IF pos_y<0
	aux_K:=-1
ENDIF

G01 X{r_fresa_p} Y{aux_K*((esp_pta-esp)/2 + r_fresa_p)} F{vel_xy}

pen_z:=0
flag:=0
WHILE pen_z < prof
	pen_z:=pen_z+feed
	IF pen_z>prof
		pen_z:= prof
	ENDIF

	G01 X{r_fresa_p} F{vel_mec}
	G01 Y{aux_K*((esp_pta-esp)/2 + r_fresa_p)}
	IF flag==0
		M04
		ACTIVAR FRESA
		flag:=1
	ENDIF
	G01 Z{-pen_z} F{vel_pen}
	G01 X{ancho-r_fresa_p} F{vel_mec}
	G01 Y{aux_K*((esp_pta+esp)/2 - r_fresa_p)} 
	G01 X{r_fresa_p}

END WHILE
PARAR FRESA
M05

G01 Z20 F{vel_z}


