// FUNCION moverAZSegura
	(mover a z segura)
	// pos_z_segura es una pos. absoluta (le voy a restar el offset de z)
	// en el caso de la chapita tiene que tener en cuenta el espesor del batiente 
	// https://lepton.atlassian.net/browse/GPL-26
	// pieza_dz  en este caso es esp_batiente (y no esp. del marco)
	// pos_z0 := -maq_dz + pieza_dz
	// pos_pren_z := (pos_z0+dist_prensor_z+drot)
	pos_z_segura := dsz -maq_dz + esp_batiente + dist_prensor_z+drot
	IF pos_z_segura > 0
		pos_z_segura := 0
	ENDIF
	G01 Z{pos_z_segura - p_offset_z} F{vel_Z}
