// header
G0 G49 G40 G80 G50 G61 G90
G54
G92.1
G21
G17
M05
M9
G1 A0 f20000 

// variables de la drillboy
PROGRAMA initVariables
G01 Y{pos_y_inicial} F{vel_xy}
IF mano_pta==1
	// puerta izquierda
	XG01 X{dp-pos_pinza}
	// puerta izquierda , el cero esta en el tope izquierdo
	// el exe va transformar las posiciones  
	// G52 X{pos_x} ---->  G52 X{pos_abs_x0 - pos_x}
	// G01 X{pos_x} ---->  G01 X{-pos_x}
	pos_abs_x0 := dp - 2*offset
	(ESPEJAR_X = {pos_abs_x0})
ELSE	
	G01 X{-offset}
ENDIF

(Posicion inicial de la pinza)
M20
G4 P{pausa}
G01 Z{pos_desac_tope-p_offset_z} F{vel_Z}
G52 X{-pos_pinza} Y0 Z{pos_z0}
(POS_PINZA = {pos_pinza})

M190
G54
G18
M110
(DESF_Y={-maq_dfresa})



IF pernios
	(PERNIOS)
	//  moverAYSegura: si es cero, prensa arriba de la puerta usando los cuernitos del cabezal
	pos_y_segura := 0
				
	(muevo Y a la posicion previa palpado directamente)
	G01 Y{ancho_pta+maq_dfresa-10} F{vel_xy}
	G01 A0 F{vel_rot}
	G52 X{-pos_pinza+desf_x0} Y0 Z{-maq_dz}

	// muevo a z segura
	PROGRAMA moverAZSegura

	t:=0
	WHILE t<cant_pernios
		pos_x := pb_pos_x[t]
		IF pos_x!=0
			(PERNIO : {t+1} de {cant_pernios})
			IF t==0
				primer_pernio:=1
				// me posiciono en el canto superior (solo en el primer pernio)
				// z segura 
				PROGRAMA moverAZSegura
				// arriba de la puerta
				G01 Y{ancho_pta+maq_dfresa-10} F{vel_xy}
			ELSE
				// no es el primer pernio, tiene que buscar la posicion X
				primer_pernio:=0
				PROGRAMA moverPinzaPos CON pos_x 
			ENDIF

			// me posicion en X pos del pernio
			G01 X{pos_x} F{vel_pinza}
			
			// hago la bisagra pp dicha - BISAGRA PARA PUERTA BLINDADA
			PROGRAMA pb_bisagra CON pos_x,pb_prof,primer_pernio

			G01 Y{ancho_pta+maq_dfresa+dsys} F{vel_xy}
			// apago la fresa
			M05
			// muevo a z segura
			// paso al siguiente pernio
			t := t + 1
		ENDIF
	END WHILE

	// el offset de Z esta en G62 Z{-maq_dz}
	IF cerradura!=0
		// si va a hacer cerradura, retiro el cabezal para que baje sin tocar contra nada
		PROGRAMA moverAZSegura
	ELSE
		// si solo hace pernios, dejo el cabezl en
		G01 Z0 F{vel_Z}
	ENDIF
	
	(fin pernios)
	M189
ENDIF	
		
IF cerradura!=0
	PROGRAMA pb_cerradura2
ENDIF

//  terminar el trabajo: retiro la pieza y finalizo todo
PROGRAMA retirarPieza


