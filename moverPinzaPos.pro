(--------------- moverPinzaPos)
	PARAMETROS pos_deseada 
	(posiciono la pinza: pos_pinza = {pos_pinza} ,  pos_deseada = {pos_deseada} )
	(offset = {offset} ,  desf_cero = {desf_cero} )

	
	WHILE pos_pinza != pos_deseada

		// moverAYSegura
		IF pos_y_segura>0
			G52 Y0
			G01 Y{pos_y_segura} F{vel_xy}
		ENDIF
		
		ant_pos_pinza := pos_pinza

		IF abs(pos_pinza - pos_deseada) <= dp-ds
			//con un solo movimiento alcanza
			pos_pinza := pos_deseada
		ELSE
			//se necesita mas de un movimiento para llegar al punto solicitado
			IF pos_pinza < pos_deseada
				pos_pinza := pos_pinza + dp-ds
			ELSE
				pos_pinza := pos_pinza - dp+ds
			ENDIF
		ENDIF

		(POS_PINZA = {pos_pinza} , p_pos_pinza = {p_pos_pinza})
		G54
		G92.1
		G52 X0 Y0

		//muevo la pieza de tal forma de que quede la siguiente posicion de la pinza en -offset (home)
		//la pinza siempre agarra a la pieza en el home. Salvo el caso de que la pinza este tenga recorrido suficiente
		//para agarrar la siguiente posicion sin mover la pieza donde esta actualmente.
		//pos_x tiene la posicion de la fresa con respecto a la pieza. con eso se puede calcular cuanto recorrido para la izquierda tengo

		pos_sig_pinza := p_pos_pinza

		IF pos_pinza > ant_pos_pinza
			// Avanza se mueve hacia la derecha la pieza caso mas comun
			
			IF pos_sig_pinza + offset < pos_pinza - ant_pos_pinza
				//el espacio a la izquierda no es suficiene, muevo a la derecha la pieza para que pueda agarrar
				//siguiente posicion
				(Muevo porque no hay espacio suficiente para la siguiente pinza)
				pos_sig_pinza := (pos_pinza - ant_pos_pinza) - offset
				(pos_sig_pinza = {pos_sig_pinza})
				
			ENDIF

		ELSE
			// se mueve para la izquierda
			IF dp - (p_pos_pinza + offset) < ant_pos_pinza - pos_pinza
				//el espacio a la derecha no es suficiene, muevo a la derecha la pieza para que pueda agarrar
				//siguiente posicion
				pos_sig_pinza := dp - (ant_pos_pinza - pos_pinza) - offset
			ENDIF
		ENDIF

		IF pieza_dx > dist_mesas
			//Para poder abrir la pinza y moverla la pieza tiene que estar agarrada
			//por los dos prensores entrales para que no se mueva
			//Verifica el prensor derecho
			IF pos_sig_pinza + ant_pos_pinza < dist_mesas + 10 + desf_cero
				(Muevo la pieza para que quede agarrada por los dos prensores)
				pos_sig_pinza := dist_mesas - ant_pos_pinza + 10 + desf_cero
			ENDIF
			
			//Verifica el prensor izquierdo
			IF pos_sig_pinza + ant_pos_pinza > pieza_dx - 10 - desf_cero
				(Muevo la pieza para que quede agarrada por los dos prensores)
				pos_sig_pinza := pieza_dx - ant_pos_pinza - 10 - desf_cero
			ENDIF
		ENDIF

		IF pos_sig_pinza != p_pos_pinza
			G01 X{pos_sig_pinza} F{vel_pinza}
		ENDIF

		IF pos_y_segura>0
			// Para el cambio cambio de pinza se activa el prensor central
			//activarPrensor
			G52 Z{pos_pren_z}
			G01 Z{-presion_pren} F{vel_Z}
		ENDIF
		
		// abrirPinza
		M21	
		G4 P{pausa_prensor}
			

		//Me posiciono en la siguiente pinza (puede ir rapido porque no tiene la pieza agarrada)
		(POS_PINZA = {pos_pinza} , p_pos_pinza = {p_pos_pinza} , ant_pos_pinza = {ant_pos_pinza} )
		G01 X{p_pos_pinza - (pos_pinza - ant_pos_pinza)} F{vel_xy}

		//  agarrarPieza
		//activo la pinza
		M20
		G4 P{pausa}
		IF pos_y_segura>0
			//desactivo el prensor
			//G01 Z{dsz + pos_pren_z - p_offset_z} F{vel_Z}
			PROGRAMA moverAZSegura
		ENDIF


		//dejo el offset en la posicion de la pinza
		G52 X{-pos_pinza + desf_x0} Y0 Z{pos_z0}

		//indica cuando entra a la cara x primera vez
		entra_cara := 1
	END WHILE

	//restauro los valores de offset Y y Z y seteo el X de acuerdo a la pinza
	G52 X{-pos_pinza + desf_x0} Y0 Z{pos_z0+l_fresa+drot}
	
(--------------- FIN moverPinzaPos)	


