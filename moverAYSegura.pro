// FUNCION moverAYSegura
	IF p_pos_y < DSY 
		//Subo como minimo a la distancia de seguridad para que no choque el cabezal
		G01 Y{DSY} F{vel_xy}
	ENDIF
	
	IF p_pos_y >= pieza_dy - min_ys_rot && pieza_dy > DSY + mar_ys_rot
		//Esta muy cerca del canto superior voy a posicion Y minima
		(MUEVO Y A POSICION SEGURA LEJOS DEL CANTO SUPERIOR)
		G01 Y{pieza_dy - mar_ys_rot} F{vel_xy}
	ENDIF
RETURN
