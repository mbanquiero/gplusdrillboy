#var op mano_pta := 0	{Derecha,Izquierda}		// Mano de la Puerta

#var mm pos_x1:= 120			// Pos 1era Bisagra :
#var mm pos_x2:= 955			// Pos 2era Bisagra :
#var mm pos_x3:= 1795			// Pos 3era Bisagra :
#var mm pos_x4:= 0				// Pos 4era Bisagra :
#var mm pos_x5:= 0				// Pos 5era Bisagra :

#var mm ancho_pta:= 825			// Ancho Puerta :
#var mm esp_pta:= 35		// Esp de la puerta:
#var mm maq_dz:= 140			// prof maquina:
#var mm maq_dfresa:= 325		// Dist. Fresa:
#var mm da:= 26			// Antecuello:
#var mm db:= 23			// Cuello:
#var mm dc:= 100.5			// Largo Total:
#var mm dd:= 7			// Salida:
#var mm prof:= 2.1			// Profundidad:

#var sn circular:= 1		// Cajeado Circular
#var mm diam:= 25		// Diametro del Cajeado

// maq_dx = indica el desfaje en el eje x
// maq_dpx<n> = idem para el pernio especifico <n> 
// doblep indica si hay que hacer doble pasada para los pernios

//Las primeras versiones de la fv1 no palpan directo, sino que retropalpan.
//La version Nueva palpa directo esto es a pedido de marcelo el 16/4/09. 
//La variable retropalpador sirve para diferenciar las 2 versiones de la maquina (si vale 1 retro_palpa, sino es la version nueva)



PLANO XY

// Radio de la fresa = r_fresa_s

G49 G40 G80 G50 G61 G90
G54
G92.1
G21 
S1000

IF !retropalpador
	//retira la punta del palpador que est� tocando el topecito met�lico, y se puede mover sin enganchar nada.
	G01 Z2 F{vel_z}
ENDIF

aux_maq_dz:=maq_dz
maq_dz := maq_dz - lambor_dz

dpalpador_z := dpalpador_z 

//invierte la posicion de la cerradura, para que tome la posicion desde abajo
pos_picaporte := alto_pta - pos_picaporte

IF contra_puerta
	mano_pta := 1- mano_pta
	IF tipo_pernio==0		
		//invierto la posicion del cuello para que quede como el marco
		da:= dc-da-db
	ENDIF
ENDIF

IF mano_pta==1
	K:=-1
ELSE
	//G52 X{alto_pta-(pos_x1+maq_dx)}
	K:=1
ENDIF

G01 X0 F{vel_xy}
G01 Y0 F{vel_y}

// al ancho del pernio (pb_db) le resto la salida del pernio
// para el pernio de la blindada
pb_db:=pb_db-salida
// y para el pernio de paso
dd:=dd-salida

pb_rebaje_canto:=rebaje_canto

// Le tengo que decir si es la primer bisagra, debido a que el ref. en y lo hace solo la primera vez
// a la prof del pernio le sumo dang, x el angulo del canto de la puerta

//es el desfasaje de la posicion del tope en x que se va a usar
dpos_x0:=0

// Desfasajes: el desf. Z se lo aplico al cuello del pernio dd, y el x la posicion x1
IF pernios
	IF tipo_pernio==0
		// BISAGRA COMUN DE LA PUERTA DE PASO
		aux_prof := prof
		prof := prof + dang
		aux_dd:=dd
		aux_dc:=dc
		aux_calle:=calle
		t:=0
		WHILE t < cant_pernios
			//invierto el orden de los pernios para que comience por el mas cercano
			desp:= cant_pernios-t-1
			xp := pp_pos_x[desp]+maq_dx+dc
			xp := alto_pta-xp
			x  := xp
			
			IF ancho_pta<=600
				dd:=aux_dd+desf2_sup_z(x)
				dc:=aux_dc+desf2_sup_l(x)		
				aux_x:=xp+desf2_sup_x(x)
			ELSE
				dd:=aux_dd+desf_sup_z(x)
				dc:=aux_dc+desf_sup_l(x)
				aux_x:=xp+desf_sup_x(x)
			ENDIF
			//modifico las variables del marco para que se adapten a la puerta
			calle:=esp_pta-(dd+de)
			mds:=0
			
			
			IF anuba == 0
				//bisagras comunes
				PROGRAMA bisagraPXY CON aux_x+dpos_x0,K,t
			ELSE
				//bisagra anuba
				PROGRAMA anubaPXY CON aux_x+dpos_x0,K,t
			ENDIF
			
			pri:=0
			t := t + 1
		END WHILE
	
		prof:=aux_prof
		dc:=aux_dc
		dd:=aux_dd
	ELSE
		// BISAGRA PARA PUERTA BLINDADA
		aux_db := pb_db
		aux_da := pb_da
		aux_pb_prof:= pb_prof
		pb_prof:= pb_prof + dang
				
		t:=0			
		WHILE t < cant_pernios
			//invierto el orden de los pernios para que comience por el mas cercano
			desp:= cant_pernios-t-1
			xp:=pp_pos_x[desp]+maq_dx+pb_da
			xp := alto_pta-xp
			x  := xp
			
			IF ancho_pta<=600
				pb_db:=aux_db+desf2_sup_z(x)
				pb_da:=aux_da+desf2_sup_l(x)
				aux_x:=xp+desf2_sup_x(x)
			ELSE
				pb_db:=aux_db+desf_sup_z(x)
				pb_da:=aux_da+desf_sup_l(x)
				aux_x:=xp+desf_sup_x(x)
			ENDIF		
			
			//IF pb_prof != 0
			//	aux_prof:= pb_prof + dang
			//ELSE
				//si la profundidad es 0 se utiliza la bisagra blindada para hacer agujeros en el 
			//	aux_prof:=0
			//ENDIF
			//modifico las variables del marco para que se adapten a la puerta
			pb_calle:=esp_pta - pb_db
			pb_mds:=0
			
			PROGRAMA pb_bisagraPXY CON aux_x+dpos_x0,K,t
			t := t + 1
		END WHILE
			
		pb_prof:= aux_pb_prof
		pb_da:=aux_da
		pb_db:=aux_db
	ENDIF
ENDIF


IF cerradura_antes
	'Fin del trabajo Cerradura antes
	G52 X0 Y0
	M05
	G92.1
	G28.1 A0
	G01 Y0 F{vel_y}
	G01 X{-dist_ax} F{vel_xy}
	G28.1 X{-dist_ax} Y0
	//version nueva referencia ultimo el Z
	IF !anuba
		//palpo
		G28.1 Z{dsz+dpalpador_z}
	ELSE
		//no palpo
		G28.1 Z0
	ENDIF
ELSE
	'finalizo
	IF pernios
		G01 Z{dsz+rebaje_canto} F{vel_z}
		G54
	ENDIF
	IF !cerradura
		//si no hace la cerradura finalizo
		IF modelo != 3
			M05
		ENDIF
		G52 X0 Y0
		IF retropalpador
			//version con retropalpador
			G28.1 Z{dsz}
		ENDIF
		IF modelo == 3
			M05
		ENDIF
		G92.1
		IF retropalpador == 0
			G28.1 A0
		ENDIF
		G01 Y0 F{vel_y}
		G01 X{-dist_ax} F{vel_xy}
		G28.1 X{-dist_ax} Y0
		IF !retropalpador
			//version nueva referencia ultimo el Z
			IF !anuba
				//palpo
				G28.1 Z{dsz+dpalpador_z}
			ELSE
				//no palpo
				G28.1 Z0
			ENDIF
		ENDIF
	ELSE
		IF pernios
			//elimino el offset del palpado
			IF retropalpador
				G28.1 Z{dsz}
			ENDIF
			G92.1
			M05
		ENDIF
		
		G01 X{pos_picaporte-cdf} F{vel_xy}	
		
	ENDIF

	IF cerradura .AND. pernios
		//pongo en pausa para rotar la pieza
		M1
	ENDIF
ENDIF

  