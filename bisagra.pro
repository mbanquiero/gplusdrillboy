PARAMETROS pos_x,prof_pernio,primer_pernio

//db2 lo uso para hacer la bisgra inclinada. y representa la segunda base del pernio
//cuando la bisagra es izquierda y tiene inclinacion en el cuello se debe penetrar mas
//abajo para no romper la inclinacion de la misma. del_z toma el valor del cuello para penetrar por debajo de la inclinacion

//si es simetrico es una bisagra inclinada de las dos lados
db3:=0
IF db2==0 .OR. db2==db
	'bisagra normal
	db2:=db
ELSE
	'bisagra inclinada
	IF db2<2*r_fresa_s
		db2:=2*r_fresa_s
	ENDIF
	IF simetrica
		db3:=(db-db2)/2
		db2:=db2 + db3
	ENDIF
ENDIF

IF ri==0
	ri:=de/2
ELSE
	IF ri<r_fresa_s
		ri:=r_fresa_s
	ENDIF
ENDIF
IF rd==0
	rd:=de/2
ELSE
	IF rd<r_fresa_s
		rd:=r_fresa_s
	ENDIF
ENDIF

IF doblep
	//doble pasada
	n_pasadas:=2
ELSE
	// pasada simple
	n_pasadas:=1
ENDIF

profundiza:=prof_pernio/n_pasadas

//pos_y0 es la pos de la sup de la madera desde la punta de la fresa
pos_y0:=ancho_pta+maq_dfresa
aux_y:= pos_y0

// computo el offset  para que X0 sea pos_x, Z0 sea la parte atras de la puerta (o sea el tope de MESA)
// ojo que la Z aumenta para adelante, es decir Z negativo profundiza 
G52 X{-pos_pinza+desf_x0 + pos_x} Z{-maq_dz}



IF palpar
	//posiciono el palpador 
	// el cabezal esta ya en posicion X de la bisagra y arriba a una cierta distancia
	G01 X{-dx_palpa} F{vel_xy}
	
	IF primer_pernio
		G01 Y{pos_y0 - 10} F{vel_xy}
		// posicion de palpado 20 + esp de la puerta
		G01 Z{pieza_dz+20} F{vel_z}
	ELSE
		G01 Z{pieza_dz+20} F{vel_z}
		G01 Y{pos_y0 - 10} F{vel_xy}
	ENDIF
	
	//instruccion de palpado
	palpa_pos_x := 0
	G31 Y15 F{vel_palpa}
	
	//Grabo la diferencia de palpado en la variable #2 del mach
	//La variable #5212 del mach tiene el offset y actual
	//Si hago un G92 Y0 hace que la variable #5212 tenga la posicion actual absoluta
	//el offset de Y es = 0
	G92 Y0
	#2 = [#5212 - { -dy_palpa}]
	G92.1
	(Fin Palpado Canto superior)
	(Correccion de offset por palpado)
	G52 X{-pos_pinza+desf_x0 + pos_x} Y[#2] Z{-maq_dz}

ENDIF

//Comieza la bisagra propiamente dicha
//Siempre es Puerta Derecha, si fuera izquierda, luego la post procesa
IF invisible
	// Bisagra invisible
	PROGRAMA invisible
ELSE
	// bisagra comun
	WHILE n_pasadas>0
		aux_y:=aux_y-profundiza

		'cuello de la bisagra
		G01 Y2 F{vel_xy}
		
		M03
		
		G01 Z0 F{vel_mec}
		
		IF mano_pta==0
			// puerta derecha: entrada por la derecha del cuello normal
			G01 X{da+db-db3-r_fresa_s} F{vel_xy}
			G01 Y{aux_y-pos_y0} F{vel_pen}
			ACTIVAR FRESA
			G01 X{da+db-r_fresa_s} Z{dd+r_fresa_s} F{vel_mec}
			G01 X{da+db-db3-r_fresa_s} Z0
			G01 X{da+(db-db2)+r_fresa_s} F{vel_mec}
			//el movimiento z esta dentro porque la bisagra inclinada tiene un movimiento en diagonal
			//si el cuello es mas alto de r_fresa y mas ancho de 28
			//se hacen dos pasadas para no dejar huecos
			IF dd>r_fresa_s .AND. db>4*r_fresa_s .AND. db2>=db
				// Ojo -> si el cuello de bisagra es superior a 28 queda un lugar sin comer
				'como el interior del cuello
				G01 Z{dd-r_fresa_s} F{vel_mec}
				G01 X{da+db-r_fresa_s} F{vel_mec}
				G01 X{da+r_fresa_s}
			ENDIF
			G01 X{da+r_fresa_s} Z{dd+r_fresa_s} F{vel_mec}
			
		ELSE
			// puerta izquierda: entrada por la izquierda del cuello normal
			G01 X{2*da+db - (da+db-db3-r_fresa_s)} F{vel_xy}
			G01 Y{aux_y-pos_y0} F{vel_pen}
			ACTIVAR FRESA
			G01 X{2*da+db - (da+db-r_fresa_s)} Z{dd+r_fresa_s} F{vel_mec}
			G01 X{2*da+db - (da+db-db3-r_fresa_s)} Z0
			G01 X{2*da+db - (da+(db-db2)+r_fresa_s)} F{vel_mec}
			
			//el movimiento z esta dentro porque la bisagra inclinada tiene un movimiento en diagonal
			//si el cuello es mas alto de r_fresa y mas ancho de 28
			//se hacen dos pasadas para no dejar huecos
			IF dd>r_fresa_s .AND. db>4*r_fresa_s .AND. db2>=db
				// Ojo -> si el cuello de bisagra es superior a 28 queda un lugar sin comer
				'como el interior del cuello
				G01 Z{dd-r_fresa_s} F{vel_mec}
				G01 X{2*da+db - (da+db-r_fresa_s)} F{vel_mec}
				G01 X{2*da+db - (da+r_fresa_s)}
			ENDIF
			G01 X{2*da+db - (da+r_fresa_s)} Z{dd+r_fresa_s} F{vel_mec}
			
			
		ENDIF


		'cuerpo de la bisagra

		IF de<=2*r_fresa_s
			// BISAGRA COMUN
			G01 X{r_fresa_s} F{vel_mec}
			G01 X{dc-r_fresa_s}
		ELSE
			// Bisagra hibrida
			IF da>ri
				G01 X{ri} F{vel_mec}
				G03 X{r_fresa_s} Z{dd+ri} I0 K{ri-r_fresa_s} F{vel_curvas}
			ELSE
				G01 X{r_fresa_s} F{vel_mec}
			ENDIF

			G01 Z{dd+de-ri} F{vel_mec}

			G03 X{ri} Z{dd+de-r_fresa_s} I{ri-r_fresa_s} K0 F{vel_curvas}

			IF rd<=r_fresa_s
				G01 X{dc-r_fresa_s} F{vel_mec}
				G01 Z{dd+r_fresa_s} F{vel_mec}
			ELSE
				G01 X{dc-rd} F{vel_mec}
				G03 X{dc-r_fresa_s} Z{dd+de-rd} I0 K{r_fresa_s-rd} F{vel_curvas}
				G01 Z{dd+rd} F{vel_mec}
				G03 X{dc-rd} Z{dd+r_fresa_s} I{r_fresa_s-rd} K0 F{vel_curvas}
			ENDIF
			G01 X{2*r_fresa_s-1} F{vel_mec}
		ENDIF

		n_pasadas:=n_pasadas-1

		PARAR FRESA
		// si hay mas pasadas me retiro 2 mm por arriba de la madera
		// para no romper el dibujo de la pasada anterior
		IF n_pasadas>0	
			G01 Y2 F{vel_xy}
		ENDIF
		
	END WHILE
ENDIF

// restauro el offset X, Y y Z
G52 X{-pos_pinza+desf_x0} Y0 Z{-maq_dz}



