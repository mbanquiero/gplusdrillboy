
' Chapita del marco


	IF mano_pta==0
		G59P11
	ELSE
		G59P10
	ENDIF

	G52 X{pos_picaporte+mdp+mdm} Y{-36*K} Z{l_fresa_p}
	G01 Z20 F{vel_z}

	
	IF K==-1
		G01 X{mda/2-r_fresa_p} Y0 F{vel_xy}
		ACTIVAR FRESA
		M04
		'comienza el mecanizado
		G01 Z{-mdi} F{vel_pen}
		G01 X{-mda/2+r_fresa_p} Y0 F{vel_mec}
	ELSE
		G01 X{-mda/2+r_fresa_p} Y0 F{vel_xy}
		ACTIVAR FRESA
		M04
		'comienza el mecanizado
		G01 Z{-mdi} F{vel_pen}
	ENDIF


	G01 Y{-(mdb+r_fresa_p)*K} F{vel_mec}
	G01 Y0 

	// el usuario ingreso mdk = dist al comienzo del cuello
	// pero yo quiero la distancia al centro del cuello => hago las correciones
	IF mdk==0
		// simetrica
		mdk:=mdc/2
	ELSE
		// transformo para que mdk = distancia al eje del cuello
		mdk:=mdk+mda/2
	ENDIF


	'cuello	
	G01 X{mda/2-r_fresa_p} 
	G01 Y{-(2*r_fresa_p-1)*K}
	G01 X{-mda/2+r_fresa_p}
	G01 Y{-(mdb+r_fresa_p)*K} 
	G01 X{-(mdk-mdd/2)}
	IF K==1
		G03 X{-(mdk-mdd/2)} Y{-(mdb+mdd-r_fresa_p)*K} I0 J{-(mdd-2*r_fresa_p)/2} F{vel_curvas}
	ELSE
		G02 X{-(mdk-mdd/2)} Y{-(mdb+mdd-r_fresa_p)*K} I0 J{(mdd-2*r_fresa_p)/2} F{vel_curvas}
	ENDIF
	G01 X{(mdc-mdk-mdd/2)} F{vel_mec}
	IF K==1
		G03 X{(mdc-mdk-mdd/2)} Y{-(mdb+r_fresa_p)*K} I0 J{(mdd-2*r_fresa_p)/2} F{vel_curvas}
	ELSE
		G02 X{(mdc-mdk-mdd/2)} Y{-(mdb+r_fresa_p)*K} I0 J{-(mdd-2*r_fresa_p)/2} F{vel_curvas}
	ENDIF
	G01 X0 F{vel_mec}

	' 1er vaciado 
	IF mde<=2*r_fresa_p
		' vaciado simple
		PARAR FRESA
		G01 X{-mdf/2} Y{-(mdb+mdd-mdg-r_fresa_p)*K} F{vel_xy}
		ACTIVAR FRESA
		pen_z:=mdi
		WHILE pen_z<mdh
			pen_z:=pen_z+feed
			IF pen_z>mdh
				pen_z:= mdh
			ENDIF

			G01 Z{-pen_z} F{vel_pen}
			G01 X{mdf/2} F{vel_mec}

			IF pen_z<mdh

				pen_z:=pen_z+feed
				IF pen_z>mdh
					pen_z:= mdh
				ENDIF
				G01 Z{-pen_z} F{vel_pen}
				G01 X{-mdf/2} F{vel_mec}
			ENDIF
		END WHILE

	ELSE
		' Cajeado rectangular {maximo 28mm}
		PARAR FRESA
		G01 X{-mdf/2} Y{-(mdb+mdd-mdg-r_fresa_p)*K} F{vel_xy}
		ACTIVAR FRESA
		pen_z:=mdi
		WHILE pen_z<mdh
			pen_z:=pen_z+feed
			IF pen_z>mdh
				pen_z:= mdh
			ENDIF

			G01 Z{-pen_z} F{vel_pen}
			G01 X{mdf/2} F{vel_mec}
			G01 Y{-(mdb+mdd-mdg-mde+r_fresa_p)*K} 
			G01 X{-mdf/2}
			G01 Y{-(mdb+mdd-mdg-r_fresa_p)*K} 

		END WHILE

	ENDIF

	PARAR FRESA
	G01 Z20 F{vel_z}


	IF mdj!=0
		' Segundo vaciado
		IF mde2<=2*r_fresa_p
			// vaciado simple
			G01 X{mdf/2+mdj+r_fresa_p} Y{-(mdb+mdd-mdg2-r_fresa_p)*K} F{vel_xy}
			ACTIVAR FRESA
			pen_z:=mdi
			WHILE pen_z<mdh2
				pen_z:=pen_z+feed
				IF pen_z>mdh2
					pen_z:= mdh2
				ENDIF

				G01 Z{-pen_z} F{vel_pen}
				G01 X{mdf/2+mdj+mdf2-r_fresa_p} F{vel_mec}

				IF pen_z<mdh2

					pen_z:=pen_z+feed
					IF pen_z>mdh2
						pen_z:= mdh2
					ENDIF
					G01 Z{-pen_z} F{vel_pen}
					G01 X{mdf/2+mdj+r_fresa_p} F{vel_mec}
				ENDIF
			END WHILE

		ELSE
			' Cajeado rectangular {maximo 28mm}
			G01 X{mdf/2+mdj+r_fresa_p} Y{-(mdb+mdd-mdg2-r_fresa_p)*K} F{vel_xy}
			ACTIVAR FRESA
			pen_z:=mdi
			WHILE pen_z<mdh2
				pen_z:=pen_z+feed
				IF pen_z>mdh2
					pen_z:= mdh2
				ENDIF

				G01 Z{-pen_z} F{vel_pen}
				G01 X{mdf/2+mdj+mdf2-r_fresa_p} F{vel_mec}
				G01 Y{-(mdb+mdd-mdg2-mde2+r_fresa_p)*K} 
				G01 X{mdf/2+mdj+r_fresa_p}
				G01 Y{-(mdb+mdd-mdg2-r_fresa_p)*K} 

			END WHILE

		ENDIF
		PARAR FRESA
		G01 Z20 F{vel_z}


	ENDIF
	
	'fin de chapita de marco
	M05
	PARAR FRESA

