(MARCO)
M117
G49 G40 G80 G50 G61 G90
G54
G92.1
G21 
S1000
G17
G01 X699
(ESPEJAR_X = 439)
M20
G4 P0.50      
G01 Z0 F25000
(G52 X-133 Y0 Z-134)
G52  X572 Y0 Z-134
(POS_PINZA = 261)
G01 Y60 F20000
(G52 X-133 Y0 Z-149)
G52  X572 Y0 Z-149
(PERNIO : 1 de 2)
(posiciono la pinza: pos_pinza = 261 ,  pos_deseada = 168.01     )
G01 Y60 F20000
(POS_PINZA = 168.01    )
G54
G92.1
(G52 X0)
G52  X439
(G01 X606.01     F10000)
G01  X-606.01 F10000
G52 Z-17
G01 Z-8 F25000
M21	
G4 P0.50      
(G01 X699 F20000)
G01  X-699 F20000
M20
G4 P0.50      
G01 Z5 F25000
(G52 X-40.01     Y0 Z-134)
G52  X479.01 Y0 Z-134
(G52 X-40.01     Y0 Z-39.70    )
G52  X479.01 Y0 Z-39.7
(G01 X8.01       F10000)
G01  X-8.01 F10000
(pos pernio = 8.01       , pos_pinza =168.01    )
( bisagra normal	)
(mds = 4)	
(Comienzo del programa bisagra en el marco)
(G52 X-32 Y169 Z-44)
G52  X471 Y169 Z-44
(G01 X77 Y-19 F20000)
G01  X-77 Y-19 F20000
M502
G01 Z0.2 F25000
G01 Z-2.50      F1000
M103
G01 Y-30.50     F2500
(comienza el mecanizado)
(cuello)
G01 Y-19
(G01 X69)
G01  X-69
G01 Y-30.50     F1000
(cuerpo)
( HIBRIDA)
(G01 X99)
G01  X-99
G02  X-103 Y-26.5 I0 J-4 F500
G01 Y-26.50     F2500
G02  X-99 Y-22.5 I-4 J0 F500
(G01 X11 F2500)
G01  X-11 F2500
G02  X-7 Y-26.5 I0 J4 F500
(G01 X7 Y-26.50     F2500)
G01  X-7 Y-26.5 F2500
G02  X-11 Y-30.5 I4 J0 F500
(G01 X77 F2500)
G01  X-77 F2500
M503
M105
G01 Z5 F2500
(fin de la bisagra en el marco)
G52 Y0 
(PERNIO : 2 de 2)
(posiciono la pinza: pos_pinza = 168.01     ,  pos_deseada = 668 )
G01 Y60 F20000
(POS_PINZA = 668)
G54
G92.1
(G52 X0)
G52  X439
(Muevo porque no hay espacio suficiente para la siguiente pinza)
(pos_sig_pinza = 238.99    )
(G01 X238.99     F10000)
G01  X-238.99 F10000
G52 Z-17
G01 Z-8 F25000
M21	
G4 P0.50      
(G01 X-261 F20000)
G01  X261 F20000
M20
G4 P0.50      
G01 Z5 F25000
(G52 X-540 Y0 Z-134)
G52  X979 Y0 Z-134
(G52 X-540 Y0 Z-39.70    )
G52  X979 Y0 Z-39.7
(G01 X508 F10000)
G01  X-508 F10000
(pos pernio = 508 , pos_pinza =668)
( bisagra normal	)
(mds = 4)	
(Comienzo del programa bisagra en el marco)
(G52 X-32 Y169 Z-44)
G52  X471 Y169 Z-44
(G01 X77 Y-19 F20000)
G01  X-77 Y-19 F20000
M502
G01 Z0.2 F25000
G01 Z-2.50      F1000
M103
G01 Y-30.50     F2500
(comienza el mecanizado)
(cuello)
G01 Y-19
(G01 X69)
G01  X-69
G01 Y-30.50     F1000
(cuerpo)
( HIBRIDA)
(G01 X99)
G01  X-99
G02  X-103 Y-26.5 I0 J-4 F500
G01 Y-26.50     F2500
G02  X-99 Y-22.5 I-4 J0 F500
(G01 X11 F2500)
G01  X-11 F2500
G02  X-7 Y-26.5 I0 J4 F500
(G01 X7 Y-26.50     F2500)
G01  X-7 Y-26.5 F2500
G02  X-11 Y-30.5 I4 J0 F500
(G01 X77 F2500)
G01  X-77 F2500
M503
M105
G01 Z5 F2500
(fin de la bisagra en el marco)
G52 Y0 
G01 Z32 F25000
M189
G01 Y60 F20000
G54
G92.1
(G52 X0)
G52  X439
G01 Z0 F25000
M05
(la pieza sale por donde entro)
(posiciono la pinza: pos_pinza = 668 ,  pos_deseada = 261 )
G01 Y60 F20000
(POS_PINZA = 261)
G54
G92.1
(G52 X0)
G52  X439
G52 Z-17
G01 Z-8 F25000
M21	
G4 P0.50      
(G01 X452 F20000)
G01  X-452 F20000
M20
G4 P0.50      
G01 Z5 F25000
(G52 X-133 Y0 Z-134)
G52  X572 Y0 Z-134
(G52 X-133 Y0 Z-39.70    )
G52  X572 Y0 Z-39.7
G54
G92.1
(G52 X0)
G52  X439
(G01 X-237.30    F10000)
G01  X237.3 F10000
G01 Z0 F25000
(G01 X-261 F10000)
G01  X261 F10000
M9
M21
M05
G54
G52 Z0
G01 Z0 F25000
(G52 X0 Y0)
G52  X439 Y0
G01 Y120 F20000
(G01 X-261 f25000)
G01  X261
G28.1 z0 F25000
G01 A3 F25000
G28.1 A3
G1 Y120 f30000
G1 Z-132 f15000
