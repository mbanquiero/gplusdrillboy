// posicion, ancho, espesor, prof
PARAMETROS pos_x,ancho,esp,prof

// Cajeado sobre el canto INFERIOR 
PARAR FRESA

G52 X{-pos_pinza + desf_x0 + pos_x} Y{-dfresa_cajeado} Z{-maq_abajo_dz+esp_pta+dcentro}
G01 X{r_fresa_i} F{vel_xy}
G01 Y-2 F{vel_xy}
M03
IF esp<=2*r_fresa_i
	// Hago una sola pasada
	// profundiza tanto al ir como al volver
	G01 Z{-esp_pta/2} F{vel_z}
	ACTIVAR FRESA
	G01 Y0 F{vel_mec}
	flag:=0
	pen_y:=if(ranura, pb_pr , 0)

	WHILE pen_y < prof
		// ida
		pen_y:=pen_y+cfeed
		IF pen_y>prof
			pen_y:= prof
		ENDIF

		IF flag==0
			M03
			flag:=1
		ENDIF

		IF zigzag
			G01 X{ancho-r_fresa_i} Y{pen_y} F{vel_cajeado}
		ELSE
			G01 Y{pen_y} F{vel_pen}
			G01 X{ancho-r_fresa_i} F{vel_cajeado}
		ENDIF

		// Vuelta
		IF pen_y<prof
			pen_y:=pen_y+cfeed
			IF pen_y>prof
				pen_y:= prof
			ENDIF
			IF zigzag
				G01 X{r_fresa_i} Y{pen_y} F{vel_cajeado}
			ELSE
				G01 Y{pen_y} F{vel_pen}
				G01 X{r_fresa_i} F{vel_cajeado}
			ENDIF
		ENDIF
	END WHILE
ELSE
	// hay que hacer rectangulos
	pen_y:=if(ranura, pb_pr , 0)
	flag:=0
	WHILE pen_y < prof
		pen_y:=pen_y+cfeed
		IF pen_y>prof
			pen_y:= prof
		ENDIF

		G01 X{r_fresa_i} F{vel_mec}
		G01 Z{-((esp_pta-esp)/2 + r_fresa_i)}
		ACTIVAR FRESA
		IF flag==0
			M03
			flag:=1
		ENDIF
		IF zigzag
			G01 X{ancho-r_fresa_i} Y{pen_y} F{vel_cajeado}
			G01 Z{-((esp_pta+esp)/2 - r_fresa_i)} 
			pen_y:=pen_y+cfeed
			IF pen_y>prof
				pen_y:= prof
			ENDIF
			G01 X{r_fresa_i} Y{pen_y} 
	
		ELSE
			G01 Y{pen_y} F{vel_pen}
			G01 X{ancho-r_fresa_i} F{vel_cajeado}
			G01 Z{-((esp_pta+esp)/2 - r_fresa_i)} 
			G01 X{r_fresa_i}
		ENDIF
	END WHILE

	IF zigzag
		G01 Z{-((esp_pta-esp)/2 + r_fresa_i)} 
		G01 X{ancho-r_fresa_i} F{vel_cajeado}
		G01 Z{-((esp_pta+esp)/2 - r_fresa_i)} 
		G01 X{r_fresa_i} 
	ENDIF
ENDIF
PARAR FRESA
// bajo Y 
G01 Y-3 F{vel_xy}
