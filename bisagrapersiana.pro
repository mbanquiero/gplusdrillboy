PARAMETROS pos_xp,primera

// hago la ranura en el canto superior

// el z esta en la parte de atras de la maquina
G52 X{pos_xp} Y0 Z{-maq_dz}

IF primera
	G01 Y{ancho_per+maq_dfresa+dsys} F{vel_xy}
ENDIF
G01 X{esp/2} 
G01 Z5 F{vel_z}

// LO MANDO A PALPAR
'Palpa
IF primera
	G28.1 Y{ancho_per+maq_dfresa+dsys}
ELSE
	G28.1 Y{dsys}
ENDIF

G01 Y0 F{vel_xy}
G01 Z{esp_per-largo2+r_fresa_s} F{vel_z}
G01 X{r_fresa_s} F{vel_xy}

pos_y0:=-dpalpador+l_fresa_s


//activo la fresa
ACTIVAR FRESA
M03

'Comienza el mecanizado en XZ
G01 Y{pos_y0} F{vel_mec}
G01 Y{-prof+pos_y0} F{vel_pen}
// Contorno
G01 Z{esp_per} F{vel_z}
G01 X{esp-r_fresa_s} F{vel_mec}
G01 Z{esp_per-largo2+r_fresa_s} F{vel_z}
G01 X{r_fresa_s} F{vel_mec}

// interior (REVISAR)
IF esp>=4*r_fresa_s
	G01 X{3*r_fresa_s-1}
	G01 Z{esp_per} F{vel_z}
ENDIF
IF esp>=6*r_fresa_s
	G01 X{5*r_fresa_s-2} F{vel_mec}
	G01 Z{esp_per-largo2+r_fresa_s} F{vel_z}
ENDIF

'Fin ranura en XZ
PARAR FRESA
G01 Y{dsys} F{vel_xy}
M05

