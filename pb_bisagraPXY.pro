// Bisagra puerta blindada, para el marco
PARAMETROS pos_x,K,npernio



// version maquina nueva
// Revisar, que era esta variable, no la encontre en ningun lado!
comp_y:=0

'Comienzo del programa bisagra en el marco

// pb_mds = generalemente 1 o 2 mm
pb_dl:=pb_calle
dy:=esp_pta + pb_mds
solape:=1

IF pb_pa==0
	pb_pa:=12	
ENDIF

//pb_mdm -> Desfasaje del marco en relacion a la puerta
// Pero pb_marco le pasa la medida ya desfasada (pos_x ya tiene el desfasaje)

aux_pos_x:=pos_x

//en la fv1 vertical, la puerta de izquierda se mecaniza igual que la derecha pero espejando el Y
//por ese motivo en la puerta izquierda hago un offset al espesor de la puerta y cambio el signo de lo movimientos en Y
IF K == 1
	//Puerta derecha, el Y va normal
	pos_y0 := 0
ELSE
	//Puerta izquierda, el Y va espejado
	pos_y0:= esp_pta
ENDIF

//en la FV1 el z0 definido en la prifundidad de la maquina
IF retropalpador .OR. palpador_m == 0
	//vesion vieja fv1 Retropalpa Interesa el primer offset para poder apollarse en la pieza
	pos_z0:= -maq_dz + l_fresa_p
ELSE
	//Modelo Nuevo Como ahora palpa hacia adelante no importa en que posion esta el offset 
	//total avanza hasta tocar la superficie de la placa
	pos_z0 := 0
ENDIF



'Comienzo del programa bisagra en el marco
// FV-1
IF npernio == 0
	G52 X{aux_pos_x} Y{pos_y0} Z{pos_z0}
ELSE
	G52 X{aux_pos_x} Y{pos_y0}
ENDIF
IF palpador_m==1
	// LO MANDO A PALPAR
	PROGRAMA palpam CON 0,0
ENDIF


//esta es una variable auxiliar hasta averiguar si es correcto que a bisagra vaya desde la calle hasta
//el ras del marco y no tomando el pb_db.
aux_db:=dy-pb_dl


//los movimientos se separan porque el x es mucho mas rapido que el Y
IF K == 1
	//bisgra derecha
	G01 X{pb_da-r_fresa_p} F{vel_xy}
ELSE
	//bisagra izquierda entra por el extremo opuesto
	G01 X{r_fresa_p} F{vel_xy}
ENDIF
	
G01 Y0 F{vel_y}


IF npernio == 0 .AND. palpador_m==0
	M03
ENDIF

G01 Z0.2 F{vel_z}
G01 Z{-pb_prof} F{vel_pen}
ACTIVAR FRESA

'comienza el mecanizado
IF K == 1
	//bisagra derecha
	G01 X{r_fresa_p} F{vel_mec}
	IF pb_rc > r_fresa_p
		//mecanizo con curvas
		G01 Y{K*(pb_db-pb_rc)}
		G02 X{pb_rc} Y{pb_db-r_fresa_p} I{pb_rc-r_fresa_p} J0 F{vel_curvas}		
		G01 X{pb_da-pb_rc} F{vel_mec}
		G02 X{pb_da-r_fresa_p} Y{pb_db-pb_rc} I0 J{r_fresa_p-pb_rc} F{vel_curvas}		
	ELSE
		//mecanizo con lineas rectas
		G01 Y{K*(pb_db-r_fresa_p)}
		G01 X{pb_da-r_fresa_p}
	ENDIF
	G01 Y0 F{vel_mec}
	
	lado:=1
ELSE
	//bisagra izquierda se hace en sentido contrario a la derecha
	G01 X{pb_da-r_fresa_p} F{vel_mec}
	IF pb_rc > r_fresa_p
		//mecanizo con curvas
		G01 Y{K*(pb_db-pb_rc)}
		G02 X{pb_da - pb_rc} Y{K*(pb_db-r_fresa_p)} I{-(pb_rc-r_fresa_p)} J0 F{vel_curvas}
		G01 X{pb_rc} F{vel_mec}
		G02 X{r_fresa_p} Y{K*(pb_db-pb_rc)} I0 J{K*(r_fresa_p-pb_rc)} F{vel_curvas}
	ELSE
		//mecanizo con lineas rectas
		G01 Y{K*(pb_db-r_fresa_p)}
		G01 X{r_fresa_p}
	ENDIF
	G01 Y0 F{vel_mec}
	
	lado:=0
ENDIF


pos_y := 0
WHILE pos_y < pb_db - 3*r_fresa_p + solape

	pos_y := pos_y + 2*r_fresa_p - solape
	IF pos_y > pb_db - 3*r_fresa_p + solape
		pos_y := pb_db - 3*r_fresa_p + solape
	ENDIF
	// correccion para que al comer el interior no rompa la curvatura rc
	// aproximo la curvatura por una linea		
	dx:=0
	IF pos_y > pb_db - pb_rc
		
		dx:=pos_y-(pb_db-pb_rc)
		IF lado==1
			G01 X{pb_da-2*(r_fresa_p-solape)-dx} F{vel_mec}			
		ELSE
			G01 X{2*r_fresa_p+dx} F{vel_mec}			
		ENDIF
		
	ENDIF

	G01 Y{K*(pos_y)} F{vel_xy}		
	IF lado==1
		//derecha			
		G01 X{2*r_fresa_p+dx} F{vel_mec}
		lado:=0
	ELSE
		//izquierda
		G01 X{pb_da-2*(r_fresa_p-solape)-dx} F{vel_mec}					
		lado:=1
	ENDIF

	
END WHILE



PARAR FRESA

// Agujero
IF pb_ra>0		
	'agujero
	
	IF K == 1
		pos_a := pb_da - pb_ax-pb_ra+r_fresa_p
	ELSE
		//el agujero de la bisagra va en el extremo opuesto de la misma cuando es izquierda
		pos_a := pb_ax-pb_ra+r_fresa_p
	ENDIF

	G01 Y{K*(pb_az)}
	G01 X{pos_a}
	ACTIVAR FRESA	

	IF pb_ra<=r_fresa_p
		G01 Z{-pb_prof-pb_pa} F{vel_pen}
	ELSE
		
		//Cantidad de pasadas para hacer el agujero
		cant_pasadas:= 3
		j:=0
		WHILE j<cant_pasadas
			G01 Z{-pb_prof-(j+1)*pb_pa/cant_pasadas} F{vel_pen}
			IF K == 1
				G02 X{pos_a} Y{pb_az} I{pb_ra-r_fresa_p} J0 F{vel_curvas}
			ELSE
				G03 X{pos_a} Y{K*(pb_az)} I{pb_ra-r_fresa_p} J0 F{vel_curvas}
			ENDIF
			//G01 Z{-pb_prof-pb_pa} F{vel_pen}
			//IF K == 1
			//	G02 X{pos_a} Y{pb_az} I{pb_ra-r_fresa_p} J0 F{vel_curvas}
			//ELSE
			//	G03 X{pos_a} Y{K*(pb_az)} I{pb_ra-r_fresa_p} J0 F{vel_curvas}
			//ENDIF
			j := j + 1
		END WHILE
	ENDIF
	
ENDIF
PARAR FRESA
'finalizo la bisagra en el marco
G01 Z{dsz+pb_rebaje_canto} F{vel_mec}

IF palpador_m==1
	//  apaga la fresa para poder palpar y que se oculte el palpador
	M05
ENDIF




