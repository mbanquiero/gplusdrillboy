#var op mano_pta := 0	{Derecha,Izquierda}		// Mano de la Puerta

#var mm pb_pos_c1:= 200		// Posicion 1er caja:
#var mm pb_d1:= 120		// Ancho Caja 1:
#var mm pb_pos_c2:= 900		// Posicion 2da caja:
#var mm pb_d2:= 200		// Ancho Caja 2:
#var mm pb_pos_c3:= 1700		// Posicion 3da caja:
#var mm ancho_pta:= 825			// Ancho Puerta :
#var mm alto_pta:= 2030		// Altura Puerta:
#var mm pb_ar:= 20		// Ancho Ranura:
#var mm pb_pr:= 10		// Prof de la Ranura:

#var mm esp_pta:= 40		// Espesor de la Puerta :
#var mm pb_esp_caja:= 16		// Espesor de la Caja:
#var mm pb_prof_caja1:= 80		// Profunidad Caja1:
#var mm pb_prof_caja2:= 100	// Profunidad Caja2:
#var mm pb_pos_lx:= 150	// Posicion de la llave
#var mm pb_pos_ly:= 50	// Posicion de la llave
#var mm pb_lr:= 20		// Radio llave

#var mm pb_alto_mirilla:= 1600 // Altura de la mirilla
#var mm pb_radio_mirilla:= 0	// Radio de la mirilla (Ojo de Buey)

#var mm maq_dz:= 122			// prof maquina:
#var mm feed:= 4			// Profundidad x pasada:

IF modelo == 3
	//Maquina fv1
	PROGRAMA pb_cerraduraXY	
ELSE
	rot_0:=3
	//dcentro:=0
	dsz2:=2
	
	G49 G40 G80 G50 G61 G90
	G54
	G92.1
	G21 (mm)
	S1000
	
	
	PLANO XY
	
	G18
	
	IF mano_pta==1
		K:=-1
	ELSE
		K:=1
	ENDIF
	
	// maq_dx2 = desfasaje en el eje x en parte de abajo (usualmente se usa en la pta izquierda)
	// en la pta derecha = 0, se aplica a las posiciondes de los cajeados 
	// no se aplica a la mirilla etc. (Solo se aplica al canto inferior)
	
	G52 Z0
	G01 Z0 F{vel_z}
	IF modelo==2
		//paso al canto inferior	
		'canto inferior	
		G01 A180 F{vel_rot}
	ENDIF
	
	//da la posicion del tope en x que se va a usar
	pos_x0:=0
	IF modelo==2
		//si es mayor a 2030 se usan otros topes
		IF alto_pta>2040
			IF K==1
				pos_x0:=-pta_tope1
			ELSE
				pos_x0:=pta_tope3
			ENDIF
		ELSE
			IF K!=1			
				pos_x0:=pta_tope2
			ENDIF	
		ENDIF
	ELSE
	//si es otro modelo solo hay dos topes el 0 y el alto de la puerta
		IF K!=1
			pos_x0:=alto_pta
		ENDIF
	ENDIF
	
	
	IF K==-1	
		G52 X{pos_x0} Y{dfresa_cajeado} Z{-maq_abajo_dz+esp_pta+dcentro}
	ELSE	
		G52 X{pos_x0} Y{dfresa_cajeado} Z{-maq_abajo_dz+esp_pta+dcentro}
	ENDIF
	
	pos_ini_cajas := 0
	IF ranura
		// RANURA
		// ojo que profundiza con rectangulos
		// en cada pasada (ida y vuelta) entonces profundiza 5mm o menos
	
		hacer_extremo := 0
		//si pb_rd=pb_ri=0 la ranura ocupa toda la puerta
		IF pb_rd == 0	
			pb_rd := -10
			pos_ini_cajas := 0
		ELSE
			pos_ini_cajas:= -pb_rd
			pb_rd := pb_rd + r_fresa_i
		ENDIF
	
		IF pb_ri == 0	
			pb_ri := 10
			//pos_ini_cajas := 0
			hacer_extremo := 1
		ELSE
			//pos_ini_cajas:= pb_ri
			pb_ri := pb_ri + r_fresa_i
		ENDIF
	
		'Ranura, profundiza haciendo rectangulos
		G01 X{K*(alto_pta-pb_rd)} Y-5 F{vel_xy}
		pen_y:=0
	
		M03
		ACTIVAR FRESA
	
		WHILE pen_y < pb_pr
			pen_y:=pen_y+feed
			IF pen_y>pb_pr
				pen_y:= pb_pr
			ENDIF
			
			G01 X{K*(alto_pta-pb_rd)} F{vel_xy}
			G01 Z{-((esp_pta-pb_ar)/2 + r_fresa_i )} F1000
			
			G01 Y{pen_y} F{vel_pen}
			G01 X{K*pb_ri} F{vel_cajeado}
			G01 Z{-((esp_pta+pb_ar)/2 - r_fresa_i )} F1000
			G01 X{K*(alto_pta-pb_rd)} F{vel_cajeado}
	
		END WHILE
		
		
		IF hacer_extremo
			' extremo que quedo sin cajear
			G01 Y-5 F{vel_xy}
			G01 X{-K*12}
			pen_y:=0
			WHILE pen_y < pb_pr
				pen_y:=pen_y+feed
				IF pen_y>pb_pr
					pen_y:= pb_pr
				ENDIF
	
				G01 X{-K*12} F{vel_cajeado}
				G01 Z{-((esp_pta-pb_ar)/2 + r_fresa_i )} F1000
	
				G01 Y{pen_y} F{vel_pen}
				G01 X{K*12} F{vel_cajeado}
				G01 Z{-((esp_pta+pb_ar)/2 - r_fresa_i )} F1000
				G01 X{-K*12} F{vel_cajeado}
	
			END WHILE
		ENDIF
		PARAR FRESA
	ENDIF
	
	// calado de las cajas OJO QUE cajeado NO APAGA LA FRESA 
	// Usualmente tiene 3 cajas o 5, la del medio es la caja grande, y el resto son chicas
	//desfasaje por los topes de la maquina
	IF K==1
		desf_x:=pos_x0
	ELSE
		desf_x:=alto_pta-pos_x0
	ENDIF
	
	
	IF ranura==0 .AND. cant_cajeados>0 
		// Si no esta la ranura,me tengo que posicionar desde un lugar seguro
		IF K==1
			G52 X0 Y{dfresa_cajeado} Z{-maq_abajo_dz+esp_pta+dcentro}
		ELSE
			G52 X{alto_pta} Y{dfresa_cajeado} Z{-maq_abajo_dz+esp_pta+dcentro}
		ENDIF
		// me retiro a una pos. segura
		IF modelo<2
			G01 Y-9 Z60 F{vel_z}
		ELSE
			G01 Y{-dsyi} F{vel_xy}
		ENDIF
	ENDIF
	
	// Hay 3 vaciados o 5, el del medio es grande y el resto peque�os
	t:=0
	cg := if(cant_cajeados==3,1,2)
	WHILE t < cant_cajeados
		pb_d:= if(t==cg,pb_d2,pb_d1)
		pb_prof_caja:= if(t==cg,pb_prof_caja2,pb_prof_caja1)
		'cajas
		//le sumo la posicion izquierda de la ranura (pb_ri) porque la cajas estan unidas al frente de la ranura
		PROGRAMA cajeado CON pos_ini_cajas+pb_pos_c[t]+maq_dx2+desf_x,pb_d,pb_esp_caja,pb_prof_caja,K
		
		//pb_d3:= pb_d2 - 100
		pb_prof2_caja2 := pb_prof_caja3 + pb_prof_caja2
		IF t == cg .AND. pb_prof2_caja2 > pb_prof_caja2
			//segundo cajeado para la caja central
			diffx:= (pb_d - pb_d3)/2
			//hack activo temporalmente la ranura y le doy la profundidad de la primer caja para que cajeado.pro la saltee
			aux_ranura := ranura
			aux_pb_pr := pb_pr
			ranura := 1
			pb_pr := pb_prof_caja
			
			PROGRAMA cajeado CON pos_ini_cajas+pb_pos_c[t]+maq_dx2+desf_x+diffx,pb_d3,pb_esp_caja,pb_prof2_caja2,K
			
			ranura:= aux_ranura
			pb_pr := aux_pb_pr 
		ENDIF
		t := t + 1
	END WHILE
	'fin de los cajeados
	G01 Y{-dsyi}	F{vel_mec}
	
	G54
	G92.1
	G01 Z0 F{vel_z}
	
	
	M05
	PARAR FRESA
	
		
	// Aca ya trabajo con otro conjunto, y en otro plano
	IF modelo==2
		//paso al plano XY
		G01 A90 F{vel_rot}
	ENDIF
	
	G59P12
	G17
	
	// ojo: la fresa aca es de 14
	IF mano_pta==1
		G52 X{pos_x0} Y0 Z{-maq_abajo_dz+esp_pta+dcentro}
	ELSE
		G52 X{pos_x0} Y0 Z{-maq_abajo_dz+esp_pta+dcentro}
	ENDIF
	
	
	IF pestillo
		// Agujero grande
		'pestillo agujero grande
		IF modelo==2
			G4 P2
			M03
		ENDIF	
		G01 X{K*(pb_pos_lx-pb_lr+r_fresa_p)} Y{pb_pos_ly} F{vel_xy}
		//los modelos anteriores a la fv5 tienen que prender frente a la posicion de trabajo porque cuando se activan sale el fresador hacia adelante
		IF modelo<2
			M04
		ENDIF
			
		pen_z:=0
		prof1:=14
		WHILE pen_z<prof1
			pen_z:=pen_z+feed
			IF pen_z>prof1
				pen_z:= prof1
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
			IF pb_lr>7
				G02 X{K*(pb_pos_lx-pb_lr+r_fresa_p)} Y{pb_pos_ly} I{K*(pb_lr-r_fresa_p)} J0 F{vel_curvas}
			ENDIF
		END WHILE
	
	
		' Agujero chico
		pen_z:=prof1
		G01 X{K*(pb_pos_lx-pb_lr2+r_fresa_p)} Y{pb_pos_ly} F{vel_xy}
		WHILE pen_z<esp_pta+2
			pen_z:=pen_z+feed
			IF pen_z>esp_pta+2
				pen_z:= esp_pta+2
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
			IF pb_lr2>r_fresa_p
				G02 X{K*(pb_pos_lx-pb_lr2+r_fresa_p)} Y{pb_pos_ly} I{K*(pb_lr2-r_fresa_p)} J0 F{vel_curvas}
			ENDIF
		END WHILE
		G01 Z{dsz2} F{vel_z}
		IF modelo<2
			M05
		ENDIF
	
	ENDIF
	
	// Agujero de la mirilla
	IF pb_radio_mirilla!=0
		'Mirilla
		IF pb_radio_mirilla>r_fresa_p
			'penetra en circulos		
			IF modelo==2
				M03
			ENDIF		
			G01 X{K*(alto_pta-pb_alto_mirilla)} Y{ancho_pta/2+pb_radio_mirilla-r_fresa_p} F{vel_xy}
			IF modelo<2
				M04
			ENDIF	
			
			ACTIVAR FRESA
			pen_z:=0
			WHILE pen_z<esp_pta+2
				pen_z:=pen_z+feed
				IF pen_z>esp_pta+2
					pen_z:= esp_pta+2
				ENDIF
				G01 Z{-pen_z} F{vel_pen}
				G02 X{K*(alto_pta-pb_alto_mirilla)} Y{ancho_pta/2+pb_radio_mirilla-r_fresa_p} I0 J{r_fresa_p-pb_radio_mirilla} F{vel_curvas}
			END WHILE
		ELSE
			' solo perfora
			
			IF modelo==2
				M03
			ENDIF
			
			G01 X{K*(alto_pta-pb_alto_mirilla)} Y{ancho_pta/2} F{vel_xy}
			IF modelo<2
				M04
			ENDIF	
			ACTIVAR FRESA
			
			pen_z:=0
			WHILE pen_z<esp_pta+2
				pen_z:=pen_z+feed
				IF pen_z>esp_pta+2
					pen_z:= esp_pta+2
				ENDIF
				G01 Z{-pen_z} F{vel_pen}
			END WHILE
		ENDIF
		G01 Z{dsz2} F{vel_z}
		IF modelo<2
			M05
		ENDIF
	ENDIF
	PARAR FRESA
	
	// Agujero del pomo
	IF pb_radio_pomo!=0
		'Agujero del pomo
		IF pb_radio_pomo>r_fresa_p
			'penetra en circulos
			IF modelo==2
				M03
			ENDIF
			
			G01 X{K*(alto_pta-pb_alto_pomo)} Y{ancho_pta/2+pb_radio_pomo-r_fresa_p} F{vel_xy}
			IF modelo<2
				M04
			ENDIF
			
			ACTIVAR FRESA
			pen_z:=-feed
			WHILE pen_z<esp_pta+2
				pen_z:=pen_z+feed
				IF pen_z>esp_pta+2
					pen_z:= esp_pta+2
				ENDIF
				G01 Z{-pen_z} F{vel_pen}
				G02 X{K*(alto_pta-pb_alto_pomo)} Y{ancho_pta/2+pb_radio_pomo-r_fresa_p} I0 J{r_fresa_p-pb_radio_pomo} F{vel_curvas}
			END WHILE
		ELSE
			' solo perfora
			IF modelo==2
				M03
			ENDIF
			
			G01 X{K*(alto_pta-pb_alto_pomo)} Y{ancho_pta/2} F{vel_xy}
			IF modelo<2
				M04
			ENDIF
	
			
			ACTIVAR FRESA
			pen_z:=0
			WHILE pen_z<esp_pta+2
				pen_z:=pen_z+feed
				IF pen_z>esp_pta+2
					pen_z:= esp_pta+2
				ENDIF
				G01 Z{-pen_z} F{vel_pen}
			END WHILE
		ENDIF
		G01 Z{dsz2} F{vel_z}
		IF modelo<2
			M05
		ENDIF
	ENDIF
	PARAR FRESA
	
	'Finalizo el programa de la cerradura
	
	IF modelo<2
		M05
	ENDIF
	G54
	G52 Z0
	G01 Z0 F{vel_z}
	IF modelo=2	
		//por tema mec�nico conviene que quede un grado que el lambor
		G52 Y0
		G01 Y0 F{vel_xy}
		M05	
		G01 A{ang_lambor+1} F{vel_rot}
	ENDIF
	G52 X0 Y0
	G01 X{-dist_ax} Y0  F{vel_xy}
	G28.1 X{-dist_ax} Y0 Z0
	
	//fin maquina
ENDIF

  