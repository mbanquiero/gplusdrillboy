PARAMETROS pos_x,ancho,esp,prof
(Vaciado de marco)

dy:=esp_pta + pb_mds
G52 X{-pos_pinza+desf_x0+pos_x} Y{dy + fn_dfy} Z{-maq_dz+esp_marco+ fn_dfz+ l_fresa_p}

G01 X{r_fresa_p} F{vel_xy}
G01 Y{-((dy-esp)/2 - pb_vaciado_desf_y + r_fresa_p)} F{vel_xy}

M502
G04 p1
G01 Z1 F{vel_z}

pen_z:=0
ACTIVAR FRESA
WHILE pen_z < prof
	pen_z:=pen_z+feed
	IF pen_z>prof
		pen_z:= prof
	ENDIF

	G01 X{r_fresa_p} F{vel_mec}
	G01 Y{-((dy-esp)/2 + r_fresa_p)}
	
	G01 Z{-pen_z} F{vel_pen}
	G01 X{ancho-r_fresa_p} F{vel_mec}
	G01 Y{-((dy+esp)/2 - r_fresa_p)} 
	G01 X{r_fresa_p}

END WHILE
PARAR FRESA
M503
(retiro Z)
PROGRAMA moverAZSegura
'fin del Vaciado en el marco
