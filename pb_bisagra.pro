// Bisagra puerta blindada
PARAMETROS pos_x,prof_pernio,primer_pernio

//profundidad del agujero de la bisagra

solape:=0.5

IF pb_pa==0
	pb_pa:=12	
ENDIF

aux_vel_curvas:=vel_curvas

G52 X{-pos_pinza+desf_x0 + pos_x} Z{-maq_dz}

//pos_y0 es la pos de la sup de la madera desde la punta de la fresa
pos_y0:=ancho_pta+maq_dfresa

IF palpar
	//posiciono el palpador 
	// el cabezal esta ya en posicion X de la bisagra y arriba a una cierta distancia
	G01 X0 F{vel_xy}
	
	IF primer_pernio
		G01 Y{pos_y0 - 10} F{vel_xy}
		// posicion de palpado 17 + esp de la puerta
		G01 Z{pieza_dz+17} F{vel_z}
	ELSE
		G01 Z{pieza_dz+17} F{vel_z}
		G01 Y{pos_y0 - 10} F{vel_xy}
	ENDIF
		
	//instruccion de palpado
	palpa_pos_x := 0
	G31 Y2 F{vel_palpa}
	
	//Grabo la diferencia de palpado en la variable #2 del mach
	//La variable #5212 del mach tiene el offset y actual
	//Si hago un G92 Y0 hace que la variable #5212 tenga la posicion actual absoluta
	//el offset de Y es = 0
	G92 Y0
	#2 = [#5212 - {-dy_palpa}]
	G92.1
	(Fin Palpado Canto superior)
	
	(Correccion de offset por palpado)
	G52 X{-pos_pinza+desf_x0 + pos_x} Y[#2] Z{-maq_dz}

ENDIF

// Y0 = es al raz del canto superior

G01 Y10 F{vel_xy}
G01 X{r_fresa_s} F{vel_xy}	
G01 Z0 F{vel_z}

M03

IF pb_doblep	
	n_pasadas:=2
ELSE	
	n_pasadas:=1
ENDIF

aux_y:=pos_y0
penetrar:=prof_pernio/n_pasadas

pb_ax_aux := pb_ax
IF prof_pernio == 0
	//si la profundidad del pernio es 0 se utiliza la bisagra blindada para hacer agujeros en el canto superior
	prof_pernio := 0
	n_pasadas := 0
	pb_ax_aux := 0
ENDIF


WHILE n_pasadas>0
	
	aux_y:=aux_y-penetrar	
	
	'entrada por la derecha ,normal
	
	IF mano_pta==1
		// si es izquierda tengo que invertir el movimiento, porque despues la va espejar
		G01 X{r_fresa_s} F{vel_xy}
	ELSE
		G01 X{pb_da-r_fresa_s} F{vel_xy}
	ENDIF
	G01 Y{aux_y-pos_y0} F{vel_pen}
	ACTIVAR FRESA
		
	'comienza el mecanizado
	'perimetro de la bisagra
	G01 Z0 F{vel_mec}
	IF mano_pta==1
		// si es izquierda tengo que invertir el movimiento, porque despues la va espejar
		G01 X{pb_da - r_fresa_s} F{vel_mec}
	ELSE
		G01 X{r_fresa_s} F{vel_mec}
	ENDIF
	
	IF pb_rc > r_fresa_s
		//mecanizo con curvas
		G01 Z{pb_db-pb_rc} F{vel_mec}
		IF mano_pta==1
			G02 X{pb_da-pb_rc} Z{pb_db-r_fresa_s} I{-(pb_rc-r_fresa_s)} K0 F{vel_curvas}
			G01 X{pb_rc} F{vel_mec}
			G02 X{r_fresa_s} Z{pb_db-pb_rc} I0 K{r_fresa_s-pb_rc} F{vel_curvas}
		ELSE
			G03 X{pb_rc} Z{pb_db-r_fresa_s} I{pb_rc-r_fresa_s} K0 F{vel_curvas}
			G01 X{pb_da-pb_rc} F{vel_mec}
			G03 X{pb_da-r_fresa_s} Z{pb_db-pb_rc} I0 K{r_fresa_s-pb_rc} F{vel_curvas}
		ENDIF
	ELSE
		//mecanizo con lineas rectas
		G01 Z{pb_db-r_fresa_s} F{vel_mec}
		IF mano_pta==1
			G01 X{r_fresa_s} F{vel_mec}
		ELSE
			G01 X{pb_da-r_fresa_s} F{vel_mec}
		ENDIF
	ENDIF
	G01 Z0 F{vel_mec}
	
	lado:=!mano_pta 
	
	'como el interior
	// pos_z representa la pos de la fresa para comer el interior
	// arriba ya comio un radio de fresa, por lo cual empiezo en la
	// posicion inicial 2 radios - solape
	// y como hasta llegar al limite de db - radio, ya que
	// por ahi ya paso la fresa cuando hizo el perimetro

	pos_z := 0
	WHILE pos_z < pb_db - 3*r_fresa_s + solape
	
		pos_z := pos_z + 2*r_fresa_s - solape
		IF pos_z > pb_db - 3*r_fresa_s + solape
			pos_z := pb_db - 3*r_fresa_s + solape
		ENDIF
		// correccion para que al comer el interior no rompa la curvatura rc
		// aproximo la curvatura por una linea		
		dx:=0
		IF pos_z>pb_db-pb_rc
			dx:=pb_rc-(pb_db-pos_z)
			IF lado==1
				G01 X{pb_da-2*(r_fresa_s-solape)-dx} F{vel_mec}				
			ELSE
				G01 X{2*r_fresa_s+dx} F{vel_mec}
			ENDIF
		ENDIF

		G01 Z{pos_z} F{vel_mec}		
		IF lado==1
			//derecha			
			G01 X{2*r_fresa_s+dx} F{vel_mec}
			lado:=0
		ELSE
			//izquierda			
			G01 X{pb_da-2*(r_fresa_s-solape)-dx} F{vel_mec}				
			lado:=1
		ENDIF
	END WHILE
	PARAR FRESA			
	G01 Z0 F{vel_mec}
	n_pasadas:=n_pasadas-1
END WHILE

//Agujero
IF pb_ra>0
	IF pb_ra < r_fresa_s
		pb_ra := r_fresa_s
	ENDIF
	
	//https://lepton.atlassian.net/browse/GPL-15
	IF mano_pta==1
		pb_ax_aux := pb_da-pb_ax
	ENDIF
	
	' Agujero
		
	pos_a:= pb_ax_aux-pb_ra+r_fresa_s
	G01 Z{pb_az} F{vel_mec}
	G01 X{pos_a} F{vel_mec}
	ACTIVAR FRESA	

	IF pb_ra>r_fresa_s
		// profundiza haciendo circulos
		aux_prof:=0
		WHILE aux_prof<pb_pa
			aux_prof:=aux_prof+feed
			IF aux_prof>pb_pa
				aux_prof:=pb_pa
			ENDIF
			G01 Y{aux_y-aux_prof-pos_y0} F{vel_pen}
			G03 X{pos_a} Z{pb_az} I{pb_ra-r_fresa_s} K0 F1000
		END WHILE
		
	ELSE
		' solo penetra
		G01 Y{aux_y-pb_pa-pos_y0} F{vel_pen}
	ENDIF
ENDIF

PARAR FRESA
'fin de la bisagra me retiro en Y


// restauro el offset X, Y y Z
G52 X{-pos_pinza+desf_x0} Y0 Z{-maq_dz}



