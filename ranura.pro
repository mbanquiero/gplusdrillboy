
// y0 -> es la parte de abajo de la pieza, y0+ancho_pieza parte de arriba
PARAMETROS pos_x,rdx,rdy,doble,sentido


	G52 X{pos_x} Y{desf_ranura}

	IF sentido==0 
		// la hago de abajo hacia arriba
		G01 X0 Y{(ancho_pieza-rdy)/2} F{vel_xy}
		ACTIVAR FRESA
		
		IF modelo == 3
			G01 Z{-prof} F{vel_z}
		ELSE
			G01 Z{-prof} F{vel_pen}
		ENDIF
		G01 X{rdx} Y{(ancho_pieza+rdy)/2} F{vel_mec}
		PARAR FRESA
		G01 Z{dsz} F{vel_z}


		IF doble
			G01 X{rdx} Y{(ancho_pieza-rdy)/2+ancho_pieza} F{vel_xy}
			ACTIVAR FRESA

			G01 Z{-prof} F{vel_pen}
			G01 X0 Y{(ancho_pieza+rdy)/2+ancho_pieza} F{vel_mec}
			PARAR FRESA
			G01 Z{dsz} F{vel_z}
		ENDIF


	ELSE
		// de arriba hacia abajo

		IF doble
			PARAR FRESA
			G01 X0 Y{(ancho_pieza+rdy)/2+ancho_pieza} F{vel_xy}
			ACTIVAR FRESA

			G01 Z{-prof} F{vel_pen}
			G01 X{rdx} Y{(ancho_pieza-rdy)/2+ancho_pieza} F{vel_mec}
			PARAR FRESA
			G01 Z{dsz} F{vel_z}
		ENDIF

		G01 X{rdx} Y{(ancho_pieza+rdy)/2} F{vel_xy}
		ACTIVAR FRESA

		G01 Z{-prof} F{vel_pen}
		G01 X0 Y{(ancho_pieza-rdy)/2} F{vel_mec}
		
		PARAR FRESA
		G01 Z{dsz} F{vel_z}

	ENDIF

	PARAR FRESA







