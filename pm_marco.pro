
G49 G40 G80 G50 G61 G90
G54
G92.1
G21 
S1000

G17

G59P10
PLANO XY

IF mano_pta==1
	K:=-1
ELSE
	K:=1
ENDIF
// -------------------------------------

comp_y:=0


IF pb_pos_x1!=0
	PROGRAMA pb_bisagraXY CON pb_pos_x1,K
ENDIF

IF pb_pos_x2!=0
	PROGRAMA pb_bisagraXY CON pb_pos_x2,K
ENDIF

IF pb_pos_x3!=0
	PROGRAMA pb_bisagraXY CON pb_pos_x3,K
ENDIF

IF pb_pos_x4!=0
	PROGRAMA pb_bisagraXY CON pb_pos_x4,K
ENDIF


// Chapita del marco
G59P10
G52 X{pos_picaporte+mdp+mdm} Y{-(36-comp_y)*K} Z0
G01 Z20 F1000

IF K==-1
	G01 X{mda/2-7} Y0 F20000
	M04
	G01 Z{-mdi} F1000
	G01 X{-mda/2+7} Y0 F20000
ELSE
	G01 X{-mda/2+7} Y0 F20000
	M04
	G01 Z{-mdi} F1000
ENDIF

G01 Y{-(mdb+7)*K} F3000
G01 Y0 


G01 X{mda/2-7} 
G01 Y{-(14-1)*K}
G01 X{-mda/2+7}
G01 Y{-(mdb+7)*K} F3000
G01 X{-(mda/2+mdi-mdd/2)}
IF K==1
	G03 X{-(mda/2+mdi-mdd/2)} Y{-(mdb+mdd-7)*K} I0 J{-(mdd-14)/2}
ELSE
	G02 X{-(mda/2+mdi-mdd/2)} Y{-(mdb+mdd-7)*K} I0 J{(mdd-14)/2}
ENDIF
G01 X{(mdc-(mdi+mda/2)-mdd/2)}
IF K==1
	G03 X{(mdc-(mdi+mda/2)-mdd/2)} Y{-(mdb+7)*K} I0 J{(mdd-14)/2}
ELSE
	G02 X{(mdc-(mdi+mda/2)-mdd/2)} Y{-(mdb+7)*K} I0 J{-(mdd-14)/2}
ENDIF
G01 X0 

// vaciado caja pestillo
PROGRAMA cajeadoXY CON pos_picaporte+mdp+mdm-mdf/2,-K*(36-comp_y),mdf,mde,m_prof_pestillo

// vaciado caja para los bulones
// que sera dp1???
dp1:=0		
PROGRAMA cajeadoXY CON pos_picaporte+mdp+mdm+dp1-7,-K*(36-comp_y),dp1+7,mde,m_prof_bulon


G01 Z20
M05

//
G54
G52 Z0
G01 Z0 F500
G52 X0 Y0
G0 X0 Y0  F20000
G28.1 X0 Y0 Z0
