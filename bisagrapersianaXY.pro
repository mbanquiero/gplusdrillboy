PARAMETROS pos_xp
//pos_xp es la posicion de la ranura con el desfasaje integrado

G52 X{pos_xp} Y0 Z{-maq_dz+esp_per+l_fresa_p}
// ubico la fresa en el borde izquierdo inferior de la ranura
G01 X{r_fresa_p} Y{ancho_per-largo}  F{vel_xy}

// activo la fresa
ACTIVAR FRESA
IF modelo <2
	M04
ENDIF

// ranura en el plano xy
G01 Z{-prof} F{vel_pen}

' contorno de la ranura
G01 Y{ancho_per} F{vel_mec}
G01 X{esp-r_fresa_p} 
G01 Y{ancho_per-largo} 
G01 X{r_fresa_p}

// como el interior (REVISAR)
IF esp>=4*r_fresa_p
	G01 X{3*r_fresa_p-1}
	G01 Y{ancho_per-r_fresa_p}
ENDIF
IF esp>=6*r_fresa_p
	G01 X{5*r_fresa_p-2}
	G01 Y{ancho_per-largo+r_fresa_p}
ENDIF
'Fin ranura en XY

PARAR FRESA
G01 Z2 F{vel_z}
IF modelo < 2
	M05
ENDIF

