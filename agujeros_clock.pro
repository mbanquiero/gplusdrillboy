
// Variables especiales
#var sn sim_x:=1		// Simetrico en X=
#var mm base := 1000	// Base de la Pieza=
#var mm altura := 1000	// Altura de la Pieza=

#var mm pos_y1 := 0		// Posicion Y:
#var sn ac1 := 1		// Con Agujero Chico
#var op tipo1 := 0			{Izquierda,Derecha}	// Tipo =

#var mm pos_y2 := 0		// Posicion Y:
#var sn ac2 := 1		// Con Agujero Chico?:
#var op tipo2 := 0			{Izquierda,Derecha}	// Tipo =

#var mm pos_y3 := 0		// Posicion Y:
#var sn ac3 := 1		// Con Agujero Chico?:
#var op tipo3 := 0			{Izquierda,Derecha}	// Tipo =

#var mm pos_y4 := 0		// Posicion Y:
#var sn ac4 := 1		// Con Agujero Chico?:
#var op tipo4 := 0			{Izquierda,Derecha}	// Tipo =


G0 G49 G40 G80 G50 G54 G90 G92.1
G21 (mm)
S1000
G61
G17
G59P17
	
IF pos_y1!=0
	IF sim_x!=0
		PROGRAMA clock CON 9.5,pos_y1,ac1,0
		PROGRAMA clock CON base-9.5,pos_y1,ac1,1
	ELSE
		PROGRAMA clock CON 9.5,pos_y1,ac1,tipo1
	ENDIF
ENDIF

IF pos_y2!=0
	IF sim_x!=0
		PROGRAMA clock CON 9.5,pos_y2,ac2,0
		PROGRAMA clock CON base-9.5,pos_y2,ac2,1
	ELSE
		PROGRAMA clock CON 9.5,pos_y2,ac2,tipo2
	ENDIF
ENDIF

IF pos_y3!=0
	IF sim_x!=0
		PROGRAMA clock CON 9.5,pos_y3,ac3,0
		PROGRAMA clock CON base-9.5,pos_y3,ac3,1
	ELSE
		PROGRAMA clock CON 9.5,pos_y3,ac3,tipo3
	ENDIF
ENDIF

IF pos_y4!=0
	IF sim_x!=0
		PROGRAMA clock CON 9.5,pos_y4,ac4,0
		PROGRAMA clock CON base-9.5,pos_y4,ac4,1
	ELSE
		PROGRAMA clock CON 9.5,pos_y4,ac4,tipo4
	ENDIF
ENDIF
