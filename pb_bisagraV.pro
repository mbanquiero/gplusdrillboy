// Bisagra puerta blindada
PARAMETROS pos_x,K,primera,prof_pernio

solape:=2.5
//DSY:=30
G18


IF K==1
	// puerta derecha
	G52 X{pos_x} Y0 Z{-maq_dz}
ELSE
	// puerta izquierda
	G52 X{alto_pta-(pos_x+pb_da)} Y0 Z{-maq_dz}
ENDIF

IF palpador==0 .OR. primera==1
	G01 Y{ancho_pta+maq_dfresa+30} F{vel_xy}
ENDIF

// Si no hace cerradura lo mando a palpar a otro lado, ya que suepuestamente el pernio ya lo hizo
IF cerradura==0
	// hay 40mm entre el centro de la fresa y el palpador
	IF K==1
		G01 X{pb_da+r_fresa_s-20} F{vel_xy}
	ELSE
		G01 X{-r_fresa_s-60} F{vel_xy}
	ENDIF
ELSE
	G01 X0 F{vel_xy}
ENDIF


IF modelo==2
// como el modelo 2 copia lambor, tiene que palpar atras (z5)
	G01  Z5 F{vel_z}
ELSE
	G01  Z10 F{vel_z}
ENDIF

' LO MANDO A PALPAR
IF primera
	G28.1 Y{ancho_pta+maq_dfresa+30-1}
ELSE
	G28.1 Y{dsys}
ENDIF
G01 Y0 F{vel_xy}
M03
G01 X{pb_da-r_fresa_s} F{vel_xy}
G01 Z0 F{vel_z}

pos_y0:=-dpalpador

// activo el lambor
LAMBOR = pend_lambor

G01 Y{pos_y0} F{vel_mec}

IF pb_doblep
	aux_y:=pos_y0-prof_pernio/2
ELSE
	aux_y:=pos_y0-prof_pernio
ENDIF

//cambia la entrada sobre el cuello del pernio. Esto se hace por un problema de arranque de viruta
IF modelo=2
	//cambio la entrada del cuello y reposiciono para que continue normalmente
	'Entrada por la izquierda de la bisgra FV-5
	G01 X{r_fresa_s} F{vel_mec}
	G01 Y{aux_y} F{vel_pen}

	ACTIVAR FRESA

	G01 Z{r_fresa_s/2} F{vel_z}
	G01 X{pb_da-r_fresa_s} F{vel_mec}
ELSE
	'entrada por la derecha ,normal

	G01 Y{aux_y} F{vel_pen}
	ACTIVAR FRESA

ENDIF

'comienza el mecanizado
'perimetro de la bisagra
G01 Z0 F{vel_z}

G01 X{r_fresa_s} F{vel_mec}
G01 Z{pb_db-pb_rc} F{vel_z}
G03 X{pb_rc} Z{pb_db-r_fresa_s} I{pb_rc-r_fresa_s} K0 F{vel_curvas}
G01 X{pb_da-pb_rc} F{vel_mec}

G03 X{pb_da-r_fresa_s} Z{pb_db-pb_rc} I0 K{r_fresa_s-pb_rc} F{vel_curvas}
G01 Z{0} F{vel_z}

'como el interior
IF pb_db>3*(r_fresa_s-solape)
	G01 Z{2*(r_fresa_s-solape)}
	G01 X{2*r_fresa_s} F{vel_mec}
ENDIF


IF pb_db>5*(r_fresa_s-solape)
	G01 Z{4*(r_fresa_s-solape)} F{vel_z}
	G01 X{pb_da-2*(r_fresa_s-solape)} F{vel_mec}
ENDIF
G01 Z0 F{vel_z}

IF pb_doblep
	' Hago la segunda pasada hasta la prof. del pernio
	aux_y:=pos_y0-prof_pernio

	//cambia la entrada sobre el cuello del pernio. Esto se hace por un problema de arranque de viruta
	IF modelo=2	
		//cambio la entrada de la biasgra y reposiciono para que continue normalmente
		'Entrada izquierda de la bisagra
		G01 X{r_fresa_s} F{vel_xy}
		G01 Y{aux_y} F{vel_pen}

		G01 Z{r_fresa_s/2} F{vel_z}
		G01 X{pb_da-r_fresa_s} F{vel_mec}

	ELSE
		'entrada por la derecha ,normal
		G01 X{pb_da-r_fresa_s} F{vel_xy}
		G01 Y{aux_y} F{vel_pen}
	ENDIF

	'comienza el mecanizado
	'perimetro de la bisagra
	G01 Z0 F{vel_z}
	G01 X{r_fresa_s} F{vel_mec}
	G01 Z{pb_db-pb_rc} F{vel_z}
	G03 X{pb_rc} Z{pb_db-r_fresa_s} I{pb_rc-r_fresa_s} K0 F{vel_curvas}
	G01 X{pb_da-pb_rc} F{vel_mec}

	G03 X{pb_da-r_fresa_s} Z{pb_db-pb_rc} I0 K{r_fresa_s-pb_rc} F{vel_curvas}
	G01 Z0 F{vel_z}

	'como el interior
	IF pb_db>3*(r_fresa_s-solape)
		G01 Z{2*(r_fresa_s-solape)}
		G01 X{2*r_fresa_s} F{vel_mec}
	ENDIF
	IF pb_db>5*(r_fresa_s-solape)
		G01 Z{4*(r_fresa_s-solape)} F{vel_z}
		G01 X{pb_da-2*(r_fresa_s-solape)} F{vel_mec}
	ENDIF
	G01 Z0 F{vel_z}
ENDIF


' Agujero
IF pb_ra>0
	pos_a:= pb_ax-pb_ra+r_fresa_s
	G01 Z{pb_az} F{vel_z}
	G01 X{pos_a} F{vel_mec}
	IF pb_ra>r_fresa_s
		// profundiza haciendo circulos
		G01 Y{aux_y-4} F{vel_pen}
		//finalizo el lambor
		//SIN LAMBOR

		G03 X{pos_a} Z{pb_az} I{pb_ra-r_fresa_s} K0 F700
		G01 Y{aux_y-8} F{vel_pen}
		G03 X{pos_a} Z{pb_az} I{pb_ra-r_fresa_s} K0 F700
		G01 Y{aux_y-12} F{vel_pen}
		G03 X{pos_a} Z{pb_az} I{pb_ra-r_fresa_s} K0 F700
	ELSE
		' solo penetra
		G01 Y{aux_y-12} F{vel_pen}
	ENDIF
ENDIF

PARAR FRESA
'fin de la bisagra me retiro en Y
//finalizo el lambor
SIN LAMBOR


IF palpador==1
	G01 Y{dsys} F{vel_xy}
ELSE
	G01 Y{ancho_pta+maq_dfresa+dsys} F{vel_xy}
ENDIF
M05

