// Bisagra puerta blindada, para el marco
PARAMETROS pos_x,K,primera

// version maquina vieja
//DSZ:=30
// Revisar, que era esta variable, no la encontre en ningun lado!
comp_y:=0		

//pb_mdm -> Desfasaje del marco en relacion a la puerta
// Pero pb_marco le pasa la medida ya desfasada (pos_x ya tiene el desfasaje)	

IF K==1
	// puerta derecha
	G52 X{pos_x} Z{l_fresa_p} Y{36-comp_y}			
	
	G01 X{pb_da-r_fresa_p} Y0 F{vel_xy}

	IF primera
		M04
	ENDIF
	
	G01 Z{-pb_prof} F{vel_pen}
	ACTIVAR FRESA

	G01 X{r_fresa_p} F{vel_mec}
	G01 Y{pb_db-pb_rc}
	G02 X{pb_rc} Y{pb_db-r_fresa_p} I{pb_rc-r_fresa_p} J0 F{vel_curvas}
	G01 X{pb_da-pb_rc} F{vel_mec}
	G02 X{pb_da-r_fresa_p} Y{pb_db-pb_rc} I0 J{r_fresa_p-pb_rc} F{vel_curvas}

	G01 Y0 F{vel_mec}
	G01 Y{r_fresa_p-1}
	G01 X{r_fresa_p*2}
	IF pb_db-r_fresa_p*4-2>0
		G01 Y{K*(pb_db-r_fresa_p*3+2)}
		G01 X{pb_da-r_fresa_p*2-1}
	ENDIF
	G01 Y0
	PARAR FRESA

	// Agujero
	IF pb_ra>0
		pos_a:= pb_da- pb_ax-pb_ra+r_fresa_p

		G01 Y{K*pb_az}
		G01 X{pos_a} 

		G01 Z{-pb_prof-5} F{vel_pen}
		G02 X{pos_a} Y{K*pb_az} I{pb_ra-r_fresa_p} J0 F{vel_curvas}
		G01 Z{-pb_prof-10} F{vel_pen}
		G02 X{pos_a} Y{K*pb_az} I{pb_ra-r_fresa_p} J0 F{vel_curvas}
		IF pb_prof<5
			G01 Z{-15} F{vel_pen}
			G02 X{pos_a} Y{K*pb_az} I{pb_ra-r_fresa_p} J0 F{vel_curvas}
		ENDIF
	ENDIF

	G01 Z{dsz} F{vel_z}


ELSE
	// puerta izquierda
	G52 X{pos_x} Y{-(36-comp_y)} Z0
		
	G01 X{r_fresa_p} Y0 F{vel_xy}

	IF primera
		M04
	ENDIF

	ACTIVAR FRESA
	G01 Z{-pb_prof} F{vel_pen}

	G01 X{pb_da-r_fresa_p} F{vel_mec}
	G01 Y{-(pb_db-pb_rc)}
	G02 X{pb_da-pb_rc} Y{-(pb_db-r_fresa_p)} I{-(pb_rc-r_fresa_p)} J0 F{vel_curvas}
	G01 X{pb_rc} F{vel_mec}
	G02 X{r_fresa_p} Y{-(pb_db-pb_rc)} I0 J{-(r_fresa_p-pb_rc)} F{vel_curvas}

	G01 Y0 F{vel_mec}
	G01 Y{-r_fresa_p+1}
	G01 X{pb_da-r_fresa_p*2}
	// de arriba y de abajo comio 14+14 =28 ... aprox 26 para dejar 1mm de cada lado
	// en total tiene pb_db luego si pb_db-26>0 falta comer la isla
	IF pb_db-r_fresa_p*4+2>0
		G01 Y{-(pb_db-r_fresa_p*2)}
		G01 X{2*r_fresa_p-1}
	ENDIF
	G01 Y0
	PARAR FRESA
	

	// Agujero
	IF pb_ra>0
		pos_a:=  pb_ax-pb_ra+r_fresa_p

		G01 Y{K*pb_az}
		G01 X{pos_a} 

		G01 Z{-pb_prof-5} F{vel_pen}
		G02 X{pos_a} Y{K*pb_az} I{pb_ra-r_fresa_p} J0 F{vel_curvas}
		G01 Z{-pb_prof-10} F{vel_pen}
		G02 X{pos_a} Y{K*pb_az} I{pb_ra-r_fresa_p} J0 F{vel_curvas}
		IF pb_prof<5
			G01 Z{-15} F{vel_pen}
			G02 X{pos_a} Y{K*pb_az} I{pb_ra-r_fresa_p} J0 F{vel_curvas}
		ENDIF
	ENDIF

	G01 Z{dsz} F{vel_z}

ENDIF

PARAR FRESA
//M05


