// Pasadores para puertas dobles

// largo_sup,largo_inf, prof_pasador,ancho_pasador


PLANO XY
G49 G40 G80 G50 G61 G90
G54
G92.1
G21 
S1000


aux_maq_dz:=maq_dz
maq_dz := maq_dz + lambor_dz

IF mano_pta==1
	K:=-1
ELSE
	K:=1
ENDIF

IF !retropalpador
	//retira la punta del palpador que est� tocando el topecito met�lico, y se puede mover sin enganchar nada.
	G01 Z2 F{vel_z}
ENDIF

G01 X0 F{vel_xy}
G01 Y0 F{vel_y}
	

// le aplico el lambor a la prof. del pasador
prof_pasador:=prof_pasador + dang
//es el desfasaje de la posicion del tope en x que se va a usar
dpos_x0 := 0
//posicion desde la punta de la fresa hasta la sup de la madera
//pos_y0 := esp_pta
IF K == 1
	//Puerta derecha, el Y va normal
	pos_y0 := 0 + desf_glob
ELSE
	//Puerta izquierda, el Y va espejado
	pos_y0:= esp_pta - desf_glob
ENDIF

IF retropalpador
	//modelo con retro palpador
	pos_z0 := -maq_dz + l_fresa_p +rebaje_canto
ELSE
	//modelo con palpador directo
	pos_z0 := 0
ENDIF

dpalpador_z := dpalpador_z +rebaje_canto

// no puedo hacer un solo pasador, o hace los 2 o ninguno
IF largo_inf!=0 .AND. largo_sup==0
	largo_sup:=largo_inf
ELSE
	IF largo_sup!=0 .AND. largo_inf==0
		largo_inf := largo_sup
	ENDIF
ENDIF

// pasador de la izquierda
//IF K == -1
//	largo_pasador:=largo_inf
//ELSE
	largo_pasador:=largo_sup
//ENDIF

G52 X{dpos_x0} Y{pos_y0} Z{pos_z0}

IF largo_pasador

	// Pasador de la izquierda
	// Lo mando para palpar
	
	
	//PROGRAMA palpam CON largo_pasador-r_fresa_p,0
	//las siguientes instrucciones reemplazan al palpam.pro, en este caso particular 
	//no se podia utilizar porque tiene una posicion Y fija y aca necesitamos que se adapte al mecanizado
	(Palpa)
	G01 Y{K*((esp_pta-ancho_pasador)/2 + r_fresa_p)} F{vel_y}	
	G01 X{largo_pasador-r_fresa_p+60} F{vel_xy}
	IF retropalpador
		//solo para la maquina vieja que retropalpa
		G01 Z1 F{vel_z}
	ENDIF
	G28.1 Z1
	G52 Z{dpalpador_z}
	G01 Z2 F{vel_z}
	//fin de palpado
	
	G01 X{largo_pasador-r_fresa_p} F{vel_xy}
	G01 Y{K*esp_pta/2} F{vel_y}
	ACTIVAR FRESA
	M03
	IF ancho_pasador<=2*r_fresa_p
		pen_z:=0
		WHILE pen_z<prof_pasador
			pen_z:=pen_z+feed
			IF pen_z>prof_pasador
				pen_z:= prof_pasador
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
			G01 X0 F{vel_cajeado}
			
			IF pen_z < prof_pasador
				pen_z := pen_z+feed
				IF pen_z > prof_pasador
					pen_z := prof_pasador
				ENDIF
				G01 Z{-pen_z} F{vel_pen}
				G01 X{largo_pasador-r_fresa_p} F{vel_cajeado}
			ENDIF
		END WHILE 
	ELSE
		pen_z:=0
		G01 X0 F{vel_cajeado}
		WHILE pen_z < prof_pasador
			pen_z := pen_z + feed
			IF pen_z > prof_pasador
				pen_z := prof_pasador
			ENDIF
			G01 Y{K*((esp_pta+ancho_pasador)/2 - r_fresa_p)} F{vel_mec}
			G01 Z{-pen_z} F{vel_pen}
			G01 X{largo_pasador-r_fresa_p} F{vel_cajeado}
			G01 Y{K*((esp_pta-ancho_pasador)/2 + r_fresa_p)} F{vel_mec}
			G01 X0 F{vel_cajeado}
			
		END WHILE 

	ENDIF

	// pasador de la izquierda
	//IF K == -1
	//	largo_pasador:=largo_sup
	//ELSE
		largo_pasador:=largo_inf
	//ENDIF
	
	PARAR FRESA
	
	G01 Z{dsz+rebaje_canto} F{vel_z}
	
	//apago para palpar
	M05	
	
	//PROGRAMA palpam CON alto_pta-largo_pasador+r_fresa_p,0
	//las siguientes instrucciones reemplazan al palpam.pro, en este caso particular 
	//no se podia utilizar porque tiene una posicion Y fija y aca necesitamos que se adapte al mecanizado
	(Palpa)
	G01 Y{K*((esp_pta-ancho_pasador)/2 + r_fresa_p)} F{vel_y}	
	G01 X{alto_pta-largo_pasador+r_fresa_p+60} F{vel_xy}
	G01 Z1 F{vel_z}
	G28.1 Z1
	G52 Z{dpalpador_z}
	G01 Z2 F{vel_z}
	//fin de palpado
	
	
	G01 X{alto_pta-largo_pasador+r_fresa_p} F{vel_xy}
	G01 Y{K*esp_pta/2} F{vel_y}
	ACTIVAR FRESA
	M03
	IF ancho_pasador <= 2*r_fresa_p
		pen_z := 0
		WHILE pen_z < prof_pasador
			pen_z := pen_z + feed
			IF pen_z > prof_pasador
				pen_z := prof_pasador
			ENDIF
			G01 Z{-pen_z} F{vel_pen}
			G01 X{alto_pta} F{vel_cajeado}
		
			IF pen_z < prof_pasador
				pen_z := pen_z + feed
				IF pen_z > prof_pasador
					pen_z := prof_pasador
				ENDIF
				G01 Z{-pen_z} F{vel_pen}
				G01 X{alto_pta-largo_pasador+r_fresa_p} F{vel_cajeado}
			ENDIF
		END WHILE 

	ELSE
		pen_z := 0
		G01 X{alto_pta} F{vel_cajeado}
		WHILE pen_z < prof_pasador
			pen_z := pen_z + feed
			IF pen_z > prof_pasador
				pen_z := prof_pasador
			ENDIF
			G01 Y{K*((esp_pta+ancho_pasador)/2 - r_fresa_p)} F{vel_mec}
			G01 Z{-pen_z} F{vel_pen}
			G01 X{alto_pta-largo_pasador+r_fresa_p} F{vel_cajeado}
			G01 Y{K*((esp_pta-ancho_pasador)/2 + r_fresa_p)}
			G01 X{alto_pta} F{vel_cajeado}
			
		END WHILE 

	ENDIF
	
	PARAR FRESA
	G01 Z{dsz+rebaje_canto} F{vel_z}
	
	M05
	
ENDIF


// Chapita del marco para la puerta
IF pos_picaporte

	// le aplico el lambor al espesor de la chapita
	aux_mdi := mdi
	mdi := mdi + dang

	// la cerradura esta desplazada del centro = esp_pta/2, segun dcentro
	//IF K == 1
		G52 X{pos_picaporte+mdp+dpos_x0} Y{pos_y0+K*dcentro} Z{pos_z0}
	//ELSE
	//	G52 X{alto_pta-pos_picaporte-mdp} Y{pos_y0+dcentro} Z{pos_z0}
	//ENDIF

	//IF largo_pasador == 0
		// si no palpo antes
	//	PROGRAMA palpam CON -mda/2+r_fresa_p,0
	//ELSE
		// ya palpo, ahora palpo sobre la posicion de la chapita
	//	PROGRAMA palpam CON -mda/2+r_fresa_p,0
	//ENDIF
	//las siguientes instrucciones reemplazan al palpam.pro, en este caso particular 
	//no se podia utilizar porque tiene una posicion Y fija y aca necesitamos que se adapte al mecanizado
	(Palpa)
	G01 Y{K*((esp_pta-mdb-mdd)/2 + r_fresa_p)} F{vel_y}
	G01 X{-mda/2+r_fresa_p+60} F{vel_xy}
	IF retropalpador
		//solo para la maquina vieja que retropalpa
		G01 Z2 F{vel_z}
	ENDIF
	G28.1 Z1
	G52 Z{dpalpador_z}
	G01 Z2 F{vel_z}
	//fin de palpado
	

	IF mdk != 0 .AND. mdc != mda
		//chapita con cuello 

		G01 X{(-mda/2+r_fresa_p)} F{vel_xy}
		G01 Y{K*esp_pta} F{vel_y}
		ACTIVAR FRESA
		M03
		G01 Z{-mdi} F{vel_pen}
		G01 Y{K*(esp_pta-(mdb+r_fresa_p))} F{vel_mec}
		G01 Y{K*esp_pta}

		IF mdk == 0
			mdk:=mdc/2
		ELSE
			mdk:=mdk+mda/2
		ENDIF

		G17
		G01 X{(mda/2-r_fresa_p)} F{vel_mec}
		G01 Y{K*(esp_pta-(2*r_fresa_p-1))} F{vel_mec}
		G01 X{(-mda/2+r_fresa_p)} F{vel_mec}
		G01 Y{K*(esp_pta-(mdb+r_fresa_p))} F{vel_mec}
		G01 X{-(mdk-mdd/2)} F{vel_mec}
		IF K==1
			G03 X{-(mdk-mdd/2)} Y{(esp_pta-(mdb+mdd-r_fresa_p))} I0 J{-(mdd-2*r_fresa_p)/2} F{vel_curvas}
		ELSE
			G02 X{-(mdk-mdd/2)} Y{-(esp_pta-(mdb+mdd-r_fresa_p))} I0 J{(mdd-2*r_fresa_p)/2} F{vel_curvas}
		ENDIF
		G01 X{(mdc-mdk-mdd/2)} F{vel_mec}
		IF K==1
			G03 X{(mdc-mdk-mdd/2)} Y{(esp_pta-(mdb+r_fresa_p))} I0 J{(mdd-2*r_fresa_p)/2} F{vel_curvas}
		ELSE	
			G02 X{(mdc-mdk-mdd/2)} Y{-(esp_pta-(mdb+r_fresa_p))} I0 J{-(mdd-2*r_fresa_p)/2} F{vel_curvas}
		ENDIF
		G01 X0 F{vel_mec}


		// 1er vaciado 
		IF mde <= 2*r_fresa_p
			// vaciado simple
			PARAR FRESA
			G01 X{-mdf/2} F{vel_xy}
			G01 Y{K*(esp_pta-(mdb+mdd-mdg-r_fresa_p))} F{vel_y}
			ACTIVAR FRESA
			pen_z := mdi
			WHILE pen_z<mdh
				pen_z := pen_z + feed
				IF pen_z > mdh
					pen_z := mdh
				ENDIF

				G01 Z{-pen_z} F{vel_pen}
				G01 X{mdf/2} F{vel_mec/2}

				IF pen_z < mdh

					pen_z := pen_z + feed
					IF pen_z > mdh
						pen_z := mdh
					ENDIF
					G01 Z{-pen_z} F{vel_pen}
					G01 X{-mdf/2} F{vel_mec/2}
				ENDIF
			END WHILE

		ELSE
			// Cajeado rectangular (maximo 28mm)
			PARAR FRESA
			G01 X{-mdf/2} F{vel_xy}
			G01 Y{K*(esp_pta-(mdb+mdd-mdg-r_fresa_p))} F{vel_y}
			ACTIVAR FRESA
			pen_z := mdi
			WHILE pen_z < mdh
				pen_z := pen_z + feed
				IF pen_z > mdh
					pen_z := mdh
				ENDIF

				G01 Z{-pen_z} F{vel_pen}
				G01 X{mdf/2} F{vel_mec/2}
				G01 Y{K*(esp_pta-(mdb+mdd-mdg-mde+r_fresa_p))} F{vel_mec}
				G01 X{-mdf/2} F{vel_mec/2}
				G01 Y{K*(esp_pta-(mdb+mdd-mdg-r_fresa_p))} F{vel_mec}

			END WHILE

		ENDIF

		PARAR FRESA
		G01 Z2 F{vel_z}

		IF mdj != 0
			// Segundo vaciado
			IF mde2 <= 2*r_fresa_p
				// vaciado simple
				G01 X{(mdf/2+mdj+r_fresa_p)} F{vel_xy}
				G01 Y{K*(esp_pta-(mdb+mdd-mdg2-r_fresa_p))} F{vel_y}
				ACTIVAR FRESA
				pen_z := mdi
				WHILE pen_z < mdh2
					pen_z := pen_z + feed
					IF pen_z > mdh2
						pen_z := mdh2
					ENDIF

					G01 Z{-pen_z} F{vel_pen}
					G01 X{(mdf/2+mdj+mdf2-r_fresa_p)} F{vel_mec/2}

					IF pen_z < mdh2

						pen_z := pen_z + feed
						IF pen_z > mdh2
							pen_z := mdh2
						ENDIF
						G01 Z{-pen_z} F{vel_pen}
						G01 X{(mdf/2+mdj+r_fresa_p)} F{vel_mec/2}
					ENDIF
				END WHILE

			ELSE
				// Cajeado rectangular (maximo 28mm)
				G01 X{(mdf/2+mdj+r_fresa_p)} F{vel_xy}
				G01 Y{K*(esp_pta-(mdb+mdd-mdg2-r_fresa_p))} F{vel_y}
				ACTIVAR FRESA
				pen_z := mdi
				WHILE pen_z < mdh2
					pen_z := pen_z + feed
					IF pen_z > mdh2
						pen_z := mdh2
					ENDIF

					G01 Z{-pen_z} F{vel_pen}
					G01 X{(mdf/2+mdj+mdf2-r_fresa_p)} F{vel_mec/2}
					G01 Y{K*(esp_pta-(mdb+mdd-mdg2-mde2+r_fresa_p))} F{vel_mec}
					G01 X{(mdf/2+mdj+r_fresa_p)} F{vel_mec/2}
					G01 Y{K*(esp_pta-(mdb+mdd-mdg2-r_fresa_p))} F{vel_mec}

				END WHILE

			ENDIF
			
		ENDIF
		PARAR FRESA
	ELSE
	//CHAPITA SIN CUELLO
	//chapita sin cuello
	//Cuando la chapita va sin cuello el ancho total de la chapita en vez de ser mdc es mda
	//y el mdc queda inutil
	mdb:=0

	//el mrc lo toma desde la interface
	//mrc:=r_fresa_p
		IF mrc<r_fresa_p		
			mrc:=r_fresa_p
		ENDIF

		G01 X{(-mda/2+r_fresa_p)} F{vel_xy}
		G01 Y{K*esp_pta} F{vel_y}
		
		G17
		
		ACTIVAR FRESA
		M03
		
		G01 Z{-mdi} F{vel_pen}
		
		mdl:=esp_pta-mdd		
		
		//Empiezo a comer el contorno de la chapita
		//NOTA:las X estan al eje o sea se come la mitad del da para la izquierda negativa y la otra mitad para la derecha positiva
		G01 X{-(mda/2-r_fresa_p)} F{vel_mec}
		//estoy abajo me muevo hacia arriba 
		G01 Y{K*(mdl+mrc)} F{vel_mec}
		
		IF mrc <= r_fresa_p
			//si la curvatura es menor al radio de la fresa hago un G01 en vez de un G02/03
			G01 X{(mda/2-mrc)} F{vel_mec}
		ELSE
			IF K==1
				G03 X{-(mda/2-mrc)} Y{(mdl+r_fresa_p)} I{mrc-r_fresa_p} J0	F{vel_curvas}
				//voy hacia la derecha
				G01 X{(mda/2-mrc)} F{vel_mec}
				G03 X{mda/2-r_fresa_p} Y{(mdl+mrc)} I0 J{-(-mrc+r_fresa_p)} F{vel_curvas}
			ELSE
				G02 X{(mda/2-mrc)} Y{-(mdl+r_fresa_p)} I{-mrc+r_fresa_p} J0	F{vel_curvas}
				//voy hacia la derecha
				G01 X{(mda/2-mrc)} F{vel_mec}
				G02 X{-(mda/2-r_fresa_p)} Y{-(mdl+mrc)} I0 J{(-mrc+r_fresa_p)} F{vel_curvas}
			ENDIF			
		ENDIF
		
		//Bajo y como la parte inferior
		G01 Y{K*esp_pta} F{vel_mec}
		G01 X{-(mda/2-r_fresa_p)} F{vel_mec}
		
		//como el interior de la chapita
		//si es mas grande de 21
		IF mdd > 3*r_fresa_p-1
			G01 Y{K*(mdl+r_fresa_p*3-1)} F{vel_mec}
			G01 X{(mda/2-r_fresa_p*2+1)}
		ENDIF
		//si es mas grande de 35
		IF mdd > 5*r_fresa_p-2
			G01 Y{K*(mdl+r_fresa_p*5-2)} F{vel_mec}
			G01 X{-(mda/2-r_fresa_p*2+1)}
		ENDIF
		//si es mas grande de 49
		IF mdd > 7*r_fresa_p-3
			G01 Y{K*(mdl+r_fresa_p*7-3)} F{vel_mec}
			G01 X{(mda/2-r_fresa_p*2+1)}
		ENDIF
		
		// 1er vaciado
		IF mde <= 2*r_fresa_p
			// vaciado simple
			PARAR FRESA
			G01 X{-mdf/2} F{vel_xy}
			G01 Y{K*(esp_pta-(mdb+mdd-mdg-r_fresa_p))} F{vel_y}
			ACTIVAR FRESA
			pen_z := mdi
			WHILE pen_z < mdh
				pen_z := pen_z + feed
				IF pen_z > mdh
					pen_z := mdh
				ENDIF

				G01 Z{-pen_z} F{vel_pen}
				G01 X{mdf/2} F{vel_mec/2}

				IF pen_z < mdh

					pen_z := pen_z + feed
					IF pen_z > mdh
						pen_z := mdh
					ENDIF
					G01 Z{-pen_z} F{vel_pen}
					G01 X{-mdf/2} F{vel_mec/2}
				ENDIF
			END WHILE

		ELSE
			// Cajeado rectangular (maximo 28mm)
			PARAR FRESA
			G01 X{-mdf/2} F{vel_xy}
			G01 Y{K*(esp_pta-(mdb+mdd-mdg-r_fresa_p))} F{vel_y}
			ACTIVAR FRESA
			pen_z := mdi
			WHILE pen_z < mdh
				pen_z := pen_z + feed
				IF pen_z > mdh
					pen_z := mdh
				ENDIF

				G01 Z{-pen_z} F{vel_pen}
				G01 X{mdf/2} F{vel_mec/2}
				G01 Y{K*(esp_pta-(mdb+mdd-mdg-mde+r_fresa_p))} F{vel_mec}
				G01 X{-mdf/2} F{vel_mec/2}
				G01 Y{K*(esp_pta-(mdb+mdd-mdg-r_fresa_p))} F{vel_mec}

			END WHILE

		ENDIF		
		
		PARAR FRESA		
	ENDIF	
	
	mdi := aux_mdi
ENDIF

maq_dz:=aux_maq_dz

'Fin del pasador {voy a referenciar}


PARAR FRESA
G01 Z{dsz+rebaje_canto} F{vel_z}
IF retropalpador
	//solo para la maquina vieja que retropalpa
	G28.1 Z{dsz}
ENDIF

M05

'finalizo

G54
G92.1
G52 X0 Y0
IF retropalpador==0
	G28.1 A0
ENDIF
G01 X{-dist_ax} F{vel_xy}
G01 Y0 F{vel_y}
IF retropalpador
	//solo para la maquina vieja que retropalpa
	G28.1 X{-dist_ax} Y0 Z0
ELSE
	//maquina nueva con palpador directo
	G28.1 X{-dist_ax} Y0
	G28.1 Z{dsz+dpalpador_z}
ENDIF

  