//Programa de la bisagra invisible

IF da==0
	da:= (dc-db)/2
ENDIF

G01 Y2 F{vel_xy}

M03
IF de<=2*r_fresa_s
	//con el diametro de la frasa alcanza para comer toda la bisagra	
	aux_y:= aux_y - profundiza 
	G01 Z{r_fresa_s+dd} F{vel_mec}
	G01 Y{aux_y-pos_y0} F{vel_pen}
	ACTIVAR FRESA

	'con una pasada la fresa come el ancho la bisagra
	G01 X{r_fresa_s} F{vel_mec}
	G01 X{dc-r_fresa_s}

	IF doblep
		'doble pasada
		aux_y:=aux_y-profundiza
		G01 Y{aux_y-pos_y0} F{vel_pen}
		G01 X{r_fresa_s} F{vel_mec}
	ENDIF

	'hago el cajeado
	G01 X{r_fresa_s+da}
	lado:=0
	WHILE aux_y>pos_y0-prof2
		IF aux_y-(pos_y0-prof2)>feed
			aux_y:=aux_y-feed
		ELSE
			aux_y:=pos_y0-prof2
		ENDIF

		G01 Y{aux_y-pos_y0} F{vel_pen}
		//para hacer el cajeado va de lado a lado y baja entre ida y vuelta
		IF lado
			G01 X{r_fresa_s+da}	F{vel_mec}
			lado:=0
		ELSE
			G01 X{da+db-r_fresa_s} F{vel_mec}
			lado:=1
		ENDIF
	END WHILE

ELSE

	G01 X{r_fresa_s+dc/2} F{vel_mec}
	WHILE n_pasadas>0
		
		aux_y:= aux_y - profundiza 
		G01 Z{r_fresa_s+dd} F{vel_mec}
		G01 Y{aux_y-pos_y0} F{vel_pen}
		ACTIVAR FRESA

		//si el radio izquierdo es menor o igual al radio de la fresa en ves de un movimiento circular hago uno recto
		IF ri<=r_fresa_s
			G01 X{r_fresa_s} F{vel_mec}
			G01 Z{de-r_fresa_s+dd} F{vel_mec}
		ELSE
			G01 X{ri} F{vel_mec}
			G03 X{r_fresa_s} Z{ri+dd} I0 K{ri-r_fresa_s} F{vel_curvas}
			G01 Z{de+dd-ri} F{vel_mec}
			G03 X{ri} Z{dd+de-r_fresa_s} I{ri-r_fresa_s} K0 F{vel_curvas}
		ENDIF

		//si el radio derecho es menor o igual al radio de la fresa en ves de un movimiento circular hago uno recto
		IF rd<=r_fresa_s
			G01 X{dc-r_fresa_s} F{vel_mec}
			G01 Z{dd+r_fresa_s} F{vel_mec}
		ELSE
			G01 X{dc-rd} F{vel_mec}
			G03 X{dc-r_fresa_s} Z{dd+de-rd} I0 K{r_fresa_s-rd} F{vel_curvas}
			G01 Z{dd+rd} F{vel_mec}
			G03 X{dc-rd} Z{dd+r_fresa_s} I{r_fresa_s-rd} K0 F{vel_curvas}
		ENDIF
		G01 X{r_fresa_s+dc/2} F{vel_mec}

		n_pasadas:=n_pasadas-1
	END WHILE

	ACTIVAR FRESA
	'Hago el cajeado
	//si el radio izquierdo es menor o igual al radio de la fresa en ves de un movimiento circular hago uno recto
	WHILE aux_y>pos_y0-prof2
		IF aux_y-(pos_y0-prof2)>feed
			aux_y:=aux_y-feed
		ELSE
			aux_y:=pos_y0-prof2
		ENDIF

		G01 X{da+db/2} F{vel_xy}
		G01 Y{aux_y-pos_y0} F{vel_pen}
		IF ri<=r_fresa_s
			G01 X{da+r_fresa_s} F{vel_mec}
			G01 Z{de-r_fresa_s+dd} F{vel_mec}
		ELSE
			G01 X{da+ri}
			G03 X{da+r_fresa_s} Z{ri+dd} I0 K{ri-r_fresa_s} F{vel_curvas}
			G01 Z{de+dd-ri} F{vel_mec}
			G03 X{da+ri} Z{dd+de-r_fresa_s} I{ri-r_fresa_s} K0 F{vel_curvas}
		ENDIF

		//si el radio derecho es menor o igual al radio de la fresa en ves de un movimiento circular hago uno recto
		IF rd<=r_fresa_s
			G01 X{da+db-r_fresa_s} F{vel_mec}
			G01 Z{dd+r_fresa_s} F{vel_mec}
		ELSE
			G01 X{da+db-rd} F{vel_mec}
			G03 X{da+db-r_fresa_s} Z{dd+de-rd} I0 K{r_fresa_s-rd} F{vel_curvas}
			G01 Z{dd+rd} F{vel_mec}
			G03 X{da+db-rd} Z{dd+r_fresa_s} I{r_fresa_s-rd} K0 F{vel_curvas}
		ENDIF
		G01 X{da+db/2} F{vel_mec}
	END WHILE
ENDIF
PARAR FRESA


