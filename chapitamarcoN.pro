// Chapita del marco, maquina nueva
// mdl = calle
// mrc = curvatura para la chapita sin cuello (hibrida)
// mrc = Inclinacion para la chapita con cuello
// 
// 

inc_cuello := 0
IF mdk!=0 .AND. mdc!=mda
	//si tiene cuello la inclinacion es igual al mrc
	inc_cuello := mrc
ENDIF


dy:=esp_pta + mds
//mdl:=dy-mdd-mdb
//
//centro:=(esp_pta+mde)/2+mdg - luz - dcentro

IF calle_chapita == 0
	mdl:=(esp_pta+mds)-mdd-mdb
ELSE
	mdl:=calle_chapita
ENDIF
pos_z0:=l_fresa_p
IF modelo == 3
	//en la FV1 el z0 definido en la prifundidad de la maquina
	IF retropalpador
		pos_z0:= -maq_dz + l_fresa_p
	ELSE
		pos_z0:= 0
	ENDIF
ENDIF

//si mdb es igual a cero la chapita no lleva cuello

'Chapita del marco
	//IF !tope_mesa
	//	IF mano_pta==0
	//		G59P11
	//	ELSE
	//		G59P10
	//	ENDIF
	//ELSE
	//	IF mano_pta==0
	//		M5911
	//	ELSE
	//		M5910
	//	ENDIF
	//ENDIF
	
IF mano_pta == 0
	IF alto_pta+mdm<=marco_tope1 .OR. modelo == 3
		// entre en el primer tope
		IF !tope_mesa
			G59P11
		ELSE
			M5911
		ENDIF
	ELSE
		// es muy largo, va contra el segundo tope (usualmente del largo maximo de la maquina)
		IF !tope_mesa
			G59P14
		ELSE
			M5911
		ENDIF
	ENDIF	
ELSE	
	IF !tope_mesa
		G59P10
	ELSE
		M5910
	ENDIF
ENDIF	

	IF modelo == 0 .OR. tope_mesa
		// FV-2 
		pos_y:=dy
	ELSE
		// FV-4 
		pos_y:=0
	ENDIF

	IF pos_x1 != 0 .AND. pernios != 0 .AND. modelo >= 2 .AND. palpador_m == 1
		G52 X{-K*(pos_picaporte+mdp+mdm)} Y{pos_y} Z{pos_z0}
	ELSE
		// hago un offset de Z compensando con el desfasaje de la mesa del marco
		G52 X{-K*(pos_picaporte+mdp+mdm)} Y{pos_y} Z{-desf_marco_z(-K*(pos_picaporte+mdp+mdm)) + pos_z0}
	ENDIF

	
	IF modelo >= 2 .AND. palpador_m == 1
		// LO MANDO A PALPAR
		PROGRAMA palpam CON K*(-mda/2+r_fresa_p),1
		
	ENDIF
	
	IF K==-1
		IF mdb!=0
			IF modelo == 3
				G01 X{mda/2-r_fresa_p - inc_cuello} F{vel_xy}
				G01 Y{-dy} F{vel_y}
			ELSE
				G01 X{mda/2-r_fresa_p} Y{-dy} F{vel_xy}
			ENDIF
		ELSE
			IF modelo == 3
				G01 X{-(mda/2-r_fresa_p)} F{vel_xy}
				G01 Y{-(mdl+mdd-r_fresa_p)} F{vel_y}
			ELSE
				G01 X{-(mda/2-r_fresa_p)} Y{-(mdl+mdd-r_fresa_p)} F{vel_xy}
			ENDIF
		ENDIF
		
		ACTIVAR FRESA
		IF modelo < 2
			M04
		ENDIF
		'comienza el mecanizado
		G01 Z0.2 F{vel_z}
		G01 Z{-mdi} F{vel_pen}
	  IF mdb!=0
	  	G01 X{-mda/2+r_fresa_p + inc_cuello} Y{-dy} F{vel_mec}
	  ENDIF
	ELSE
		IF mdb!=0
			IF modelo == 3
				G01 X{-(-mda/2+r_fresa_p + inc_cuello)} F{vel_xy}
				G01 Y{-dy} F{vel_y}
			ELSE
				G01 X{-(-mda/2+r_fresa_p)} Y{-dy} F{vel_xy}
			ENDIF
		ELSE
			IF modelo == 3
				G01 X{-K*(-mda/2+r_fresa_p)} F{vel_xy}
				G01 Y{-(mdl+mdd-r_fresa_p)} F{vel_y}
			ELSE
				G01 X{-K*(-mda/2+r_fresa_p)} Y{-(mdl+mdd-r_fresa_p)} F{vel_xy}
			ENDIF
		ENDIF
		ACTIVAR FRESA
		IF modelo < 2
			M04
		ENDIF
	  'comienza el mecanizado
	  G01 Z0.2 F{vel_z}
	  G01 Z{-mdi} F{vel_pen}
	ENDIF
  
  IF mdk!=0 .AND. mdc!=mda
		//chapita con cuello
			
		// el usuario ingreso mdk = dist al comienzo del cuello
		// pero yo quiero la distancia al centro del cuello => hago las correciones
		IF mdk==0
		  // simetrica
		 mdk:=mdc/2
		ELSE
		  // transformo para que mdk = distancia al eje del cuello
		  mdk:=mdk+mda/2
		ENDIF
		
		IF mdb!=0
			'perimetro del cuello
			G01 X{K*(mda/2-r_fresa_p)} Y{-(mdl+mdd-r_fresa_p)} F{vel_mec}
			G01 X{-K*(mda/2-r_fresa_p)}
			G01 G01 X{-K*(mda/2-r_fresa_p - inc_cuello)} Y{-dy}
			G01 X{K*(mda/2-r_fresa_p - inc_cuello)}
		  
			'interior del cuello
			G01 Y{-dy+(2*r_fresa_p-1)}
			G01 X{-K*(mda/2-r_fresa_p)}
		ENDIF
		  
		//me posiciono para hacer el cuerpo
		G01 Y{-(mdl+mdd-r_fresa_p)}
		  
		'perimetro del cuerpo de la chapita
		G01 X{K*(mdk-mdd/2)}
		  
		IF K==1
		  G03 X{(mdk-mdd/2)} Y{-(mdl+r_fresa_p)} I0 J{(mdd-2*r_fresa_p)/2} F{vel_curvas}
		ELSE
		  G02 X{-(mdk-mdd/2)} Y{-(mdl+r_fresa_p)} I0 J{(mdd-2*r_fresa_p)/2} F{vel_curvas}
		ENDIF
		G01 X{-K*(mdc-mdk-mdd/2)} F{vel_mec}
		IF K==1
		  G03 X{-(mdc-mdk-mdd/2)} Y{-(mdl+mdd-r_fresa_p)} I0 J{-(mdd-2*r_fresa_p)/2} F{vel_curvas}
		ELSE
		  G02 X{(mdc-mdk-mdd/2)} Y{-(mdl+mdd-r_fresa_p)} I0 J{-(mdd-2*r_fresa_p)/2} F{vel_curvas}
		ENDIF
		IF mdb!=0
			G01 X0 F{vel_mec}
		ELSE
			G01 X{K*(mdk-mdd/2 - r_fresa_p)}
		ENDIF
	  	  
  
	  IF mdd>4*r_fresa_p
		  ' como la isla central
		  G01 Y{-(mdl+mdd/2)} F{vel_mec}
		  G01 X{K*(mdk-r_fresa_p)}
		  G01 X{-K*(mdc-mdk-r_fresa_p)}
		  G01 X0
	  ENDIF
  
  
	  ' 1er vaciado
	  IF mde<=2*r_fresa_p
		  ' vaciado simple
		  PARAR FRESA
		  IF modelo == 3
		  	//G01 X{-K*(-mdf/2)} F{vel_xy}
		  	G01 X{-K*(-mdf/2+r_fresa_p)} F{vel_xy}
		  	G01 Y{-(mdl+mdg+r_fresa_p)} F{vel_y}
		  ELSE
		  	//G01 X{-K*(-mdf/2)} Y{-(mdl+mdg+r_fresa_p)} F{vel_xy}
		  	G01 X{-K*(-mdf/2+r_fresa_p)} Y{-(mdl+mdg+r_fresa_p)} F{vel_xy}
		  ENDIF
		  ACTIVAR FRESA
		  pen_z:=mdi
		  WHILE pen_z<mdh
			  pen_z:=pen_z+feed
			  IF pen_z>mdh
				  pen_z:= mdh
			  ENDIF
  
			  G01 Z{-pen_z} F{vel_pen}
			  //G01 X{-K*(mdf/2)} F{vel_mec}
			  G01 X{-K*(mdf/2-r_fresa_p)} F{vel_mec}
  
			  IF pen_z<mdh
  
				  pen_z:=pen_z+feed
				  IF pen_z>mdh
					  pen_z:= mdh
				  ENDIF
				  G01 Z{-pen_z} F{vel_pen}
				  //G01 X{-K*(-mdf/2)} F{vel_mec}
				  G01 X{-K*(-mdf/2+r_fresa_p)} F{vel_mec}
			  ENDIF
		  END WHILE
  
	  ELSE
		  'Cajeado rectangular {maximo 28mm}
		  PARAR FRESA
		  IF modelo == 3
		  	//G01 X{-K*(-mdf/2)} F{vel_xy}
		  	G01 X{-K*(-mdf/2+r_fresa_p)} F{vel_xy}
		  	G01 Y{-(mdl+mdg+r_fresa_p)} F{vel_y}
		  ELSE
		  	//G01 X{-K*(-mdf/2)} Y{-(mdl+mdg+r_fresa_p)} F{vel_xy}
		  	G01 X{-K*(-mdf/2+r_fresa_p)} Y{-(mdl+mdg+r_fresa_p)} F{vel_xy}
		  ENDIF
		  ACTIVAR FRESA
		  pen_z:=mdi
		  WHILE pen_z<mdh
			  pen_z:=pen_z+feed
			  IF pen_z>mdh
				  pen_z:= mdh
			  ENDIF
  
			  G01 Z{-pen_z} F{vel_pen}
			  //G01 X{-K*(mdf/2)} F{vel_mec}
			  G01 X{-K*(mdf/2-r_fresa_p)} F{vel_mec}
			  G01 Y{-(mdl+mdg+mde-r_fresa_p)}
			  //G01 X{-K*(-mdf/2)}
			  G01 X{-K*(-mdf/2+r_fresa_p)}
			  G01 Y{-(mdl+mdg+r_fresa_p)}
  
		  END WHILE
  
	  ENDIF
  
	  PARAR FRESA
	  G01 Z{dsz} F{vel_z}
  
  
	  IF mdj!=0
		  ' Segundo vaciado
		  IF mde2<=2*r_fresa_p
			  ' vaciado simple
			  IF modelo == 3
			  	G01 X{-K*(mdf/2+mdj+r_fresa_p)} F{vel_xy}
			  	//G01 X{-K*(mdf/2+mdj)} F{vel_xy}
			  	G01 Y{-(mdl+mdg2+r_fresa_p)} F{vel_y}
			  ELSE
			  	G01 X{-K*(mdf/2+mdj+r_fresa_p)} Y{-(mdl+mdg2+r_fresa_p)} F{vel_xy}
			  	//G01 X{-K*(mdf/2+mdj)} Y{-(mdl+mdg2+r_fresa_p)} F{vel_xy}
			  ENDIF
			  ACTIVAR FRESA
				G01 Z0.2 F{vel_z}
			  pen_z:=mdi
			  WHILE pen_z<mdh2
				  pen_z:=pen_z+feed
				  IF pen_z>mdh2
					  pen_z:= mdh2
				  ENDIF
  
				  G01 Z{-pen_z} F{vel_pen}
				  G01 X{-K*(mdf/2+mdj+mdf2-r_fresa_p)} F{vel_mec}
				  //G01 X{-K*(mdf/2+mdj+mdf2)} F{vel_mec}
  
				  IF pen_z<mdh2
  
					  pen_z:=pen_z+feed
					  IF pen_z>mdh2
						  pen_z:= mdh2
					  ENDIF
					  G01 Z{-pen_z} F{vel_pen}
					  G01 X{-K*(mdf/2+mdj+r_fresa_p)} F{vel_mec}
					  //G01 X{-K*(mdf/2+mdj)} F{vel_mec}
				  ENDIF
			  END WHILE
  
		  ELSE
			  ' Cajeado rectangular {maximo 28mm}
			  IF modelo == 3
			  	G01 X{-K*(mdf/2+mdj+r_fresa_p)} F{vel_xy}
			  	//G01 X{-K*(mdf/2+mdj)} F{vel_xy}
			  	G01 Y{-(mdl+mdg2+r_fresa_p)} F{vel_y}
			  ELSE
			  	G01 X{-K*(mdf/2+mdj+r_fresa_p)} Y{-(mdl+mdg2+r_fresa_p)} F{vel_xy}
			  	//G01 X{-K*(mdf/2+mdj)} Y{-(mdl+mdg2+r_fresa_p)} F{vel_xy}
				ENDIF
			  ACTIVAR FRESA			  
				G01 Z0.2 F{vel_z}
			  pen_z:=mdi
			  WHILE pen_z<mdh2
				  pen_z:=pen_z+feed
				  IF pen_z>mdh2
					  pen_z:= mdh2
				  ENDIF
  
				  G01 Z{-pen_z} F{vel_pen}
				  G01 X{-K*(mdf/2+mdj+mdf2-r_fresa_p)} F{vel_mec}
				  //G01 X{-K*(mdf/2+mdj+mdf2)} F{vel_mec}
				  G01 Y{-(mdl+mdg2+mde2-r_fresa_p)} 
				  G01 X{-K*(mdf/2+mdj+r_fresa_p)}
				  //G01 X{-K*(mdf/2+mdj)}
				  G01 Y{-(mdl+mdg2+r_fresa_p)} 
  
			  END WHILE
  
		  ENDIF
  		
  		'Finaliza el programa chapita
  		
	  	PARAR FRESA
			G01 Z{dsz} F{vel_z}
		ENDIF
		
	ELSE
		'chapita sin cuello
		IF mrc<r_fresa_p
			mrc:=r_fresa_p
		ENDIF
		mdl:=dy-mdd
		
		
		IF msentido=0
			'sentido horario
			
			'como el perimetro
			
			G01 Y{-(mdl+mrc)} F{vel_mec}
			
			IF K==1
				G03 X{(mda/2-mrc)} Y{-(mdl+r_fresa_p)} I{-mrc+r_fresa_p} J0 F{vel_curvas}
			ELSE
				G02 X{-(mda/2-mrc)} Y{-(mdl+r_fresa_p)} I{mrc-r_fresa_p} J0 F{vel_curvas}
			ENDIF
			G01 X{-K*(mda/2-mrc)} F{vel_mec}
			IF K==1
				G03 X{-(mda/2-r_fresa_p)} Y{-(mdl+mrc)} I0 J{-mrc+r_fresa_p} F{vel_curvas}
			ELSE
				G02 X{(mda/2-r_fresa_p)} Y{-(mdl+mrc)} I0 J{-mrc+r_fresa_p} F{vel_curvas}
			ENDIF
			
			G01 Y{-dy} F{vel_mec}
			G01 X{K*(mda/2-r_fresa_p)}
						
			
		ELSE
			'sentido normal antihorario
			'como el perimetro de la chapita sin cuello
			G01 X{-K*(mda/2-r_fresa_p)} F{vel_mec}
			G01 Y{-(mdl+mrc)}
			IF K==1
				G02 X{-(mda/2-mrc)} Y{-(mdl+r_fresa_p)} I{mrc-r_fresa_p} J0	F{vel_curvas}
			ELSE
				G03 X{(mda/2-mrc)} Y{-(mdl+r_fresa_p)} I{-mrc+r_fresa_p} J0	F{vel_curvas}
			ENDIF
			G01 X{K*(mda/2-mrc)} F{vel_mec}
			IF K==1
				G02 X{mda/2-r_fresa_p} Y{-(mdl+mrc)} I0 J{-mrc+r_fresa_p} F{vel_curvas}
			ELSE
				G03 X{-(mda/2-r_fresa_p)} Y{-(mdl+mrc)} I0 J{-mrc+r_fresa_p} F{vel_curvas}
			ENDIF
			G01 Y{-dy} F{vel_mec}
		
		ENDIF
		
		
		IF mdd>3*r_fresa_p-1
		  IF mdd>5*r_fresa_p-2
				G01 Y{-dy+r_fresa_p*2-1}
				G01 X{-K*(mda/2-r_fresa_p*2+1)}
			ELSE
				G01 Y{-(mdl+r_fresa_p*3-1)}
				G01 X{-K*(mda/2-r_fresa_p*2+1)}
			ENDIF
		ENDIF
		
		IF mdd>5*r_fresa_p-2
			IF mdd>7*r_fresa_p-3
				G01 Y{-dy+r_fresa_p*4-2}
				G01 X{K*(mda/2-r_fresa_p*2+1)}
			ELSE
				G01 Y{-(mdl+r_fresa_p*3-1)}
				G01 X{K*(mda/2-r_fresa_p*2+1)}
			ENDIF
		ENDIF
		
		IF mdd>7*r_fresa_p-3
			IF mdd>9*r_fresa_p-4
				G01 Y{-dy+r_fresa_p*6-3}
				G01 X{-K*(mda/2-r_fresa_p*2+1)}
			ELSE
				G01 Y{-(mdl+r_fresa_p*3-1)}
				G01 X{-K*(mda/2-r_fresa_p*2+1)}
			ENDIF
		ENDIF
		
		' 1er vaciado 
	  IF mde<=2*r_fresa_p
		  ' vaciado simple
		  PARAR FRESA
		  IF modelo == 3
		  	//G01 X{-K*(-mdf/2)} F{vel_xy}
		  	G01 X{-K*(-mdf/2+r_fresa_p)} F{vel_xy}
		  	G01 Y{-(mdl+mdg+r_fresa_p)} F{vel_xy}
		  ELSE
		  	//G01 X{-K*(-mdf/2)} Y{-(mdl+mdg+r_fresa_p)} F{vel_xy}
		  	G01 X{-K*(-mdf/2+r_fresa_p)} Y{-(mdl+mdg+r_fresa_p)} F{vel_xy}
		  ENDIF
		  ACTIVAR FRESA
			G01 Z0.2 F{vel_z}
		  pen_z:=mdi
		  WHILE pen_z<mdh
			  pen_z:=pen_z+feed
			  IF pen_z>mdh
				  pen_z:= mdh
			  ENDIF
  
			  G01 Z{-pen_z} F{vel_pen}
			  //G01 X{-K*(mdf/2)} F{vel_mec}
			  G01 X{-K*(mdf/2-r_fresa_p)} F{vel_mec}
  
			  IF pen_z<mdh
  
				  pen_z:=pen_z+feed
				  IF pen_z>mdh
					  pen_z:= mdh
				  ENDIF
				  G01 Z{-pen_z} F{vel_pen}
				  //G01 X{-K*(-mdf/2)} F{vel_mec}
				  G01 X{-K*(-mdf/2+r_fresa_p)} F{vel_mec}
			  ENDIF
		  END WHILE
  
	  ELSE
		  ' Cajeado rectangular {maximo 28mm}
		  PARAR FRESA
		  IF modelo == 3
		  	//G01 X{-K*(-mdf/2)} F{vel_xy}
		  	G01 X{-K*(-mdf/2+r_fresa_p)} F{vel_xy}
		  	G01 Y{-(mdl+mdg+r_fresa_p)} F{vel_y}
		  ELSE
		  	//G01 X{-K*(-mdf/2)} Y{-(mdl+mdg+r_fresa_p)} F{vel_xy}
		  	G01 X{-K*(-mdf/2+r_fresa_p)} Y{-(mdl+mdg+r_fresa_p)} F{vel_xy}
		  ENDIF
		  ACTIVAR FRESA
			G01 Z0.2 F{vel_z}
		  pen_z:=mdi
		  WHILE pen_z<mdh
			  pen_z:=pen_z+feed
			  IF pen_z>mdh
				  pen_z:= mdh
			  ENDIF
  
			  G01 Z{-pen_z} F{vel_pen}
			  //G01 X{-K*(mdf/2)} F{vel_mec}
			  G01 X{-K*(mdf/2-r_fresa_p)} F{vel_mec}
			  G01 Y{-(mdl+mdg+mde-r_fresa_p)} 
			  //G01 X{-K*(-mdf/2)}
			  G01 X{-K*(-mdf/2+r_fresa_p)}
			  G01 Y{-(mdl+mdg+r_fresa_p)}
  
		  END WHILE
  
	  ENDIF
  
  	'Finaliza el programa chapita
	  PARAR FRESA
	  G01 Z{dsz} F{vel_z}
	ENDIF
	
	M05
	PARAR FRESA

