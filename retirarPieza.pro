	// retirar pieza

	G54
	G92.1
	G52 X0
	IF pos_y_segura>0
		G01 Z0 F{vel_z}
	ENDIF

	// apago la fresa
	M05

	// retiro de la pieza
	IF no_traer_pieza
		(la pieza sale por el lado opuesto por el que entro)
		//tengo que llevar la pieza a la posicion	pieza_dx + desf_x1
		//desf_x1 es la separacion entre el 0 de la maquina y el tope del otro extremo de la maquina donde se apolla la pieza
		//calculo la minima posicion necesaria para que salga por el tope derecho
		pos_minima := pieza_dx + desf_x1 - (dp-ds-offset)
		(POS_PINZA = {pos_minima}, ANT_POS_PINZA = {pos_pinza} , LARGO PIEZA={pieza_dx},)
		// aca falta mover y para que quede sobre la pieza y prense
		IF pos_pinza < pos_minima
			PROGRAMA moverPinzaPos CON pos_minima
		ENDIF
		
		G54
		G92.1
		G52 X0

		G01 X{pieza_dx + dsx - pos_pinza+230} F{vel_pinza}

		//desactivarTope
		G01 Z{pos_desac_tope-p_offset_z} F{vel_Z}

		//posiciono la pieza para que toque el lado izquierdo de la pieza con el tope derecho
		G01 X{pieza_dx + desf_x1 - pos_pinza} F{vel_pinza}
		
	ELSE
		(la pieza sale por donde entro)
		(offset = {offset})
		pos_maxima := offset
		IF pos_pinza > pos_maxima
			PROGRAMA moverPinzaPos CON pos_maxima
		ENDIF

		G54
		G92.1
		G52 X0

		G01 X{-pos_pinza} F{vel_pinza}

		//desactivarTope
		G01 Z{pos_desac_tope-p_offset_z} F{vel_Z}
		

	ENDIF
	
	// finalizar pieza
	//abro la pinza y pongo en pausa
	//activo el prensor y abro la pinza
	M9
	M21

	M05

	G54
	G52 Z0
	G01 Z0 F{vel_z}
	G52 X0 Y0
	G01 Y120 F{vel_xy}
	G01 X{-dist_ax} f25000
	//G28.1 X{-dist_ax} F{vel_xy}
	G28.1 z0 F{vel_z}

	//referencio el eje a
	G01 A{rot_0} F{vel_rot}
	G28.1 A{rot_0}
	G1 Y120 f30000
	G1 Z-132 f15000


