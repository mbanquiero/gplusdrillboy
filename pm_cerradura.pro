// CERRADURA puerta monopunto
// Desfasaje abajo
// no uso dm (maq_dabajo = 0)
maq_dabajo:=0		

G49 G40 G80 G50 G61 G90
G54
92.1
G21 (mm)
S1000

PLANO XY
G18

IF mano_pta==1
	G59P15
	// OJO: RECORDAR MULTIPLICAR POR K (-1) LOS RADIOS I DE LOS G02 Y G03
	K:=-1
ELSE
	K:=1
ENDIF

G52 Z0
G01 Z0 F2000


IF mano_pta==0
	dm:= maq_dabajo * (pos_picaporte-cdf)/2200
ELSE
	dm:= maq_dabajo * (1-(pos_picaporte-cdf)/2200)
ENDIF

// Cerradura
G52 X{K*(pos_picaporte-cdf)} Y0 Z{-maq_abajo_dz-dm+esp_pta}
G01 X{K*cdb/2} Y-6 F20000
aux_z:=(esp_pta-cdb)/2+8
aux_k:=(cdb-2*8)
G01 Z{-aux_z} F1500
ACTIVAR FRESA
M03
G01 Y{cdl} F500
IF mano_pta==0
	G02 X{K*cdb/2} Z{-aux_z-aux_k} I0  K{-(aux_k/2)} F3000
ELSE
	G03 X{K*cdb/2} Z{-aux_z-aux_k} I0  K{-(aux_k/2)} F3000
ENDIF
G01 X{K*(cda-cdb/2)} F3000
IF mano_pta==0
	G02 X{K*(cda-cdb/2)} Z{-aux_z} I0  K{aux_k/2} F3000
ELSE
	G03 X{K*(cda-cdb/2)} Z{-aux_z} I0  K{aux_k/2} F3000
ENDIF
G01 X{K*cdb/2} F3000
G01 Y-6 F20000
PARAR FRESA


IF mano_pta==0
	dm:= maq_dabajo * (pos_picaporte-cdf)/2200
ELSE
	dm:= maq_dabajo * (1-(pos_picaporte-cdf)/2200)
ENDIF

// Cajon cerraduras
IF circular==1
	G52 X{K*(pos_picaporte-diam/2)} Y0 Z{-maq_abajo_dz-dm+esp_pta}
ELSE
	G52 X{K*(pos_picaporte-cdf+cdh)} Y0 Z{-maq_abajo_dz-dm+esp_pta}
ENDIF
G01 Z{-esp_pta/2} F1500

G01 X0 F3000
ACTIVAR FRESA
prof_y:=cdl
WHILE prof_y<cdd
	prof_y:=prof_y+feed
	IF prof_y>cdd
		prof_y:=cdd
	ENDIF
	
	
	IF circular==1
		G01 X{K*8} F20000
		G01 Y{prof_y} F500
		G02 X{K*8} Z{-esp_pta/2} I{K*(diam/2-8)} K0 F3000
	ELSE
		G01 Y{prof_y} F500
		G01 X{K*cdc} F3000
		IF prof_y<cdd
			prof_y:=prof_y+feed
			IF prof_y>cdd
				prof_y:=cdd
			ENDIF
			G01 Y{prof_y} F500
			G01 X0 F3000
		ENDIF
	ENDIF

END WHILE

G01 Y-6 F20000
G01 X0 F3000
PARAR FRESA
M05
G54
G52 Z0
G01 Z0 F1000

// Aca ya trabajo con otro conjunto, y en otro plano
// Ojo pestillo
IF mano_pta==1
	G59P16
ELSE
	G59P12
ENDIF
G17

IF mano_pta==0
	dm:= maq_dabajo * pos_picaporte/2200
ELSE
	dm:= maq_dabajo * (1-pos_picaporte/2200)
ENDIF

G52 X{K*pos_picaporte} Y{cdi-cdj/2} Z{-maq_abajo_dz-dm+esp_pta}
G01 X0 Y7 F20000
M04

prof_z:=0
WHILE prof_z<esp_pta+2
	prof_z:=prof_z+7
	IF prof_z>=esp_pta
		prof_z:=esp_pta+2
	ENDIF
	G01 Z{-prof_z}  F500
	IF cdj>14
		G02 X0 Y7 I0 J{(cdj-14)/2} F1000
	ENDIF
END WHILE

G54
G52 Z0
G01 Z0 F1000

// Agujero de la condena
IF cdk!=0 .AND. condena==1
	IF mano_pta==1
		G59P16
	ELSE
		G59P12
	ENDIF

	IF mano_pta==0
		dm:= maq_dabajo * (pos_picaporte+cdg)/2200
	ELSE
		dm:= maq_dabajo * (1-(pos_picaporte+cdg)/2200)
	ENDIF

	G52 X{K*(pos_picaporte+cdg)} Y{cdi-cdk/2} Z{-maq_abajo_dz-dm+esp_pta}
	G01 X0 Y7 F20000
	M04

	prof_z:=0
	WHILE prof_z<esp_pta+2
		prof_z:=prof_z+7
		IF prof_z>=esp_pta
			prof_z:=esp_pta+2
		ENDIF
		G01 Z{-prof_z}  F500
		IF cdk>14
			G02 X0 Y7 I0 J{(cdk-14)/2} F1000
		ENDIF
		IF cdk2!=0
			G01 Y{cdk/2} 
			G01 X{K*(cdk2-7)}
			G01 X0
			G01 Y7

		ENDIF

	END WHILE


	G54
	G52 Z0
	G01 Z0 F2000
ENDIF

M05
G54
G52 Z0
G01 Z0 F2000
G52 X0 Y0
G0 X0 Y0  F20000
G28.1 X0 Y0 Z0
  