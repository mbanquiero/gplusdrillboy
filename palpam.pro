PARAMETROS pp_x1,marco
//ojo la fresa tiene que estar apagada antes de llamar a esto


'Palpa


IF modelo == 3
	//FV1
	IF marco
		G01 X{pp_x1+60} F{vel_xy}	
		G01 Y-20 F{vel_y}
	ELSE
		G01 Y{15*K} F{vel_y}
		G01 X{pp_x1+0} F{vel_xy}	
	ENDIF
ELSE
	//FV5
	G01 Y-15 F{vel_xy}
	G01 X{pp_x1}
ENDIF

IF retropalpador .OR. modelo != 3
	//retro palpador modelo anterior de maquina fv1 o puede ser la fv5 que tambien retropalpa
	G01 Z1 F{vel_z}
ENDIF
IF modelo == 3 .AND. retropalpador == 0
	//fv1 con palpador directo. El offset tiene que estar en 0 para que palpe
	G52 Z0
ENDIF
G28.1 Z1
G52 Z{dpalpador_z}
//G01 Z2 F{vel_z}

// prende y apaga la fresa para poder palpar y que se oculte el palpador
// la prendo aca para que se oculte el palpador antes de empezar a hacer el dibujo
M03


