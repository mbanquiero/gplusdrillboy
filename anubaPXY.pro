PARAMETROS pos_x,K,npernio


// bisagra para marco, maquina nueva
// mds = generalemente 1 o 2 mm 

df := calle
dy := esp_pta + mds
	
IF rd < r_fresa_p
	rd := r_fresa_p
ENDIF

dpal_marco:=5

pos_y0 := de

IF K == -1
	//puerta izquierda
	//Los agujeros de la anuba necesitan tener 7 grados de inclinacion.
	//Como el cabezal de la fv1 no puede hacerlo, se coloca la puerta en la maquina con la inclinacion deseada
	//Esa inclinacion se puede hacer para puerta derecha, luego para puerta izquierda la puerta hay que voltearla de cabeza
	//teniendo que espeja la posicion en X de las bisagras.
	//espejo en X
	pos_x := alto_pta - pos_x
	//espejo en Y	
	//pos_y0 := esp_pta-de
ENDIF

//en la FV1 el z0 definido en la profundidad de la maquina
pos_z0 := -maq_dz + l_fresa_p


'Comienzo del programa bisagra en la puerta
G52 X{pos_x} Y{pos_y0} Z{pos_z0}

//bisagra tipo anuba
//Programa de la bisagra Anuba
// Agujero
IF rd>0
	//dejo el offset en el centro del agujero. El X ya esta bien ubicado. Corrijo el Y
	
	'agujero
	
	G01 Y0
	G01 X0
	ACTIVAR FRESA	
	M03

	IF rd<=r_fresa_p
		//radio del agujero igual al de la fresa solo profundiza
		G01 Z{-prof} F{vel_pen}
			
	ELSE
		//radio del agujero distinto del radio de la fresa
		// hace varias pasadas en circulos aumentando la profundidad
		
		//Cantidad de pasadas para hacer el agujero
		cant_pasadas:= 3
		j:=0
		WHILE j<cant_pasadas
			G01 Z{-(j+1)*prof/cant_pasadas} F{vel_pen}
			IF K == 1
				G02 X0 Y0 I{rd-r_fresa_p} J0 F{vel_curvas}
			ELSE
				G03 X0 Y0 I{rd-r_fresa_p} J0 F{vel_curvas}
			ENDIF
			
			j := j + 1
		END WHILE
	ENDIF
	
ENDIF
PARAR FRESA
'finalizo la bisagra en el canto de la puerta
G01 Z{dsz} F{vel_mec}


'fin de la bisagra anuba



