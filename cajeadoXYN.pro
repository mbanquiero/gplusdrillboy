
PARAMETROS pos_x,K,ancho,esp,prof

(Vaciado de marco)

dy:=esp_pta + pb_mds

IF modelo==0 .OR. tope_mesa
    // FV-2
    pos_y:=dy
ELSE
    // FV-4 
    pos_y:=0
ENDIF

IF modelo == 3
	//fv1
	pos_z0 := -maq_dz+l_fresa_p
ELSE
	//resto de la maquinas
	pos_z0 := l_fresa_p
ENDIF

IF modelo == 3 .AND. retropalpador == 0 .AND. pb_frente
	//Nuevo
	//Es la fv1 con palpador directo y como tiene frente el vaciado, Quiere decir que ya palpo (No tengo que volver a palpar)
	pos_z0 := dpalpador_z
ENDIF


IF K==1
	// Puerta Derecha
	G52 X{-pos_x-ancho} Y{pos_y} Z{pos_z0}
ELSE
	// Puerta Izquierda
	G52 X{pos_x} Y{pos_y} Z{pos_z0}
ENDIF

IF modelo == 3 .AND. retropalpador == 0 .AND. pb_frente == 0
	//Nuevo
	// LO MANDO A PALPAR solo para la fv1 con el palpador directo si no hizo el frente del vaciado
	PROGRAMA palpam CON 0,1
ENDIF


G01 Y{-((dy-esp)/2 + r_fresa_p)} F{vel_xy}
G01 X{r_fresa_p}


IF modelo < 2
	M04
ELSE
	M03
ENDIF

G01 Z1 F{vel_z}

pen_z:=0
ACTIVAR FRESA
WHILE pen_z < prof
	pen_z:=pen_z+feed
	IF pen_z>prof
		pen_z:= prof
	ENDIF

	G01 X{r_fresa_p} F{vel_mec}
	G01 Y{-((dy-esp)/2 + r_fresa_p)}
	
	G01 Z{-pen_z} F{vel_pen}
	G01 X{ancho-r_fresa_p} F{vel_mec}
	G01 Y{-((dy+esp)/2 - r_fresa_p)} 
	G01 X{r_fresa_p}

END WHILE
PARAR FRESA
G01 Z{dsz + pb_rebaje_canto}
	
	


