'programa bisagra invisible en el marco

IF de_m<2*r_fresa_p
	de_m:=2*r_fresa_p
ENDIF
//si el da_m es igual a 0 es porque el cajeado va centrado en el ancho de la bisagra
IF da_m==0
	da_m:=(dc_m-db_m)/2
ENDIF
IF ri_m==0
	ri_m:=de_m/2
ENDIF
IF rd_m==0
	rd_m:=de_m/2
ENDIF

G01 Y{-dy+r_fresa_p+dd_m} F{vel_xy}
G01 X{dc_m/2} F{vel_xy}
M502
G4 P2
IF doblep	.OR. prof_m >= 1.5*feed
	n_pasadas:=2
ELSE
	n_pasadas:=1
ENDIF
G01 Z2 F{vel_z}
aux_z:=0
profundiza:=prof_m/n_pasadas

IF de_m<=2*r_fresa_p
	//si con el diametro de la fresa alcanza para comer toda la bisagra
	'con una pasada la fresa come todo el ancho
	aux_z:= -profundiza
	
	G01 Z{aux_z} F{vel_pen}
	ACTIVAR FRESA

	//como el lado izquierdo
	G01 X{r_fresa_p} F{vel_mec}
	//luego el derecho
	G01 X{dc_m-r_fresa_p} F{vel_mec}

	IF doblep
		'segunda pasada
		aux_z:= aux_z - profundiza
		G01 Z{aux_z} F{vel_pen}
		G01 X{r_fresa_p} F{vel_mec}
	ENDIF

	'hago el cajeado
	G01 X{r_fresa_p+da_m} F{vel_xy}
	lado:=0
	aux_z:=-prof_m
	WHILE aux_z>-prof2_m
		IF aux_z+prof2_m>feed
			aux_z:=aux_z-feed
		ELSE
			aux_z:=-prof2_m
		ENDIF
		G01 Z{aux_z} F{vel_pen}
		//para hacer el cajeado va de lado a lado y baja entre ida y vuelta
		IF lado
			G01 X{r_fresa_p+da_m} F{vel_mec}
			lado:=0
		ELSE
			G01 X{da_m+db_m-r_fresa_p} F{vel_mec}
			lado:=1
		ENDIF
	END WHILE

ELSE

	WHILE n_pasadas>0
		
		aux_z:= aux_z - profundiza
		G01 Z{aux_z} F{vel_pen}
		ACTIVAR FRESA

		//si el radio izquierdo es menor o igual al radio de la fresa en ves de un movimiento circular hago uno recto
		'perimetro de la bisagra
		IF ri_m<=r_fresa_p
			G01 X{r_fresa_p} F{vel_mec}
			G01 Y{-dy+de_m-r_fresa_p+dd_m}
		ELSE
			G01 X{ri_m} F{vel_mec}
			G02 X{r_fresa_p} Y{-dy+ri_m+dd_m} I0 J{ri_m-r_fresa_p} F{vel_curvas}
			G01 Y{-dy+de_m+dd_m-ri_m} F{vel_mec}
			G02 X{ri_m} Y{-dy+dd_m+de_m-r_fresa_p} I{ri_m-r_fresa_p} J0 F{vel_curvas}
		ENDIF

		//si el radio derecho es menor o igual al radio de la fresa en ves de un movimiento circular hago uno recto
		IF rd_m<=r_fresa_p
			G01 X{dc_m-r_fresa_p} F{vel_mec}
			G01 Y{-dy+dd_m+r_fresa_p}
		ELSE
			G01 X{dc_m-rd_m} F{vel_mec}
			G02 X{dc_m-r_fresa_p} Y{-dy+dd_m+de_m-rd_m} I0 J{r_fresa_p-rd_m} F{vel_curvas}
			G01 Y{-dy+dd_m+rd_m} F{vel_mec}
			G02 X{dc_m-rd_m} Y{-dy+dd_m+r_fresa_p} I{r_fresa_p-rd_m} J0 F{vel_curvas}
		ENDIF
		//voy rapido porque ya comio
		G01 X{dc_m/2} F{vel_xy}

		n_pasadas:=n_pasadas-1
	END WHILE

	'Hago el cajeado
	//si el radio izquierdo es menor o igual al radio de la fresa en ves de un movimiento circular hago uno recto
	aux_z:=-prof_m
	WHILE aux_z>-prof2_m
		IF aux_z+prof2_m>feed
			aux_z:=aux_z-feed
		ELSE
			aux_z:=-prof2_m
		ENDIF

		G01 X{da_m+db_m/2} F{vel_xy}
		G01 Z{aux_z} F{vel_pen}
		IF ri_m<=r_fresa_p
			G01 X{da_m+r_fresa_p} F{vel_mec}
			G01 Y{-dy+de_m-r_fresa_p+dd_m}
		ELSE
			G01 X{da_m+ri_m} F{vel_mec}
			G02 X{da_m+r_fresa_p} Y{-dy+ri_m+dd_m} I0 J{ri_m-r_fresa_p} F{vel_curvas}
			G01 Y{-dy+de_m+dd_m-ri_m} F{vel_mec}
			G02 X{da_m+ri_m} Y{-dy+dd_m+de_m-r_fresa_p} I{ri_m-r_fresa_p} J0 F{vel_curvas}
		ENDIF

		//si el radio derecho es menor o igual al radio de la fresa en ves de un movimiento circular hago uno recto
		IF rd_m<=r_fresa_p
			G01 X{da_m+db_m-r_fresa_p} F{vel_mec}
			G01 Y{-dy+dd_m+r_fresa_p}
		ELSE
			G01 X{da_m+db_m-rd_m} F{vel_mec}
			G02 X{da_m+db_m-r_fresa_p} Y{-dy+dd_m+de_m-rd_m} I0 J{r_fresa_p-rd_m} F{vel_curvas}
			G01 Y{-dy+dd_m+rd_m} F{vel_mec}
			G02 X{da_m+db_m-rd_m} Y{-dy+dd_m+r_fresa_p} I{r_fresa_p-rd_m} J0 F{vel_curvas}
		ENDIF
		G01 X{da_m+db_m/2} F{vel_mec}
	END WHILE
ENDIF

