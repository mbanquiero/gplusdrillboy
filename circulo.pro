// CIRCULO DE RADIO radio, y Centro en (x0,y0)
PARAMETROS x0,y0,radio

	PI:=3.141517
	alfa:=0
	WHILE alfa<360
		x:= x0 + radio*cos(alfa/180*PI)
		y:= y0 + radio*sin(alfa/180*PI)
		G01 X{x} Y{y} F{vel1}

		alfa:=alfa + 10
	END WHILE

//

