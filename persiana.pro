// persiana


PLANO XY
G49 G40 G80 G50 G61 G90
G54
G92.1
G21 
S1000


// 2 mm por arriba del cero = sup de la pieza
dsz := 2
			
prof:=prof_ranura

rot_0:=3

pi:=3.14159
alfa:=angulo_ranura*pi/180
dx:=largo_ranura*cos(alfa)
dy:=largo_ranura*sin(alfa)
rx:=r_fresa_ps*cos(alfa)
ry:=r_fresa_ps*sin(alfa)
// correccion del solape x el radio de la fresa
solape_ranura:=solape_ranura - 2*(r_fresa_ps-rx)

//largo_inferior==largo espiga inferior
// pos_x1 = pos de la primera ranura,
// pos_x2 usualmente es igual a pos_x1, pero luego, como el paso
// es fijo, se agranda pos_x2 para que quede al ras de la ultima ranura


// dr es la distancia entre ranuras
dr:=dx-solape_ranura
//+2*r_fresa_ps*(1-cos(alfa))
IF dr<10
	dr:=10
ENDIF

IF modelo == 3 .AND. retropalpador == 0
	//retira la punta del palpador que est� tocando el topecito met�lico, y se puede mover sin enganchar nada.
	G01 Z5 F{vel_z}
ENDIF

IF modelo==2
	G01 Z0 F{vel_z}
	G01 A90 F{vel_rot}
ENDIF
// el g59p13 tiene una compensacion en Y por el angulo de 50x50 donde apoya abajo. El z lo tiene en cero
g59p13
IF modelo < 3
	//modelos anteriores a la fv1
	G52 X0 Y0 Z{-maq_abajo_dz+esp_pieza+l_fresa_ps}
ELSE
	//fv1
	G52 X0 Y0 Z{-maq_dz+l_fresa_ps}
ENDIF

IF modelo >= 2
	M03
ENDIF

G01 Y0 F{vel_xy}
G01 X0


' ranuras
// calculo el largo util y la cantidad de ranuras
largo_u := largo_pieza - 2*pos_x1 - 2*(r_fresa_ps-rx)
cant:= INT((largo_u-dx)/dr ) + 1
IF cant<1
	cant:=1
ENDIF


// activo la fresa
IF modelo < 2
	M04
ENDIF


ENDIF
dt:=0
cant_tra:=0
IF pos_x3>0
	IF pos_x3<30
		cant_tra:=pos_x3
		// travesanios centrados, cuando llega a un trav. deja 2 varillas libres
		dt:= INT((cant-2*cant_tra)/(cant_tra+1))
		// hago dt varillas, luego dejo 2, hago dt varillas, dejo 2 etc
	ENDIF
ENDIF



// la primer ranura empieza justo en la pos_x1, 
// a partir de ahi va a un paso constante dr
i:=0
t:=0
r:=0
aux_tra:=0
WHILE i<cant

	// el aux_tra<cant_tra es para eliminar el error de redondeo en 1 que puede haber en la ultima varilla
	IF aux_tra<cant_tra .AND. cant_tra>0 .AND. r==dt
		// llegue al travesanio, dejo 2 varillas 
		i:=i+2
		r:=0
		// y cuento un trav. mas
		aux_tra:=aux_tra+1
	ELSE
		' hago la varilla
		PROGRAMA ranura CON pos_x1+i*dr+rx + (r_fresa_ps-rx),dx-2*rx,dy-2*ry,pieza_doble,t
		i:=i+1
		r:=r+1
	ENDIF
		
	IF t==1
		t := 0
	ELSE
		t := 1
	ENDIF

END WHILE

' Alojamiento de las espigas
esp_pta := ancho_pieza
pb_mds:=0

' ultima espiga
IF largo_inferior!=0
	PROGRAMA espiga CON  largo_pieza - pos_x2 + sep2 ,ancho_pieza/2 , largo_inferior ,ancho_espiga,prof_espiga
	IF pieza_doble
		PROGRAMA espiga CON  largo_pieza - pos_x2 + sep2 ,3*ancho_pieza/2 , largo_inferior ,ancho_espiga,prof_espiga
	ENDIF
ENDIF


' espiga intermedias
IF cant_tra>0 
	' travesanios centrados
	j:=cant_tra-1
	WHILE j>=0
		pos_tra:=pos_x1+((j+1)*dt + 2*j) *dr + 2*(r_fresa_ps-rx) + solape_ranura
		ancho_tra:=2*(dr-sep1)-solape_ranura-2*(r_fresa_ps-rx)
		// el trav va desde pos_tra hasta pos_tra+2*dr
		// para calcular la espiga hay que centrar en base a la separacion sep1

		PROGRAMA espiga CON pos_tra + sep1, ancho_pieza/2,ancho_tra,ancho_espiga,prof_espiga
		IF pieza_doble
			PROGRAMA espiga CON pos_tra + sep1, 3*ancho_pieza/2,ancho_tra,ancho_espiga,prof_espiga
		ENDIF
		j:=j-1
	END WHILE
ENDIF

' 1er espiga
PROGRAMA espiga CON pos_x1 - sep1 - largo_espiga, ancho_pieza/2,largo_espiga,ancho_espiga,prof_espiga
IF pieza_doble
	PROGRAMA espiga CON pos_x1 - sep1 - largo_espiga, 3*ancho_pieza/2,largo_espiga,ancho_espiga,prof_espiga
ENDIF


	
IF modelo==2 .AND. palpador_m==1
	G54
	G92.1
	G01 Z0 F{vel_z}
	G52 X0 Y0
	G01 X{-dist_ax} F{vel_xy}
	M05
	G01 Y0
	G01 A{rot_0} F{vel_rot}
	G28.1 X{-dist_ax} Y0 Z0
ELSE
	IF modelo != 3
		M05
	ENDIF
	G54
	G92.1
	IF modelo != 3 .OR. retropalpador
		//fv1 modelo viejo con retropalpador o modelo anterior de la maquina
		G01 Z0 F{vel_z}
	ELSE
		//modelo nuevo de la fv1 con palpador directo
		G01 Z5 F{vel_z}
	ENDIF
	IF modelo == 3
		M05
	ENDIF
	G52 X0 Y0
	G01 X{-dist_ax} Y0  F{vel_xy}
	IF modelo != 3 .OR. retropalpador
		//fv1 modelo viejo con retropalpador o modelo anterior de la maquina
		G28.1 X{-dist_ax} Y0 Z0
	ELSE
		//modelo nuevo de la fv1 con palpador directo
		G28.1 X{-dist_ax} Y0
		G28.1 Z5
	ENDIF
ENDIF





