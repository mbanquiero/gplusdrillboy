
PARAMETROS pos_x,K,ancho,esp,prof

(Frente del vaciado de marco)

IF pb_fradio<r_fresa_p
	IF pb_fradio
		pb_fradio:=r_fresa_p
	ELSE
		pb_fradio:=esp/2		
	ENDIF
ENDIF

dy:=esp_pta + pb_mds
G52 X{-pos_pinza+desf_x0+pos_x} Y{dy + fn_dfy} Z{-maq_dz+esp_marco+ fn_dfz+ l_fresa_p}
pos_y:=0

//arranco por el medio de la chapita para no romper ninguna curvatura
G01 Y{-((dy-esp)/2 - pb_vaciado_desf_y + r_fresa_p)} F{vel_xy}
G01 X{(ancho-r_fresa_p)/2}

M502
G04 p1
G01 Z1 F{vel_z}

ACTIVAR FRESA

G01 Z{-prof} F{vel_pen}
G01 X{ancho-pb_fradio} F{vel_mec}
	
//lado derecho de la curvatura
G03 X{ancho-r_fresa_p} Y{-((dy+esp)/2-pb_fradio)} I0 J{pb_fradio-r_fresa_p} F{vel_curvas}
G01 Y{-((dy-esp)/2+pb_fradio)} F{vel_mec}
G03 X{ancho-pb_fradio} Y{-((dy-esp)/2+r_fresa_p)} I{r_fresa_p-pb_fradio} J0 F{vel_curvas} 	
	
G01 X{pb_fradio} F{vel_mec}
	
//lado izquierdo de la curvatura
G03 X{r_fresa_p} Y{-((dy-esp)/2+pb_fradio)} I0 J{r_fresa_p-pb_fradio} F{vel_curvas}
G01 Y{-((dy+esp)/2-pb_fradio)} F{vel_mec}
G03 X{pb_fradio} Y{-((dy+esp)/2-r_fresa_p)} I{pb_fradio-r_fresa_p} J0 F{vel_curvas}
	
G01 X{ancho/2+r_fresa_p} F{vel_mec}
	
PARAR FRESA
M503
(retiro Z)
PROGRAMA moverAZSegura

