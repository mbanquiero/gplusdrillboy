// posicion, ancho, espesor, prof
PARAMETROS pos_x,ancho,esp,prof,K



//desf_vir es el desfasaje que se le hace a la parte inferior de los cajeado por la acumulacion de viruta
//en la parte inferior del cajeado queda como si fuera una rampa porque la diferencia de altura entre el frente(profundidad=0) del cajeado
//y en la profundidad total es de desf_vir
desf_vir:=2



// Cajeado sobre el canto INFERIOR (Por eso toma los parametros XXX_abajo)
PARAR FRESA

IF retropalpador
	G52 X{pos_x} Y{pos_y0} Z{pos_z0}
ELSE
	G52 X{pos_x} Y{pos_y0}
ENDIF

G01 X{r_fresa_i} F{vel_xy}
M03

IF prof >0
	//desfasaje por la viruta	
	desf_vir_x := desf_vir/INT(prof/cfeed + 0.95)
ELSE
	desf_vir_x := 0
ENDIF
IF zigzag
	desf_vir_x := 2*desf_vir_x
ENDIF
aux_x := 0

IF esp<=2*r_fresa_i
	// Hago una sola pasada
	// profundiza tanto al ir como al volver
	G01 Y{-K*esp_pta/2} F{vel_y}
	G01 Z0 F{vel_mec}
	ACTIVAR FRESA
	flag:=0

	IF ranura
		pen_z:=pb_pr
	ELSE
		pen_z:=0
	ENDIF

	WHILE pen_z < prof
		// ida
		pen_z:=pen_z+cfeed
		IF pen_z>prof
			pen_z:= prof
		ENDIF

		IF flag==0
			M03
			flag:=1
		ENDIF
		aux_x := aux_x + desf_vir_x

		IF zigzag
			G01 X{(ancho-r_fresa_i)} Z{-pen_z} F{vel_cajeado}

		ELSE
			G01 Z{-pen_z} F{vel_pen}
			G01 X{(ancho-r_fresa_i)} F{vel_cajeado}
		ENDIF

		// Vuelta
		IF pen_z<prof
			pen_z:=pen_z+cfeed
			IF pen_z>prof
				pen_z:= prof
			ENDIF
			IF zigzag
				G01 X{r_fresa_i+aux_x} Z{-pen_z} F{vel_cajeado}
			ELSE
				G01 Z{-pen_z} F{vel_pen}
				G01 X{r_fresa_i+aux_x} F{vel_cajeado}
			ENDIF

		ENDIF

	END WHILE

	
ELSE
	// hay que hacer rectangulos
	IF ranura
		pen_z:=pb_pr
	ELSE
		pen_z:=0
	ENDIF
	
	G01 X{r_fresa_i} F{vel_xy}
	G01 Y{-K*((esp_pta-esp)/2 + r_fresa_i)} F{vel_y}
	G01 Z0 F{vel_mec}
	ACTIVAR FRESA
	flag:=0
	WHILE pen_z < prof
		pen_z:=pen_z+cfeed
		IF pen_z>prof
			pen_z:= prof
		ENDIF
		
		G01 X{r_fresa_i+aux_x} F{vel_mec}
		G01 Y{-K*((esp_pta-esp)/2 + r_fresa_i)}
			
		aux_x := aux_x + desf_vir_x
			

		IF flag==0
			M03
			flag:=1
		ENDIF
		IF zigzag

			G01 X{(ancho-r_fresa_i)} Z{-pen_z} F{vel_cajeado}
			G01 Y{-K*((esp_pta+esp)/2 - r_fresa_i)} 
			pen_z:=pen_z+cfeed
			IF pen_z>prof
				pen_z:= prof
			ENDIF
			G01 X{r_fresa_i+aux_x} Z{-pen_z}
	
		ELSE
			G01 Z{-pen_z} F{vel_pen}
			G01 X{(ancho-r_fresa_i)} F{vel_cajeado}
			G01 Y{-K*((esp_pta+esp)/2 - r_fresa_i)} 
			G01 X{r_fresa_i+aux_x}
		ENDIF

	END WHILE

	IF zigzag
		G01 Y{-K*((esp_pta-esp)/2 + r_fresa_i)} 
		G01 X{(ancho-r_fresa_i)} F{vel_cajeado}
		G01 Y{-K*((esp_pta+esp)/2 - r_fresa_i)} 
		G01 X{r_fresa_i+desf_vir} 
	ENDIF
ENDIF

PARAR FRESA
// me retiro a una posicion segura
IF retropalpador
	//version fv1 con retropalpador
	G28.1 Z{dsz}
ELSE
	//version de la fv1 con palpador directo
	G01 Z{dsz+rebaje_canto} F{vel_z}
ENDIF
IF ranura==0
	G01 Y60 F{vel_xy}
ENDIF


